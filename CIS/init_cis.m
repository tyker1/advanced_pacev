global filterVect;
global fs;
global frameSize;
global StimulationRate;
global MidFreq;
global KoefA;
global KoefB;
global filterOrd;
global NumChannel;
global OverlapLen;
global SampleSize;
global PulsePerFrame;

NumChannel = 15;
filterOrd = 6;
fs = 16000;
StimulationRate = 500;
PulsePerFrame = 4;
frameSize = fs/StimulationRate * PulsePerFrame;
OverlapLen = frameSize; % 50% overlap
SampleSize = 4;
KoefA = zeros(filterOrd+1, NumChannel);
KoefB = zeros(filterOrd+1, NumChannel);

filterVect = [250,500
                      500,750
                      750,1000
                      1000,1250
                      1250,1500
                      1500,1750
                      1750,2050
                      2050,2400
                      2400,2825
                      2825,3312
                      3312,3812
                      3812,4375
                      4375,5000
                      5000,6500
                      6500,8000];
 filterLow = filterVect(:,1);
 filterHigh = filterVect(:,2);
 
 MidFreq = round((filterLow + filterHigh)/2);
 filtBandWidth = filterHigh-filterLow;
 
 for i=1:NumChannel-1
     [KoefB(:,i),KoefA(:,i)] = butter(filterOrd/2, [filterLow(i),filterHigh(i)]./(fs/2),'bandpass');
 end
 
 [KoefB(:,NumChannel), KoefA(:,NumChannel)] = butter(filterOrd, filterLow(NumChannel)/(fs/2),'high');
 
 