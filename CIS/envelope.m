function [ envErg ] = envelope(filterErg, numChannel, frameSize, overlap)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

envErg = zeros(frameSize+overlap, numChannel);

for i = 1:numChannel
    
    temp = hilbert(filterErg(:,i));
    envErg(:,i) = abs(temp);
    
end


end

