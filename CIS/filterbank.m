function [ filtErg ] = filterbank( sig, numChannel, koefb, koefa, frameSize,overlap )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

filtErg = zeros(frameSize+overlap, numChannel);

for i = 1:numChannel
    filtErg(:,i) = filter(koefb(:,i), koefa(:,i), sig);
end

end

