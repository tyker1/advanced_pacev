%Initialisierungsfunktion f��r ACE-Strategie
global WindowSize;
global Samplefrequence;
global SamplePerFrame;
global M;
global N;
global WindowFunc;
global FilterKoeff;
global FilterGain;
global AudioData;
global MidFreq;


[Data, Samplefrequence] = audioread(Datapath);
AudioData = Data(1:(Samplefrequence*SimDuration));


%% Allgemeine Initialisierungen
WindowSize = 128; %FFT-Fenstergrosse definiert nach standart ACE
Samplefrequence = 16000; % Standart ACE arbeitet mit dem Abtastfrequenz von 16kHz
SamplePerFrame = 16;
M = 22; %Anzahl der Elektrode bzw. Frequenzbaender

N = 8; %Anzahl der aktivierende Elektrode je Frame

WindowFunc = hann(WindowSize); %Jedes Frame muss erst mit Hanningfenster vorverarbeitet sein


%% Einstellung fuer Filterbank, Standart ACE hat 22 Kanaele
% Format: Mittelfrequenz | Anzahl der FFT-Bins | Koeffizient fuer jedes
% FFT-Bins
FilterVect = [250,1,0.98;
              375,1,0.98;
              500,1,0.98;
              625,1,0.98;
              750,1,0.98;
              875,1,0.98;
              1000,1,0.98;
              1125,1,0.98;
              1250,1,0.98;
              1437,2,0.68;
              1687,2,0.68;
              1937,2,0.68;
              2187,2,0.68;
              2500,3,0.65;
              2875,3,0.65;
              3312,4,0.65;
              3812,4,0.65;
              4375,5,0.65;
              5000,5,0.65;
              5687,6,0.65;
              6500,7,0.65;
              7437,8,0.65];
          
FilterKoeff = zeros(WindowSize,M);
MidFreq = FilterVect(:,1);
FFTBins = FilterVect(:,2);
FilterGain = FilterVect(:,3);
base = floor(MidFreq(1) /  (Samplefrequence/WindowSize)) + 1;
%% Einhuellende Detektion
for i = 1:M
    offset = sum(FFTBins(1:i-1));
    % Da FFT(0) ist der Gleichanteil, faengt das Filterbank von FFT(2) an
    % da Erste Bandpassfilter dem Mittelfrequenz 250Hz entspricht
    FilterKoeff((offset+base):(offset+FFTBins(i)+base-1),i) = ones(FFTBins(i),1).*FilterGain(i);
    newFilterVect(i,:) = [MidFreq(i), FilterGain(i), offset+base - 1, offset++FFTBins(i)+base-1-1];
end
