function [ smoothedSeg ] = smoothFilt( Seg )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


len = length(Seg);
smoothedSeg = zeros(1,len);
smoothedSeg(1) = Seg(1);
for i = 2:len
    smoothedSeg(i) = (Seg(i-1)+Seg(i))/2;
end

end

