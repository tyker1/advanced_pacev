function x = wrcoefAD(o,c,l,varargin)
%WRCOEFAD Reconstruct single branch from 1-D wavelet coefficients.
%   modified from MATLAB function WRCOEF, in order to reconstruct the

%   M. Misiti, Y. Misiti, G. Oppenheim, J.M. Poggi 12-Mar-96.
%   Last Revision: 06-Feb-2011.
%   Copyright 1995-2015 The MathWorks, Inc.

% Check arguments.
narginchk(4,6)
o = lower(o(1));
rmax = length(l); nmax = rmax-2;

if o=='a'
    nmin = 0; 
    else nmin = 1; 
end
if ischar(varargin{1})
    [Lo_R,Hi_R] = wfilters(varargin{1},'r'); next = 2;
else
    Lo_R = varargin{1};  Hi_R = varargin{2}; next = 3;
end
if nargin>=(3+next) , n = varargin{next}; else n = nmax; end

if (n<nmin) || (n>nmax) || (n~=fix(n))
    error(message('Wavelet:FunctionArgVal:Invalid_ArgVal'));
end

% Get DWT_Mode
dwtATTR = dwtmode('get');

x = c;

imin = rmax-n;
x  = upsconv1(x,F1,l(imin+1),dwtATTR);
for k=2:n , x = upsconv1(x,Lo_R,l(imin+k),dwtATTR); end
