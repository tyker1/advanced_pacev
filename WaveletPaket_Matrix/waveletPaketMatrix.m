function [ cellFilterMatrix, cellReconsMatrix ] = waveletPaketMatrix( sigLen, level, Lo_D,Hi_D,Lo_R,Hi_R ,mextMode)
%waveletPaketMatrix
%   generate a Set of Matrix for wavelet paket decomposition with specific
%   level for multi-resolution analysis of a specific length of signals, it returns 2 cell-matrices witch
%   includes the forwards transform Matrices (cellFilterMatrix) and invers
%   transform Matrices (cellReconsMatrix). cellFilterMatrix{i,j} contains
%   the forwards transform Matrix for the node [i-1,j-1] in the wavelet
%   paket tree, it can be simply applied to the signal of intresest using:
%               WaveletTransformedNodeIJ = cellFilterMatrix{i+1,j+1} * sig
%   to obtain the WaveletTransformed Signal at [i,j]
%   
%   Parameters:
%   note: both Lo_D, Hi_D, Lo_R and Hi_R must be even length
%   sigLen          : Length of Signal on witch wavelet paket transform
%                          will be applied
%   level             : Level of wavelet paket transform (cellFilterMatrix
%   and cellReconsMatrix will be then both (Level+1 x 2^Level) CellMatrix
%   Lo_D             : approximation filter (LP-Filter) of the mother
%                          wavelet, can be generated using wfilters
%                          function from Matlab-Wavelet Toolbox
%   Hi_D             : detail filter (HP-Filter) of the mother wavelet, can
%                           be generated using wfilters function
%   Lo_R            :  reconstruction filter of apprxoimation level
%   Hi_R            :  reconstruction filter of detail level
%   mextMode : extention mode for extending the original signal, string,
%   can be set to 'sym' for symetric extention or 'per' for periodic
%   extention

cellFilterMatrix = cell(level+1,2^level);
cellReconsMatrix = cell(level+1,2^level);

cellFilterMatrix{1,1} = eye(sigLen);
cellReconsMatrix{1,1} = eye(sigLen);

for i = 2:level+1
    
    nodes = 2^(i-1);
    for j = 1:2:nodes
            lastX = floor((j-1)/2) + 1;
            lastFMat = cellFilterMatrix{i-1,lastX};
            lastRMat = cellReconsMatrix{i-1,lastX};
            lenN = size(lastFMat,1);
            [Lo_Mat,Hi_Mat] = waveletFilterMatrix(lenN,Lo_D,Hi_D,lastFMat,mextMode);
            lenN1 = size(Lo_Mat,1);
            [rLo_Mat,rHi_Mat] = waveletRecMatrix(lenN1,Lo_R,Hi_R,lastRMat,lenN,mextMode);
            
            cellFilterMatrix{i,j} = Lo_Mat;
            cellFilterMatrix{i,j+1} = Hi_Mat;
            cellReconsMatrix{i,j} = rLo_Mat;
            cellReconsMatrix{i,j+1} = rHi_Mat;
            
    end
    
end

end

