function [ kMat ] = generateKeepMat( N,fVect, s, perFlag )
%GENERATEKEEPMAT Summary of this function goes here
%   Detailed explanation goes here

Len = length(fVect);
if isempty(s)
    if ~perFlag
        LenX = N*2 - Len + 2;
    else
        LenX = N*2;
    end
else
    LenX = s;
end

if ~perFlag
    LenY = N*2 - 1 + Len - 1;
    kMat = zeros(LenX,LenY);

    first = 1+floor((LenY-LenX)/2);
    last = LenY - ceil((LenY-LenX)/2);
    idxVect = first:last;
    for i = 1:LenX
        kMat(i,idxVect(i)) = 1;
    end
else
    LenY  = N*2 + (Len) + Len - 1;
    kMat = zeros(LenX,LenY);
    first = Len;
    %last = Len + LenX-1;
    for i = 1:LenX
        kMat(i,first+i-1) = 1;
    end
end



end

