clc;
clear all;
sigLen = 512;
level = 10;

tic
[Lo_D, Hi_D, Lo_R, Hi_R] = wfilters('db7');
MatFiltTime = toc;
loops = 100;

mode = 'per';

dwtmode(mode);
tic
[cellFilter,cellRec] = waveletPaketMatrix(sigLen,level,Lo_D,Hi_D,Lo_R,Hi_R,mode);
MatTime = toc;

total = 0;
lMat = zeros((1-2^(level+1))/(1-2),1);
iter = 1;
for i = 1:level+1
    for j = 1:2^(i-1)
        outLen = size(cellFilter{i,j},1);
        total = total + outLen;
        lMat(iter) = outLen;
        iter = iter + 1;
    end
end

wpMat = zeros(total,sigLen);
startIdx = 1;
for i = 1:level+1
    outLen = size(cellFilter{i,1},1);
    for j = 1:2^(i-1)
        endIdx = startIdx + outLen - 1;
        wpMat(startIdx:endIdx,:) = cellFilter{i,j};
        startIdx = endIdx + 1;
    end
end


testSig = rand(sigLen,1);

%% Performance Test for fulltree decomposition with Matrix vs wpdec function from Matlab
disp('Starting Performance Test');
disp('-->Runing Matrix Multiplication of wavelet Packet Transform');
tic
for i = 1:loops
    testMatComp = wpMat * testSig;
end
toc

tic
for i = 1:loops
    for j = 1:level+1
        for k = 1:2^(j-1)
            test = cellFilter{j,k} * testSig;
        end
    end
end
MatFull = toc;
disp('---->Matrix Multiplication finished');
disp('-->Running wpdec function of Matlab for the Transform');
tic
for i = 1:loops
    test1 = wpdec(testSig,level,'db7');
end
WPTFull = toc;
disp('---->wpdec finished');
fprintf('To Generate the Matrix : %.5f s\nFull Decomposition with %d level and %d times of db7-wavelet\nWavelet = %.5f\t Matlab = %.5f\n', MatFiltTime + MatTime, level, loops,MatFull, WPTFull);
fprintf('Total time consum:\nMatrix Mulitplication = %.5f s\nMatlab = %.5f s\n', MatFull+MatFiltTime+MatTime,WPTFull);