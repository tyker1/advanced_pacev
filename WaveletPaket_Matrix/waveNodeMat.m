function [ fMat,rMat ] = waveNodeMat( cord, N, Lo_D,Hi_D, Lo_R,Hi_R, dwtExtMode )
%waveNodeMat
%   generate forward Wavelet Transform Matrix and it's invers Transform
%   Matrix for a given Node in the Wavelet Packet Tree
%
% Parameters:
% cord          : Vector [i,j] of the Node in Wavelet Packet Tree
% N                : Signal length of the original Signal
% Lo_D, Hi_D, Lo_R, Hi_R : Filter Coeffizents of Wavelet Filterbank (using
%                    wfilters function)
% dwtExtMode : Extention Mode for extending the original signal (string,
% can be set to 'sym' (for symetry extention) or 'per' ( for periodic
% extention)


stack = [];
endDepth = cord(1);
endIdx = cord(2);
total = 1;
stack(total,:) = [endDepth, endIdx];
total = total + 1;
while (endDepth > 0)
    endDepth = endDepth - 1;
    endIdx = floor(endIdx / 2);
    stack(total,:) = [endDepth,endIdx];
    total = total + 1;
end

total = total - 1;
lN = N;
fMat = [];
rMat = [];

for i = total-1:-1:1
    ord = stack(i,2);
    [aMat,dMat] = waveletFilterMatrix(lN,Lo_D,Hi_D,fMat, dwtExtMode);
    lN1 = size(aMat,1);
    [aMatI,dMatI] = waveletRecMatrix(lN1,Lo_R,Hi_R,rMat,lN, dwtExtMode);
    lN = lN1;
    
    if (mod(ord,2) == 0)
        fMat = aMat;
        rMat = aMatI;
    else
        fMat = dMat;
        rMat = dMatI;
    end
end

end

