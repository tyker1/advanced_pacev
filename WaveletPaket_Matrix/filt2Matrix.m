function [ ftMat ] = filt2Matrix( N, fVect , mode)
%FILT2MATRIX 
%   Generate a Matrixform of FIR Filter given in fVect, witch can be used
%   to improve the performence
%
%  Parameters:
%  N                : Signal length
%  fVect          : Filter Coeffetient of a FIR Filter
%  mode         : string, the size of convolved Signal:
%                       - full : returns a full convolution matrix (result
%                       of convolution is N + len(fVect) - 1
%                       - valid: returns only those parts of the
%                       convolution that are computed without the
%                       zero-padded edges. 
%                       -  same : returns the central part of the
%                       convolution that has same length as N

Len = length(fVect);
% newLen = (N + 2*(Len-1)) + (Len - 1);
% LenY = newLen - Len + 1;
newLen = N + Len - 1;
LenY = N;
ftMat = zeros(newLen,LenY);

newVect = fliplr(fVect);

for i = 1:newLen
    if ( i <= LenY)
        startMat = i - Len + 1;
        if startMat < 1
            startMat = 1;
        end
        startVect = Len - i + 1;
        if startVect < 1
            startVect = 1;
        end
        ftMat(i,i:-1:startMat) = newVect(Len:-1:startVect);
    else
        ftMat(i,i-Len+1 : end) = newVect(1:end-(i-LenY));
    end
end

% switch mode
%     case 'valid'
%         newX = N - Len + 1;
%         kepMat = zeros(newX,newLen);
%         sPos = Len;
%         for i = 1:newX
%             kepMat(i,sPos + i - 1) = 1;
%         end
%     case 'full'
%         kepMat = eye(newLen);
%     case 'same'
%         newX = N;
%         kepMat = zeros(newX, newLen);
%         sPos = round((Len-1)/2);
%         for i = 1:newX
%             kepMat(i,sPos + i - 1) = 1;
%         end
% end
% 
% ftMat = kepMat * ftMat;

switch mode
    case 'valid'
        newX = N - Len + 1;
        sPos = Len;
        keepVect = sPos:(newX+sPos-1);
    case 'full'
        keepVect = 1:newLen;
    case 'same'
        newX = N;
        sPos = round((Len-1)/2);
        keepVect = sPos:(newX+sPos-1);
end

ftMat = ftMat(keepVect,:);

end

