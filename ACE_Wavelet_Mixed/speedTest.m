clear variables;

[audioDat,fs] = audioread('../AudioSample/spfg.wav');

loops = 10000;

startFrame = 3*fs;
FrameSize = 512;
endFrame = startFrame + FrameSize - 1;

Frame = audioDat(startFrame:endFrame);

[Lo_D,Hi_D,Lo_R,Hi_R] = wfilters('db7');
LenFilter = length(Lo_D);
extMat = generateExtMat(FrameSize,LenFilter-1,'sym');
[LenX,LenY] = size(extMat);
Lo_D_Mat = filt2Matrix(LenX,Lo_D);
downMatA = generateDMat(LenX,Lo_D);
aMat = downMatA * Lo_D_Mat * extMat;
Hi_D_Mat = filt2Matrix(LenX,Hi_D);
downMatD = generateDMat(LenX,Hi_D);
dMat = downMatD * Hi_D_Mat * extMat;
LenFilt = length(Lo_D) - 1;
tic
for i = 1:loops
    %extFrame = wextend('1D','sym',Frame,LenFilt);
    MatA = aMat * Frame;
    MatD = dMat * Frame;    
end
timeMat = toc;
AveMat = timeMat / loops * 1000;

tic

for i = 1:loops
    [a,d] = dwt(Frame,Lo_D, Hi_D);
end
timeDWT = toc;
AveDWT = timeDWT / loops *1000;

subplot(211);
plot(MatA,'*');
hold on
plot(a,'o');
hold off
legend('Matrix Multiplication','DWT Matlab');
title('Approximation Coefficient');

subplot(212);
plot(MatD,'*');
hold on
plot(d,'o');
hold off
legend('Matrix Multiplication','DWT Matlab');
title('Detail Coefficient');
fprintf('Total %d loops:\nMatrix: total=%.4f s\t averange=%.4f ms\nDWT: total=%.4f s\taverange=%.4f ms\n',loops,timeMat,AveMat,timeDWT,AveDWT);