clear variables;

[audioDat,fs] = audioread('../AudioSample/spfg.wav');

loops = 10000;

startFrame = 3*fs;
FrameSize = 1024;
endFrame = startFrame + FrameSize - 1;
Frame = audioDat(startFrame:endFrame);
[Lo_D,Hi_D,Lo_R,Hi_R] = wfilters('db7');

Level = 3;
lN = FrameSize;
aMat = [];
dMat = [];
aMatI = [];
dMatI = [];
for i = 1:Level
    [aMat,dMat] = waveletFilterMatrix(lN,Lo_D,Hi_D,aMat);
    lN1 = size(aMat,1);
    [aMatI,dMatI] = waveletRecMatrix(lN1,Lo_R,Hi_R,aMatI,lN);
    lN = lN1;
end

