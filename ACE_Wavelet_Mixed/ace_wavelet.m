clc;
clearvars;
clear OverlapSave; % L�schen von persistente Variable in OverlapSave Func
close all;

global SimDuration;
global Datapath;

SimDuration = 10; %Dateil�nge in Sekunde
Datapath = '../AudioSample/spfg.wav'; %Pfad zur Audiodatei
tic
init_ace;
toc
RawData = audioread(Datapath);
AudioData = RawData(1:(SampleFrequency*SimDuration));

Frames = floor(length(AudioData) / SamplePerFrame);
ChannelMat = zeros(Frames,numElectrode);

test = AudioData(1:SamplePerFrame);
tic
for i = 1:Frames
    frameStart = (i-1)*SamplePerFrame + 1;
    frameEnd   = i*SamplePerFrame;
    Frame =AudioData(frameStart:frameEnd);
    %filtSig = filterbank(Frame,WaveletNodes,NodeLen,Lo_D,Hi_D,Lo_R,Hi_R,DWTDepth,numElectrode,SamplePerFrame, wTree);
    Buffer = OverlapSave(Frame,WindowSize,SamplePerFrame);
    Buffer = Buffer .* WinFunc';
    filtSig = Ffilterbank(Buffer',numElectrode,waveletMat,WindowSize,NodeLen);
    envSound = envelope(filtSig, lowA, lowB,numElectrode, WinFunc, 1);
    [selAmp,selIdx] = selection(envSound,numElectrode,N);
    for j = 1:N
        ChannelMat(i,selIdx(j)) = selAmp(j);
    end
end
toc

synSig = synthese(ChannelMat, SamplePerFrame, MidF, SampleFrequency, numElectrode);
%synSig = synSig ./ max(synSig) / 2;