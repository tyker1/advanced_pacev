function [ selAmp,selIdx ] = selection(envSound,numChannel,N)
%SELECTION Summary of this function goes here
%   Detailed explanation goes here


[newA,newIdx] = sort(envSound,'descend');
selAmp = newA(1:N);
selIdx = newIdx(1:N);
end

