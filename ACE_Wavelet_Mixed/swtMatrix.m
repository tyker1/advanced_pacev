function [ aMat, dMat, uLo_D, uHi_D ] = swtMatrix( N,Lo_D,Hi_D, Mat )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

Len = length(Lo_D);
extMat = generateExtMat(N,Len/2,'per');
LenX = size(extMat,1);
Mat_Lo_D = filt2Matrix(LenX,Lo_D);
Mat_Hi_D = filt2Matrix(LenX,Hi_D);
kepMat = swtKeepMat(size(Mat_Lo_D,1),N,Len+1);
upMat = swtUpMat(Len);
uLo_D = (upMat * Lo_D(:))';
uHi_D = (upMat * Hi_D(:))';
aMat = kepMat * Mat_Lo_D * extMat;
dMat = kepMat * Mat_Hi_D * extMat;

if  ~isempty(Mat)
    aMat = aMat * Mat;
    dMat = dMat * Mat;
end

end

function [kepMat] = swtKeepMat(MatLen,N,first)

kepMat = zeros(N,MatLen);

ord = first:first+N-1;
for i = 1:N
    kepMat(i,ord(i)) = 1;
end

end

function [upMat] = swtUpMat(Len)
LenY = Len;
LenX = Len*2;

upMat = zeros(LenX,LenY);

for i = 1:2:LenX
    upMat(i,(i-1)/2+1) = 1;
end

end