function [ Envs ] = FFTEnvelope(r_sqr,FilterKoeff,M)
% Einhuellende Detektion im Bildbereich
%   Ausgang ist M Werte entspricht die Energie aus jeweiligem Filter

Envs = zeros(M,1);

for i = 1:M
    % Envs ist die "Power-sum combination of bins"
    Envs(i) = sqrt(r_sqr*FilterKoeff(:,i));
end

end

