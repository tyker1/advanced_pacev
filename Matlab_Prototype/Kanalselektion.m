function [outAmp,outIndex] = Kanalselektion(a, N, Labs, sl, sr, av, alpha, M, NumVirtCh)

outAmp = zeros(1,N);
outIndex = zeros(1,N);
new_a = zeros(1,M);

for i = NumVirtCh:NumVirtCh:M
    [am, idx_a] = max(a(i-NumVirtCh+1:i));
    new_a(idx_a+i-NumVirtCh) = am;
end

%a_db = 10*log10(new_a*32767);
a_db = 10 * log10(new_a/(10^-12)); % formel aus Masterarbeit Sherif Omran 2011
PowerLawMask= zeros(1,M);

for i = 1:N
   [max_wert, max_idx] = max(a_db-10*log10(PowerLawMask.^(1/alpha)));
   maske = a_db(max_idx);
   for j = 1:M
       PowerLawMask(j) = power_law(maske, j, max_idx, Labs,sl,sr,av,alpha,PowerLawMask(j));
   end
   
   outAmp(i) = a(max_idx);
   outIndex(i) = max_idx;
end

end

function Lt = power_law(a, z, idx, Labs, sl, sr, av, alpha,it)
          Lt = it;
          if (it == 0)
              Lt = (10^(Labs(z)/10)).^alpha;
          end
          
          Lt = Lt + (10.^(spread_func(a,idx, z, sl, sr, av)/10)).^alpha;
end
    
function Li = spread_func(a, i, z, sl, sr, av)
    if (i > z)
        Li = a - av - sl*(i-z);
    else
        Li = a - av - sr*(z-i);
    end
    
    if (Li < 0)
        Li = 0;
    end
    
end