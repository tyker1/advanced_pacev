clc;
clearvars;
clear OverlapSave; % L�schen von persistente Variable in OverlapSave Func
close all;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    Initialisierung und Dateneinlesen    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Erzeugte globale Variable:
% AudioData       : Audiodaten aus Wav Datei mit den L�nge definiert in "SimDuration"
% Samplefrequence : Abtastfrequenz von der entsprechenden Audiodatei
% SamplePerFrame  : Framel�nge
% WindowSize      : Fenstergr��e f�r FFT (Entspricht nicht die Framel�nge)
% WindowFunc      : Fensterfunktion f�r FFT
% FilterKoeff     : Koeffizientenmatrix f�r FFT Filter
% FilterGain      : D�mpfungsfaktor f�r Modifizierung der Filter
% M               : Anzahl der Kan�le M = NumVirtCh * numElektrode
% NumVirtCh       : Anzahl der Virtuelle Kan�le von jeder Elektrode
% N               : Anzahl der auszuw�hlenden Kan�le gilt N =< numElektrode =< M
% Labs            : Level Function (absolute H�rschwelle f�r Filterbank mit der minimale Schwelle auf 0dB)
% av              : Attenuation Parameter
% sl              : Left Slope
% sr              : Right Slope
% alpha           : Parameter fuer "Power-law Modell"

global SimDuration;
global Datapath;

SimDuration = 10; %Dateil�nge in Sekunde
Datapath = '../AudioSample/spfg.wav'; %Pfad zur Audiodatei

init_pacev;

Frames = floor(length(AudioData) / SamplePerFrame);
ChannelMat = zeros(Frames,M); % Matrix fuer Abspeicher von Kanalaktivitaeten

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Bearbeitung aller Frames               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[Lo_D,Hi_D,Lo_R,Hi_R] = wfilters('db7');
tic
for i = 1:Frames
    frameStart = (i-1)*SamplePerFrame + 1;
    frameEnd   = i*SamplePerFrame;
    Frame =AudioData(frameStart:frameEnd);
    
    Buffer = OverlapSave(Frame, WindowSize, SamplePerFrame); % Erzeugung von Daten mit Ueberlappung fuer FFT
%     test =wpdec(Frame,2,'db7');
%     test = wpsplt(test, [2 2]);
%     test = wpsplt(test, [2 1]);
%    [C,L] = wavedec(Frame,8,Lo_D,Hi_D);
%    d1 = detcoef(C,L,1);
%    [C1,L1] = wavedec(d1,2,Lo_D,Hi_D);
    fftErg = fftFilter(Buffer, WindowSize, WindowFunc'); % FFT Filterbank
    Einhuellende = FFTEnvelope(fftErg, FilterKoeff, M); % Einhuellende Detektion fuer PACE
    [selAmp, selIdx] = Kanalselektion(Einhuellende, N, Labs, sl, sr, av, alpha, M, NumVirtCh); % Kanalselektion
    for j = 1:N
        ChannelMat(i,selIdx(j)) = selAmp(j);
    end
end
toc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Rekonstruktion von dem Signal           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

synSig = synthese(ChannelMat, SamplePerFrame, MidFreq, Samplefrequence, M);
