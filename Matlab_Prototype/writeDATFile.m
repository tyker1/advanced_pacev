fileName = 'tableAPACEv.dat';

fileID = fopen(fileName,'w');
fwrite(fileID,'DATA');
fwrite(fileID,[4 192 1 2],'integer*4');
offsetglobal = floor(MidFreq(1)/(BinBW))+1;

for i = 1:M
    for j = 1:4
        fwrite(fileID,newFilterVect(i,j),'double');
    end
end

fclose(fileID);