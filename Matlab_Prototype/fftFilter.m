function [ fftres ] = fftFilter( signal,WindowSize, WindowFunc  )
% FFT Filterbank f�r PACE
%   Ausgang ist das Magnetude Quadrat von FFTBins

windowedSig = signal .* WindowFunc;
spectrum = fft(windowedSig)./WindowSize*2;
% r(j)^2 = x(j)^2+y(j)^2
% wo x und y den Real bzw. Imaginaerteil des Spektralanteil entspricht
r_sqr = imag(spectrum).^2 + real(spectrum).^2;

fftres = r_sqr;

end

