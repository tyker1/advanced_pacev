function [ nodeValue,stack ] = waveGetNode( C,L, cord, Lo_D,Hi_D, Lo_R, Hi_R )
%waveGetNode(C,L, cord,Lo_D, Hi_D, Lo_R, Hi_R)
%   erhaeltet die Koeffizienten von bestimmten WaveletPacketTransformation
%   C,L ist die dekomposierte Daten aus wavedec, cord ([Depth,Index]) beinhaltet die
%   Koordinate fuer das Ziel Knote, Lo und Hi sind die Filterkoeffizienten
%   aus denselben Mother-Wavelet wie C,L (durch Funktion wfilters)


%table = java.util.Hashtable;
%table.put("xxxx",{vector});
%double(table.get("xxxxx"));
depth = cord(1);
idx = cord(2);

if idx < 2
    if idx == 0
        nodeValue = appcoef(C,L,Lo_R, Hi_R, depth);
    else
        nodeValue = detcoef(C,L, depth);
    end
    return;
end

%%% Find Start Node and construct stack
stack = zeros(depth+1,2);
iter = 1;
startDepth = depth;
startOrd = idx;
stack(iter,:) = [startDepth,startOrd];
iter = iter + 1;
while startOrd > 1
    startOrd = floor(startOrd/2);
    startDepth = startDepth - 1;
    stack(iter,:) = [startDepth, startOrd];
    iter = iter + 1;
end
%%%
if startOrd == 0
    Sig = appcoef(C,L,Lo_R, Hi_R,startDepth);
else
    Sig = detcoef(C,L,startDepth);
end

for i = iter-2:-1:1
    [cA,cD] = dwt(Sig, Lo_D,Hi_D);
    if (mod(stack(i,2),2) == 0)
        Sig = cA;
    else
        Sig = cD;
    end
end

nodeValue = Sig;

end