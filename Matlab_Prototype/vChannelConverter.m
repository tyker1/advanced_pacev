function [ matCh ] = vChannelConverter( channels )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


totalCh = length(channels);

vCh = floor(totalCh / 24);



Chs = find(channels);

lenSel = length(Chs);

matCh = zeros(24,lenSel);

for i=1:lenSel
    lowEle = floor(Chs(i)/vCh) + 1;
    highEle = lowEle + 1;
    
    rateLow = vCh - mod(Chs(i),vCh);
    rateLow = rateLow / vCh;
    matCh(lowEle,i) = channels(Chs(i)) * rateLow;
    matCh(highEle,i) = channels(Chs(i)) * (1-rateLow);
end

end

