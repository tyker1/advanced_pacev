function [outAmp,outIndex] = m_sel(a, N, Labs, sl, sr, av, alpha, M)
% [sortedArray, IndexBeforeSort] = sort(a,'descend');
% 
% outAmp = sortedArray(1:N);
% outIndex = IndexBeforeSort(1:N);

outAmp = zeros(1,N);
outIndex = zeros(1,N);

Mask_a = zeros(1,N);
a_db = 10*log10(a);
for i = 1:N
   max = -65536;
   max_idx = 0;
   for j = 1:M
       mask_effect = power_law(Mask_a, j, i-1, Labs, sl, sr, av, alpha);
       delta = a_db(j) - mask_effect;
       
       if ((delta > max) && (isempty(find(outIndex == j, 1))))
           max = delta;
           max_idx = j;
       end
   end
   
   Mask_a(i) = a_db(max_idx); % Einheit dB
   outAmp(i) = a(max_idx);
   outIndex(i) = max_idx;
end

end

function Lt = power_law(a, z, Len, Labs, sl, sr, av, alpha)
        
        if (Len == 0)
            Lt = 0;
            return;
        end
        % I = 10^(L/10);
        % It(zi) = [Iabs(zi)^alpha + Sum(Ii(zj)^alpha)]^(1/alpha)
        It = (10^(Labs(z)/10)).^alpha;
        for idx = 1:Len
            It = It + (10.^(spread_func(a(idx),idx, z, sl, sr, av)/10)).^alpha;
        end
        
        It = It.^(1/alpha);
        Lt = 10*log10(It);
end
    
function Li = spread_func(a, i, z, sl, sr, av)
    if (i > z)
        Li = a - av - sl*(i-z);
    else
        Li = a - av - sr*(z-i);
    end
end
