%Initialisierungsfunktion f��r ACE-Strategie
%% Erlkaerung fuer globalen Variable
global WindowSize;
global Samplefrequence;
global SamplePerFrame;
global M;
global N;
global WindowFunc;
global FilterKoeff;
global FilterGain;
global Labs;
global av;
global sl;
global sr;
global alpha;

%% Allgemeine Initialisierungen
WindowSize = 512; %FFT-Fenstergrosse definiert nach standart ACE
Samplefrequence = 44100; % Standart ACE arbeitet mit dem Abtastfrequenz von 16kHz
SamplePerFrame = 64;
M = 43; %Anzahl der Elektrode bzw. Freqenzbaender

N = 11; %Anzahl der aktivierende Elektrode je Frame

WindowFunc = hann(WindowSize); %Jedes Frame muss erst mit Hanningfenster vorverarbeitet sein


%% Einstellung fuer Filterbank, Standart ACE hat 22 Kanaele
% Format: Mittelfrequenz | Anzahl der FFT-Bins | Koeffizient fuer jedes
% FFT-Bins
% FilterVect =   [250	1	0.98
%                 312	1	0.98
%                 375	1	0.98
%                 437	1	0.98
%                 500	1	0.98
%                 562	1	0.98
%                 625	1	0.98
%                 687	1	0.98
%                 750	1	0.98
%                 812	1	0.98
%                 875	1	0.98
%                 937	1	0.98
%                 1000	1	0.98
%                 1062	1	0.98
%                 1125	1	0.98
%                 1187	1	0.98
%                 1250	1	0.98
%                 1312	1	0.98
%                 1437	2	0.68
%                 1562	2	0.68
%                 1687	2	0.68
%                 1812	2	0.68
%                 1937	2	0.68
%                 2062	2	0.68
%                 2187	2	0.68
%                 2312	2	0.68
%                 2500	3	0.65
%                 2688	3	0.65
%                 2875	3	0.65
%                 3063	3	0.65
%                 3312	4	0.65
%                 3562	4	0.65
%                 3812	4	0.65
%                 4062	4	0.65
%                 4375	5	0.65
%                 4688	5	0.65
%                 5000	5	0.65
%                 5313	5	0.65
%                 5687	6	0.65
%                 6062	6	0.65
%                 6500	7	0.65
%                 6938	7	0.65
%                 7437	16	0.65
% ];
     FilterVect = [215.332031250000 1 1;
                   301.464843750000 1 1;
                   387.597656250000 1 1;
                   473.730468750000 1 1;
                   559.863281250000 1 1;
                   645.996093750000 1 1;
                   732.128906250000 1 1;
                   818.261718750000 1 1;
                   904.394531250000 1 1;
                   990.527343750000 1 1;
                   1076.66015625000 1 1;
                   1162.79296875000 1 1;
                   1248.92578125000 1 1;
                   1335.05859375000 1 1;
                   1421.19140625000 1 1;
                   1507.32421875000 1 1;
                   1593.45703125000 1 1;
                   1679.58984375000 1 1;
                   1765.72265625000 1 1;
                   1894.92187500000 2 1;
                   2067.18750000000 2 1;
                   2239.45312500000 2 1;
                   2411.71875000000 2 1;
                   2583.98437500000 2 1;
                   2756.25000000000 2 1;
                   2971.58203125000 3 1;
                   3229.98046875000 3 1;
                   3488.37890625000 3 1;
                   3789.84375000000 4 1;
                   4134.37500000000 4 1;
                   4478.90625000000 4 1;
                   4866.50390625000 5 1;
                   5297.16796875000 5 1;
                   5770.89843750000 6 1;
                   6330.76171875000 7 1;
                   6976.75781250000 8 1;
                   7708.88671875000 9 1;
                   8527.14843750000 10 1;
                   9474.60937500000 12 1;
                   10551.2695312500 13 1;
                   11757.1289062500 15 1;
                   13178.3203125000 18 1;
                   14857.9101562500 21 1];    
FilterKoeff = zeros(WindowSize,M);
MidFreq = FilterVect(:,1);
FFTBins = FilterVect(:,2);
FilterGain = FilterVect(:,3);

%% Einhuellende Detektion
for i = 1:M
    offset = sum(FFTBins(1:i-1));
    % Da FFT(0) ist der Gleichanteil, faengt das Filterbank von FFT(2) an
    % da Erste Bandpassfilter dem Mittelfrequenz 250Hz entspricht
    FilterKoeff((offset+3):(offset+FFTBins(i)+2),i) = ones(FFTBins(i),1).*FilterGain(i);
end

%% Pyschoakustisches Modell
%Intepretierte Funktion der absoluten Hoerschwelle in Ruheumgebung nach 
%"Perceptual coding of digital audio" IEEE vol. 88, no. 4, pp455-515, 2000
Tabs = @(f) 3.64*(f/1000).^(-0.8)-6.5*exp(-0.6.*(f/1000-3.3).^2)+10^(-3)*(f/1000).^4;

% Level Function
Labs = Tabs(MidFreq);
Labs = Labs - min(Labs); % Nach Paper, soll minimale Werte von Labs 0dB sein

% Einstellung fuer Masking Modells
% Alle in dB
av = 10; % attenuation parameter
sl=20; % left slope
sr=17; % right slope
alpha = 0.25; % parameter fuer "Power-law Modell"