function [ extMat ] = generateExtMat( N,Len, mode )
%GENERATEEXTMAT Summary of this function goes here
%   Detailed explanation goes here

LenX = N + 2*Len;
LenY = N;

switch mode
    case 'sym'
        extMat = zeros(LenX,LenY);
        extMat(Len+1:Len+N,:) = eye(N);
        iterator = 1;
        idx = 1;
        
        for i = Len:-1:1
            extMat(i,idx) = 1;
            idx = idx + iterator;
            if ((idx > LenY) || (idx < 1))
                iterator = iterator * -1;
                idx = idx + iterator;
            end
        end
        
        idx = LenY;
        iterator = -1;
        for i = LenX-Len+1:LenX
            extMat(i,idx) = 1;
            idx = idx + iterator;
            if ((idx > LenY) || (idx < 1))
                iterator = iterator * -1;
                idx = idx + iterator;
            end
        end
    case 'per'
        flag = 0;
        if (mod(N,2))
            LenX = LenX + 1;
            flag = 1;
        end
        extMat = zeros(LenX,LenY);
        extMat(Len+1:Len+N,:) = eye(N);
        extMat(Len+N+flag,N) = 1;
        iter = floor(Len/(N+flag));
        endIdxd = Len;
        startIdxu = Len+N+flag + 1;
        remLen = rem(Len,N+flag);
        for i = 1:iter
            extMat(endIdxd-(N+flag)+1:endIdxd,:) = extMat(Len+1:Len+N+flag,:);
            endIdxd = endIdxd - N - flag;
            extMat(startIdxu:startIdxu+N+flag-1,:) = extMat(Len+1:Len+N+flag,:);
            startIdxu=startIdxu + N + flag;
        end
        extMat(1:remLen,:) = extMat(Len+N+flag-remLen+1:Len+N+flag,:);
        extMat(startIdxu:end,:) = extMat(Len+1:Len+remLen,:);
end

% Len = length(fVect);
% LenX = N + 2*(Len - 1);
% LenY = N;
% extMat = zeros(LenX,LenY);
% 
% extMat(Len:(LenX-Len+1),:) = eye(N);
% idx = 1;
% iterator = 1;
% for i = Len-1:-1:1
%     extMat(i,idx) = 1;
%     idx = idx + iterator;
%     if ((idx > LenY) || (idx < 1))
%         iterator = iterator * -1;
%         idx = idx + iterator;
%     end
% end
% 
% idx = LenY;
% iterator = -1;
% for i = LenX-Len+2:LenX
%     extMat(i,idx) = 1;
%     idx = idx + iterator;
%     if ((idx > LenY) || (idx < 1))
%         iterator = iterator * -1;
%         idx = idx + iterator;
%     end
% end