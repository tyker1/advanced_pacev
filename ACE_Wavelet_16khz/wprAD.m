function [ sig ] = wprAD( Coef, cord, L, stack,Lo_R, Hi_R)
%wprAD rekonstruiert der gegebene Koeffizienten
%   Detailed explanation goes here


depth = cord(1);
idx = cord(1);
iter = 1;
if isempty(stack)
    stack = zeros(depth+1,2);
    iter = 1;
    startDepth = depth;
    startOrd = idx;
    stack(iter,:) = [startDepth,startOrd];
    iter = iter + 1;
    while startOrd > 1
        startOrd = floor(startOrd/2);
        startDepth = startDepth - 1;
        stack(iter,:) = [startDepth, startOrd];
        iter = iter + 1;
    end
else
    while (stack(iter,1) ~= 0)
        iter = iter + 1;
    end
end

iter = iter - 1;

lenCoef = length(L);

sig = Coef;

dwtATTR = dwtmode('get');

for i = 1:iter
    if (mod(stack(i,2),2) == 0)
        sig = upsconv1(sig,Lo_R, L(lenCoef - stack(i,1)+1), dwtATTR);
    else
        sig = upsconv1(sig,Hi_R, L(lenCoef - stack(i,1)+1), dwtATTR);
    end
end
depth = stack(iter,1)-1;

while depth > 0
    sig = upsconv1(sig,Lo_R,L(lenCoef-depth+1),dwtATTR);
    depth = depth - 1;
end

end

