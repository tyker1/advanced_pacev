function [ MatLo_D,MatHi_D ] = waveletFilterMatrix( N, Lo_D,Hi_D, Mat, dwtMode)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
Len = length(Lo_D);
if dwtMode == 'per'
    extMat=generateExtMat(N,Len/2,dwtMode);
else
    extMat = generateExtMat(N,Len-1,dwtMode);
end
LenX = size(extMat,1);
MatA_D = filt2Matrix(LenX,Lo_D,'valid');
MatD_D = filt2Matrix(LenX,Hi_D,'valid');
newLenX = size(MatA_D,1);
dwnMat = generateDMat(newLenX,Lo_D,dwtMode);

MatLo_D = dwnMat * MatA_D * extMat;
MatHi_D = dwnMat * MatD_D * extMat;

if ~isempty(Mat)
    MatLo_D = MatLo_D * Mat;
    MatHi_D = MatHi_D * Mat;
end

end

