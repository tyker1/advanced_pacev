function [envSound] = envelopeNoRecon(filtSig, numChannel, nodeLen,lowA, lowB, useTP)

envSound = zeros(numChannel, 1);

for i = 1:numChannel
    for j = 1:nodeLen
        if ~isempty(filtSig{i,j})
            %envSound(i) = envSound(i) + mean(abs(filtSig{i,j}));
            tpSig = abs(filtSig{i,j});
            if (useTP == 1)
                rectSig = abs(filtSig{i,j});
                tpSig = filtfilt(lowB, lowA, rectSig);
            end
            envSound(i) = envSound(i) + mean(tpSig);
        else
            continue;
        end
    end
end

end