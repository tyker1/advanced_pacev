function [ nodeValue,stack,newTree ] = wtGetNode( tree, C,L, cord, Lo_D,Hi_D, Lo_R, Hi_R )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

depth = cord(1);
idx = cord(2);
stack = [];
newTree = tree;

key = string(depth)+","+string(idx);

nodeValue = double(newTree.get(key));

if ~(isempty(nodeValue))
    return;
end

if idx < 2
    if idx == 0
        nodeValue = appcoef(C,L,Lo_R, Hi_R, depth);
    else
        nodeValue = detcoef(C,L, depth);
    end
    newTree.put(key,nodeValue);
    return;
end

stack = zeros(depth+1,2);
iter = 1;
startDepth = depth;
startOrd = idx;
stack(iter,:) = [startDepth,startOrd];
iter = iter + 1;
endIter = 0;
flag = 0;
while startOrd > 1
    startOrd = floor(startOrd/2);
    startDepth = startDepth - 1;
    stack(iter,:) = [startDepth, startOrd];
    key = string(startDepth)+","+string(startOrd);
    
    if (flag == 0) && (~isempty(newTree.get(key)))
        endIter = iter;
        flag = 1;
    end
    iter = iter + 1;
end
%%%
if endIter == 0
    iter = iter - 2;
    
    if startOrd == 0
        Sig = appcoef(C,L,Lo_R, Hi_R,startDepth);
    else
        Sig = detcoef(C,L,startDepth);
    end
    
    key = string(startDepth) + "," + string(startOrd);
    newTree.put(key,Sig);
else
    iter = endIter - 1;
    startDepth = stack(endIter,1);
    startOrd = stack(endIter,2);
    key = string(startDepth)+","+string(startOrd);
    Sig = double(newTree.get(key));
end

for i = iter:-1:1
    dep = stack(i,1);
    ord = stack(i,2);
    key = string(dep)+","+string(ord);
    key1 = "";
    [cA,cD] = dwt(Sig,Lo_D,Hi_D);
    if (mod(ord,2) == 0)
        Sig = cA;
        key1 = string(dep)+","+string(ord+1);
    else
        Sig = cD;
        key1 = key;
        key = string(dep)+","+string(ord-1);
    end
    newTree.put(key,cA);
    newTree.put(key1,cD);
end

nodeValue = Sig;

end

