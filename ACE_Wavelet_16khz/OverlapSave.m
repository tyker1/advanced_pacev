function [ OverlappedFrame ] = OverlapSave( Frame, WindowSize, FrameLen )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

persistent Buffer;

if isempty(Buffer)
    Buffer = zeros(1,WindowSize);
end

Buffer(end-FrameLen+1:end) = Frame;
OverlappedFrame = Buffer;

Buffer(1:end-FrameLen) = Buffer(FrameLen+1:end);

end

