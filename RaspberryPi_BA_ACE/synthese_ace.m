function [ sig ] = synthese_ace( elektrode2, SamplePerFrame, MidFreq, M )
%synthese_ace: Synthese Funktion f�r ACE-Strategie
%   Eingabe Parameter:
%   elektrode2 -- Elektrodeaktivit�ten von gesamter Simulation (eine L*M
%                 Matrix), L sind die insgamste Frames und M ist die Anzahl
%                 der verf�gbaren Kan�len. F�r ein bestimmtes Frame K
%                 sind elektrode2(k,:) die aktivit�ten von Elektrode, eine
%                 Elektrode E elektrode2(k,E) ist null wenn es in diesem
%                 Frame nicht aktiviert ist, oder enthalt eine Amplitude
%                 f�r die Impulse
%   SamplePerFrame -- SamplePerFrame entspricht die Anzahl der Samples pro
%                     Frame (Nicht die Analysefenster von FFT) definiert im
%                     init_ace.m, dient dazu ein Frame zu rekonstruieren
%   MidFreq      -- MidFreq ist ein Vektor f�r Mittelfrequenzen der
%                   Kan�len, die werden automatisch erzeugt beim jedem
%                   Simmulationsvorgang durch init_ace.m, bitte direkt die
%                   Variable MidFreq �bergeben
%   M            -- Anzahl der insgesamte verf�gbaren Elektrode, laut ACE
%                   Strategie werden normaleweise 22 Elektrode zur
%                   Verf�gung stehen. Diese Variable entspricht auch die
%                   Einstellung von init_ace.m

    L = length(elektrode2);

    sig = zeros(1,L*SamplePerFrame);
    fs = Samplefrequence;
    % tl = zeros(1,M);
    % tr = zeros(1,M);
    tl = zeros(1,M);

    for i = 1:L
        sig_sin =zeros(1,SamplePerFrame);
        for j = 1:M
            amp1 = elektrode2(i,j);
            if (i == L)
                amp2 = 0;
            else
                amp2 = elektrode2(i+1,j);
            end
            if (amp1 ~= 0) || (amp2 ~= 0)
                tr = tl(j) + SamplePerFrame/fs;
                t = linspace(tl(j),tr,SamplePerFrame);
                tl(j) = tr + 1/fs;
                temp_amp = amp_interp(amp1,amp2,SamplePerFrame);
                sig_sin = sig_sin + temp_amp.* sin(2*pi*MidFreq(j).*t);
            end
        end
        sig((i-1)*SamplePerFrame+1:i*SamplePerFrame) = sig_sin;
    end
end

function vect_amp = amp_interp(a1,a2,amp_N)

n = 0:amp_N-1;

delta = (a2-a1) / amp_N;

vect_amp = a1 + n.*delta;

end