/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: imohimohmglndjec_merge.h
 *
 * Code generated for Simulink model 'ace_try'.
 *
 * Model version                  : 1.57
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:12:51 2017
 */

#ifndef SHARE_imohimohmglndjec_merge
#define SHARE_imohimohmglndjec_merge
#include "rtwtypes.h"

extern void imohimohmglndjec_merge(int32_T idx[22], real_T x[22], int32_T offset,
  int32_T np, int32_T nq, int32_T iwork[22], real_T xwork[22]);

#endif

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
