/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: imohimohmglndjec_merge.c
 *
 * Code generated for Simulink model 'ace_try'.
 *
 * Model version                  : 1.57
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:12:51 2017
 */

#include "rtwtypes.h"
#include "imohimohmglndjec_merge.h"

/* Function for MATLAB Function: '<S1>/MATLAB Function' */
void imohimohmglndjec_merge(int32_T idx[22], real_T x[22], int32_T offset,
  int32_T np, int32_T nq, int32_T iwork[22], real_T xwork[22])
{
  int32_T n;
  int32_T q;
  int32_T qend;
  int32_T iout;
  int32_T exitg1;
  if (!((np == 0) || (nq == 0))) {
    n = np + nq;
    for (q = 0; q + 1 <= n; q++) {
      iwork[q] = idx[offset + q];
      xwork[q] = x[offset + q];
    }

    n = 0;
    q = np;
    qend = np + nq;
    iout = offset - 1;
    do {
      exitg1 = 0;
      iout++;
      if (xwork[n] >= xwork[q]) {
        idx[iout] = iwork[n];
        x[iout] = xwork[n];
        if (n + 1 < np) {
          n++;
        } else {
          exitg1 = 1;
        }
      } else {
        idx[iout] = iwork[q];
        x[iout] = xwork[q];
        if (q + 1 < qend) {
          q++;
        } else {
          q = iout - n;
          while (n + 1 <= np) {
            idx[(q + n) + 1] = iwork[n];
            x[(q + n) + 1] = xwork[n];
            n++;
          }

          exitg1 = 1;
        }
      }
    } while (exitg1 == 0);
  }
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
