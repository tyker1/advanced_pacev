/* Include files */

#include "ace_try_sfun.h"
#include "c2_ace_try.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "ace_try_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);
static const mxArray* sf_opaque_get_hover_data_for_msg(void *chartInstance,
  int32_T msgSSID);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c2_const_M                     (22.0)
#define c2_const_N                     (4.0)
#define c2_b_M                         (22.0)
#define c2_b_N                         (4.0)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c2_debug_family_names[12] = { "r_sqr", "a_env", "i", "M",
  "N", "nargin", "nargout", "u", "electrode", "index", "output", "env_out" };

/* Function Declarations */
static void initialize_c2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance);
static void initialize_params_c2_ace_try(SFc2_ace_tryInstanceStruct
  *chartInstance);
static void enable_c2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance);
static void disable_c2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance);
static void c2_update_debugger_state_c2_ace_try(SFc2_ace_tryInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c2_ace_try(SFc2_ace_tryInstanceStruct
  *chartInstance);
static void set_sim_state_c2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_st);
static void finalize_c2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance);
static void sf_gateway_c2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance);
static void mdl_start_c2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance);
static void initSimStructsc2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance);
static void c2_fftft_invoke(SFc2_ace_tryInstanceStruct *chartInstance, real_T
  c2_b_u[128], real_T c2_y[128]);
static void c2_fftEnv_invoke(SFc2_ace_tryInstanceStruct *chartInstance, real_T
  c2_b_u[128], real_T c2_y[22]);
static void c2_Sel_invoke(SFc2_ace_tryInstanceStruct *chartInstance, real_T
  c2_b_u[22], real_T c2_y[4], real_T c2_y1[4]);
static void init_script_number_translation(uint32_T c2_machineNumber, uint32_T
  c2_chartNumber, uint32_T c2_instanceNumber);
static const mxArray *c2_sf_marshallOut(void *chartInstanceVoid, void *c2_inData);
static void c2_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance, const
  mxArray *c2_b_env_out, const char_T *c2_identifier, real_T c2_y[22]);
static void c2_b_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId, real_T c2_y[22]);
static void c2_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_b_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static void c2_c_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_index, const char_T *c2_identifier, real_T c2_y[4]);
static void c2_d_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId, real_T c2_y[4]);
static void c2_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_c_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static const mxArray *c2_d_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static real_T c2_e_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_e_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static void c2_f_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId, real_T c2_y[128]);
static void c2_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_f_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static int32_T c2_g_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static uint8_T c2_h_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_is_active_c2_ace_try, const char_T *c2_identifier);
static uint8_T c2_i_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId);
static void init_dsm_address_info(SFc2_ace_tryInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc2_ace_tryInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    initSimStructsc2_ace_try(chartInstance);
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c2_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c2_is_active_c2_ace_try = 0U;
}

static void initialize_params_c2_ace_try(SFc2_ace_tryInstanceStruct
  *chartInstance)
{
  real_T c2_d0;
  real_T c2_d1;
  sf_mex_import_named("M", sf_mex_get_sfun_param(chartInstance->S, 0, 0), &c2_d0,
                      0, 0, 0U, 0, 0U, 0);
  chartInstance->c2_M = c2_d0;
  sf_mex_import_named("N", sf_mex_get_sfun_param(chartInstance->S, 1, 0), &c2_d1,
                      0, 0, 0U, 0, 0U, 0);
  chartInstance->c2_N = c2_d1;
}

static void enable_c2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c2_update_debugger_state_c2_ace_try(SFc2_ace_tryInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c2_ace_try(SFc2_ace_tryInstanceStruct
  *chartInstance)
{
  const mxArray *c2_st;
  const mxArray *c2_y = NULL;
  const mxArray *c2_b_y = NULL;
  const mxArray *c2_c_y = NULL;
  const mxArray *c2_d_y = NULL;
  const mxArray *c2_e_y = NULL;
  uint8_T c2_hoistedGlobal;
  const mxArray *c2_f_y = NULL;
  c2_st = NULL;
  c2_st = NULL;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_createcellmatrix(5, 1), false);
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", *chartInstance->c2_electrode, 0, 0U,
    1U, 0U, 1, 4), false);
  sf_mex_setcell(c2_y, 0, c2_b_y);
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", *chartInstance->c2_env_out, 0, 0U,
    1U, 0U, 1, 22), false);
  sf_mex_setcell(c2_y, 1, c2_c_y);
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", *chartInstance->c2_index, 0, 0U, 1U,
    0U, 1, 4), false);
  sf_mex_setcell(c2_y, 2, c2_d_y);
  c2_e_y = NULL;
  sf_mex_assign(&c2_e_y, sf_mex_create("y", *chartInstance->c2_output, 0, 0U, 1U,
    0U, 1, 22), false);
  sf_mex_setcell(c2_y, 3, c2_e_y);
  c2_hoistedGlobal = chartInstance->c2_is_active_c2_ace_try;
  c2_f_y = NULL;
  sf_mex_assign(&c2_f_y, sf_mex_create("y", &c2_hoistedGlobal, 3, 0U, 0U, 0U, 0),
                false);
  sf_mex_setcell(c2_y, 4, c2_f_y);
  sf_mex_assign(&c2_st, c2_y, false);
  return c2_st;
}

static void set_sim_state_c2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_st)
{
  const mxArray *c2_b_u;
  real_T c2_dv0[4];
  int32_T c2_i0;
  real_T c2_dv1[22];
  int32_T c2_i1;
  real_T c2_dv2[4];
  int32_T c2_i2;
  real_T c2_dv3[22];
  int32_T c2_i3;
  chartInstance->c2_doneDoubleBufferReInit = true;
  c2_b_u = sf_mex_dup(c2_st);
  c2_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell("electrode",
    c2_b_u, 0)), "electrode", c2_dv0);
  for (c2_i0 = 0; c2_i0 < 4; c2_i0++) {
    (*chartInstance->c2_electrode)[c2_i0] = c2_dv0[c2_i0];
  }

  c2_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell("env_out", c2_b_u,
    1)), "env_out", c2_dv1);
  for (c2_i1 = 0; c2_i1 < 22; c2_i1++) {
    (*chartInstance->c2_env_out)[c2_i1] = c2_dv1[c2_i1];
  }

  c2_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell("index", c2_b_u,
    2)), "index", c2_dv2);
  for (c2_i2 = 0; c2_i2 < 4; c2_i2++) {
    (*chartInstance->c2_index)[c2_i2] = c2_dv2[c2_i2];
  }

  c2_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell("output", c2_b_u,
    3)), "output", c2_dv3);
  for (c2_i3 = 0; c2_i3 < 22; c2_i3++) {
    (*chartInstance->c2_output)[c2_i3] = c2_dv3[c2_i3];
  }

  chartInstance->c2_is_active_c2_ace_try = c2_h_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("is_active_c2_ace_try", c2_b_u, 4)),
    "is_active_c2_ace_try");
  sf_mex_destroy(&c2_b_u);
  c2_update_debugger_state_c2_ace_try(chartInstance);
  sf_mex_destroy(&c2_st);
}

static void finalize_c2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance)
{
  int32_T c2_i4;
  int32_T c2_i5;
  uint32_T c2_debug_family_var_map[12];
  real_T c2_b_u[128];
  real_T c2_r_sqr[128];
  real_T c2_a_env[22];
  real_T c2_i;
  real_T c2_c_M;
  real_T c2_c_N;
  real_T c2_nargin = 3.0;
  real_T c2_nargout = 4.0;
  real_T c2_b_electrode[4];
  real_T c2_b_index[4];
  real_T c2_b_output[22];
  real_T c2_b_env_out[22];
  int32_T c2_i6;
  real_T c2_c_u[128];
  real_T c2_dv4[128];
  int32_T c2_i7;
  int32_T c2_i8;
  real_T c2_b_r_sqr[128];
  real_T c2_dv5[22];
  int32_T c2_i9;
  int32_T c2_i10;
  real_T c2_b_a_env[22];
  real_T c2_c_electrode[4];
  real_T c2_c_index[4];
  int32_T c2_i11;
  int32_T c2_i12;
  int32_T c2_i13;
  int32_T c2_i14;
  int32_T c2_b_i;
  int32_T c2_i15;
  int32_T c2_i16;
  int32_T c2_i17;
  int32_T c2_i18;
  int32_T c2_i19;
  int32_T c2_i20;
  int32_T c2_i21;
  int32_T c2_i22;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
  for (c2_i4 = 0; c2_i4 < 128; c2_i4++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c2_u)[c2_i4], 0U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
  }

  chartInstance->c2_sfEvent = CALL_EVENT;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
  for (c2_i5 = 0; c2_i5 < 128; c2_i5++) {
    c2_b_u[c2_i5] = (*chartInstance->c2_u)[c2_i5];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 12U, 12U, c2_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c2_r_sqr, 0U, c2_e_sf_marshallOut,
    c2_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c2_a_env, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_i, 2U, c2_d_sf_marshallOut,
    c2_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c2_c_M, 3U, c2_d_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c2_c_N, 4U, c2_d_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 5U, c2_d_sf_marshallOut,
    c2_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 6U, c2_d_sf_marshallOut,
    c2_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c2_b_u, 7U, c2_c_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c2_b_electrode, 8U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c2_b_index, 9U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c2_b_output, 10U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c2_b_env_out, 11U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  c2_c_N = c2_b_N;
  c2_c_M = c2_b_M;
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 3);
  for (c2_i6 = 0; c2_i6 < 128; c2_i6++) {
    c2_c_u[c2_i6] = c2_b_u[c2_i6];
  }

  c2_fftft_invoke(chartInstance, c2_c_u, c2_dv4);
  for (c2_i7 = 0; c2_i7 < 128; c2_i7++) {
    c2_r_sqr[c2_i7] = c2_dv4[c2_i7];
  }

  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 4);
  for (c2_i8 = 0; c2_i8 < 128; c2_i8++) {
    c2_b_r_sqr[c2_i8] = c2_r_sqr[c2_i8];
  }

  c2_fftEnv_invoke(chartInstance, c2_b_r_sqr, c2_dv5);
  for (c2_i9 = 0; c2_i9 < 22; c2_i9++) {
    c2_a_env[c2_i9] = c2_dv5[c2_i9];
  }

  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 5);
  for (c2_i10 = 0; c2_i10 < 22; c2_i10++) {
    c2_b_a_env[c2_i10] = c2_a_env[c2_i10];
  }

  c2_Sel_invoke(chartInstance, c2_b_a_env, c2_c_electrode, c2_c_index);
  for (c2_i11 = 0; c2_i11 < 4; c2_i11++) {
    c2_b_electrode[c2_i11] = c2_c_electrode[c2_i11];
  }

  for (c2_i12 = 0; c2_i12 < 4; c2_i12++) {
    c2_b_index[c2_i12] = c2_c_index[c2_i12];
  }

  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 7);
  for (c2_i13 = 0; c2_i13 < 22; c2_i13++) {
    c2_b_output[c2_i13] = 0.0;
  }

  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 8);
  for (c2_i14 = 0; c2_i14 < 22; c2_i14++) {
    c2_b_env_out[c2_i14] = c2_a_env[c2_i14];
  }

  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 9);
  c2_i = 1.0;
  c2_b_i = 0;
  while (c2_b_i < 4) {
    c2_i = 1.0 + (real_T)c2_b_i;
    CV_EML_FOR(0, 1, 0, 1);
    _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 10);
    c2_b_output[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 275, 16, MAX_uint32_T, (int32_T)sf_integer_check
      (chartInstance->S, 1U, 275U, 16U, c2_b_index[sf_eml_array_bounds_check
       (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 275, 16, MAX_uint32_T,
        (int32_T)sf_integer_check(chartInstance->S, 1U, 275U, 16U, c2_i), 1, 4)
       - 1]), 1, 22) - 1] = c2_b_electrode[sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 294, 12, MAX_uint32_T,
       (int32_T)sf_integer_check(chartInstance->S, 1U, 294U, 12U, c2_i), 1, 4) -
      1];
    c2_b_i++;
    _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
  }

  CV_EML_FOR(0, 1, 0, 0);
  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, -10);
  _SFD_SYMBOL_SCOPE_POP();
  for (c2_i15 = 0; c2_i15 < 4; c2_i15++) {
    (*chartInstance->c2_electrode)[c2_i15] = c2_b_electrode[c2_i15];
  }

  for (c2_i16 = 0; c2_i16 < 4; c2_i16++) {
    (*chartInstance->c2_index)[c2_i16] = c2_b_index[c2_i16];
  }

  for (c2_i17 = 0; c2_i17 < 22; c2_i17++) {
    (*chartInstance->c2_output)[c2_i17] = c2_b_output[c2_i17];
  }

  for (c2_i18 = 0; c2_i18 < 22; c2_i18++) {
    (*chartInstance->c2_env_out)[c2_i18] = c2_b_env_out[c2_i18];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_ace_tryMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  for (c2_i19 = 0; c2_i19 < 4; c2_i19++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c2_electrode)[c2_i19], 1U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
  }

  for (c2_i20 = 0; c2_i20 < 4; c2_i20++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c2_index)[c2_i20], 2U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
  }

  for (c2_i21 = 0; c2_i21 < 22; c2_i21++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c2_output)[c2_i21], 3U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
  }

  for (c2_i22 = 0; c2_i22 < 22; c2_i22++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c2_env_out)[c2_i22], 4U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
  }
}

static void mdl_start_c2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc2_ace_try(SFc2_ace_tryInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c2_fftft_invoke(SFc2_ace_tryInstanceStruct *chartInstance, real_T
  c2_b_u[128], real_T c2_y[128])
{
  _ssFcnCallExecArgInfo c2_args[2];
  c2_args[0U].dataPtr = (void *)c2_b_u;
  c2_args[1U].dataPtr = (void *)c2_y;
  slcsInvokeSimulinkFunction(chartInstance->S, "fftft", &c2_args[0U]);
}

static void c2_fftEnv_invoke(SFc2_ace_tryInstanceStruct *chartInstance, real_T
  c2_b_u[128], real_T c2_y[22])
{
  _ssFcnCallExecArgInfo c2_args[2];
  c2_args[0U].dataPtr = (void *)c2_b_u;
  c2_args[1U].dataPtr = (void *)c2_y;
  slcsInvokeSimulinkFunction(chartInstance->S, "fftEnv", &c2_args[0U]);
}

static void c2_Sel_invoke(SFc2_ace_tryInstanceStruct *chartInstance, real_T
  c2_b_u[22], real_T c2_y[4], real_T c2_y1[4])
{
  _ssFcnCallExecArgInfo c2_args[3];
  c2_args[0U].dataPtr = (void *)c2_b_u;
  c2_args[1U].dataPtr = (void *)c2_y;
  c2_args[2U].dataPtr = (void *)c2_y1;
  slcsInvokeSimulinkFunction(chartInstance->S, "Sel", &c2_args[0U]);
}

static void init_script_number_translation(uint32_T c2_machineNumber, uint32_T
  c2_chartNumber, uint32_T c2_instanceNumber)
{
  (void)(c2_machineNumber);
  (void)(c2_chartNumber);
  (void)(c2_instanceNumber);
}

static const mxArray *c2_sf_marshallOut(void *chartInstanceVoid, void *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  int32_T c2_i23;
  const mxArray *c2_y = NULL;
  real_T c2_b_u[22];
  SFc2_ace_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_ace_tryInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  for (c2_i23 = 0; c2_i23 < 22; c2_i23++) {
    c2_b_u[c2_i23] = (*(real_T (*)[22])c2_inData)[c2_i23];
  }

  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", c2_b_u, 0, 0U, 1U, 0U, 1, 22), false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static void c2_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance, const
  mxArray *c2_b_env_out, const char_T *c2_identifier, real_T c2_y[22])
{
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_env_out), &c2_thisId,
                        c2_y);
  sf_mex_destroy(&c2_b_env_out);
}

static void c2_b_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId, real_T c2_y[22])
{
  real_T c2_dv6[22];
  int32_T c2_i24;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_b_u), c2_dv6, 1, 0, 0U, 1, 0U, 1, 22);
  for (c2_i24 = 0; c2_i24 < 22; c2_i24++) {
    c2_y[c2_i24] = c2_dv6[c2_i24];
  }

  sf_mex_destroy(&c2_b_u);
}

static void c2_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_b_env_out;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  real_T c2_y[22];
  int32_T c2_i25;
  SFc2_ace_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_ace_tryInstanceStruct *)chartInstanceVoid;
  c2_b_env_out = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_env_out), &c2_thisId,
                        c2_y);
  sf_mex_destroy(&c2_b_env_out);
  for (c2_i25 = 0; c2_i25 < 22; c2_i25++) {
    (*(real_T (*)[22])c2_outData)[c2_i25] = c2_y[c2_i25];
  }

  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_b_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  int32_T c2_i26;
  const mxArray *c2_y = NULL;
  real_T c2_b_u[4];
  SFc2_ace_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_ace_tryInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  for (c2_i26 = 0; c2_i26 < 4; c2_i26++) {
    c2_b_u[c2_i26] = (*(real_T (*)[4])c2_inData)[c2_i26];
  }

  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", c2_b_u, 0, 0U, 1U, 0U, 1, 4), false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static void c2_c_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_index, const char_T *c2_identifier, real_T c2_y[4])
{
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_index), &c2_thisId, c2_y);
  sf_mex_destroy(&c2_b_index);
}

static void c2_d_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId, real_T c2_y[4])
{
  real_T c2_dv7[4];
  int32_T c2_i27;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_b_u), c2_dv7, 1, 0, 0U, 1, 0U, 1, 4);
  for (c2_i27 = 0; c2_i27 < 4; c2_i27++) {
    c2_y[c2_i27] = c2_dv7[c2_i27];
  }

  sf_mex_destroy(&c2_b_u);
}

static void c2_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_b_index;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  real_T c2_y[4];
  int32_T c2_i28;
  SFc2_ace_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_ace_tryInstanceStruct *)chartInstanceVoid;
  c2_b_index = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_index), &c2_thisId, c2_y);
  sf_mex_destroy(&c2_b_index);
  for (c2_i28 = 0; c2_i28 < 4; c2_i28++) {
    (*(real_T (*)[4])c2_outData)[c2_i28] = c2_y[c2_i28];
  }

  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_c_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  int32_T c2_i29;
  const mxArray *c2_y = NULL;
  real_T c2_b_u[128];
  SFc2_ace_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_ace_tryInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  for (c2_i29 = 0; c2_i29 < 128; c2_i29++) {
    c2_b_u[c2_i29] = (*(real_T (*)[128])c2_inData)[c2_i29];
  }

  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", c2_b_u, 0, 0U, 1U, 0U, 2, 128, 1),
                false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static const mxArray *c2_d_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  real_T c2_b_u;
  const mxArray *c2_y = NULL;
  SFc2_ace_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_ace_tryInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  c2_b_u = *(real_T *)c2_inData;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_b_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static real_T c2_e_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId)
{
  real_T c2_y;
  real_T c2_d2;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_b_u), &c2_d2, 1, 0, 0U, 0, 0U, 0);
  c2_y = c2_d2;
  sf_mex_destroy(&c2_b_u);
  return c2_y;
}

static void c2_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_nargout;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  real_T c2_y;
  SFc2_ace_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_ace_tryInstanceStruct *)chartInstanceVoid;
  c2_nargout = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_e_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_nargout), &c2_thisId);
  sf_mex_destroy(&c2_nargout);
  *(real_T *)c2_outData = c2_y;
  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_e_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  int32_T c2_i30;
  const mxArray *c2_y = NULL;
  real_T c2_b_u[128];
  SFc2_ace_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_ace_tryInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  for (c2_i30 = 0; c2_i30 < 128; c2_i30++) {
    c2_b_u[c2_i30] = (*(real_T (*)[128])c2_inData)[c2_i30];
  }

  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", c2_b_u, 0, 0U, 1U, 0U, 1, 128), false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static void c2_f_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId, real_T c2_y[128])
{
  real_T c2_dv8[128];
  int32_T c2_i31;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_b_u), c2_dv8, 1, 0, 0U, 1, 0U, 1, 128);
  for (c2_i31 = 0; c2_i31 < 128; c2_i31++) {
    c2_y[c2_i31] = c2_dv8[c2_i31];
  }

  sf_mex_destroy(&c2_b_u);
}

static void c2_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_r_sqr;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  real_T c2_y[128];
  int32_T c2_i32;
  SFc2_ace_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_ace_tryInstanceStruct *)chartInstanceVoid;
  c2_r_sqr = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_r_sqr), &c2_thisId, c2_y);
  sf_mex_destroy(&c2_r_sqr);
  for (c2_i32 = 0; c2_i32 < 128; c2_i32++) {
    (*(real_T (*)[128])c2_outData)[c2_i32] = c2_y[c2_i32];
  }

  sf_mex_destroy(&c2_mxArrayInData);
}

const mxArray *sf_c2_ace_try_get_eml_resolved_functions_info(void)
{
  const mxArray *c2_nameCaptureInfo = NULL;
  c2_nameCaptureInfo = NULL;
  sf_mex_assign(&c2_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c2_nameCaptureInfo;
}

static const mxArray *c2_f_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  int32_T c2_b_u;
  const mxArray *c2_y = NULL;
  SFc2_ace_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_ace_tryInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  c2_b_u = *(int32_T *)c2_inData;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_b_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static int32_T c2_g_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId)
{
  int32_T c2_y;
  int32_T c2_i33;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_b_u), &c2_i33, 1, 6, 0U, 0, 0U, 0);
  c2_y = c2_i33;
  sf_mex_destroy(&c2_b_u);
  return c2_y;
}

static void c2_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_b_sfEvent;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  int32_T c2_y;
  SFc2_ace_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_ace_tryInstanceStruct *)chartInstanceVoid;
  c2_b_sfEvent = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_sfEvent),
    &c2_thisId);
  sf_mex_destroy(&c2_b_sfEvent);
  *(int32_T *)c2_outData = c2_y;
  sf_mex_destroy(&c2_mxArrayInData);
}

static uint8_T c2_h_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_is_active_c2_ace_try, const char_T *c2_identifier)
{
  uint8_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_i_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_b_is_active_c2_ace_try), &c2_thisId);
  sf_mex_destroy(&c2_b_is_active_c2_ace_try);
  return c2_y;
}

static uint8_T c2_i_emlrt_marshallIn(SFc2_ace_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId)
{
  uint8_T c2_y;
  uint8_T c2_u0;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_b_u), &c2_u0, 1, 3, 0U, 0, 0U, 0);
  c2_y = c2_u0;
  sf_mex_destroy(&c2_b_u);
  return c2_y;
}

static void init_dsm_address_info(SFc2_ace_tryInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc2_ace_tryInstanceStruct *chartInstance)
{
  chartInstance->c2_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c2_u = (real_T (*)[128])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c2_electrode = (real_T (*)[4])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c2_index = (real_T (*)[4])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c2_output = (real_T (*)[22])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c2_env_out = (real_T (*)[22])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 4);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c2_ace_try_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1003499029U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(1080119364U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(1034209463U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(4282941764U);
}

mxArray* sf_c2_ace_try_get_post_codegen_info(void);
mxArray *sf_c2_ace_try_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("SjkMk1ZrQuzb83sNLUS55B");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(128);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,4,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,1,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(4);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,1,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(4);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,1,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(22);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,1,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(22);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c2_ace_try_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c2_ace_try_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c2_ace_try_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("late");
  mxArray *fallbackReason = mxCreateString("client_server");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("Sel");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c2_ace_try_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c2_ace_try_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString(
      "auSA6HhoCzapQWZVqTGwtG");
    mwSize exp_dims[2] = { 3, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);

    {
      mxArray* mxFcnName = mxCreateString("Sel");
      mxSetCell(mxExportedFunctionsUsedByThisChart, 0, mxFcnName);
    }

    {
      mxArray* mxFcnName = mxCreateString("fftEnv");
      mxSetCell(mxExportedFunctionsUsedByThisChart, 1, mxFcnName);
    }

    {
      mxArray* mxFcnName = mxCreateString("fftft");
      mxSetCell(mxExportedFunctionsUsedByThisChart, 2, mxFcnName);
    }

    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c2_ace_try(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x5'type','srcId','name','auxInfo'{{M[1],M[5],T\"electrode\",},{M[1],M[10],T\"env_out\",},{M[1],M[6],T\"index\",},{M[1],M[7],T\"output\",},{M[8],M[0],T\"is_active_c2_ace_try\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 5, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c2_ace_try_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc2_ace_tryInstanceStruct *chartInstance = (SFc2_ace_tryInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _ace_tryMachineNumber_,
           2,
           1,
           1,
           0,
           7,
           0,
           0,
           0,
           0,
           0,
           &chartInstance->chartNumber,
           &chartInstance->instanceNumber,
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_ace_tryMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_ace_tryMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _ace_tryMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"u");
          _SFD_SET_DATA_PROPS(1,2,0,1,"electrode");
          _SFD_SET_DATA_PROPS(2,2,0,1,"index");
          _SFD_SET_DATA_PROPS(3,2,0,1,"output");
          _SFD_SET_DATA_PROPS(4,2,0,1,"env_out");
          _SFD_SET_DATA_PROPS(5,10,0,0,"M");
          _SFD_SET_DATA_PROPS(6,10,0,0,"N");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,0,0,0,0,1,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,311);
        _SFD_CV_INIT_EML_FOR(0,1,0,259,271,311);

        {
          unsigned int dimVector[2];
          dimVector[0]= 128U;
          dimVector[1]= 1U;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c2_c_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 4U;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c2_b_sf_marshallOut,(MexInFcnForType)
            c2_b_sf_marshallIn);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 4U;
          _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c2_b_sf_marshallOut,(MexInFcnForType)
            c2_b_sf_marshallIn);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 22U;
          _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)
            c2_sf_marshallIn);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 22U;
          _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)
            c2_sf_marshallIn);
        }

        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_d_sf_marshallOut,(MexInFcnForType)c2_c_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_d_sf_marshallOut,(MexInFcnForType)c2_c_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _ace_tryMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc2_ace_tryInstanceStruct *chartInstance = (SFc2_ace_tryInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(0U, (void *)chartInstance->c2_u);
        _SFD_SET_DATA_VALUE_PTR(1U, (void *)chartInstance->c2_electrode);
        _SFD_SET_DATA_VALUE_PTR(2U, (void *)chartInstance->c2_index);
        _SFD_SET_DATA_VALUE_PTR(3U, (void *)chartInstance->c2_output);
        _SFD_SET_DATA_VALUE_PTR(5U, (void *)&chartInstance->c2_M);
        _SFD_SET_DATA_VALUE_PTR(6U, (void *)&chartInstance->c2_N);
        _SFD_SET_DATA_VALUE_PTR(4U, (void *)chartInstance->c2_env_out);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sERzacJZiH0QLq3Y8tLXVrH";
}

static void sf_opaque_initialize_c2_ace_try(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc2_ace_tryInstanceStruct*) chartInstanceVar)->S,
    0);
  initialize_params_c2_ace_try((SFc2_ace_tryInstanceStruct*) chartInstanceVar);
  initialize_c2_ace_try((SFc2_ace_tryInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c2_ace_try(void *chartInstanceVar)
{
  enable_c2_ace_try((SFc2_ace_tryInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c2_ace_try(void *chartInstanceVar)
{
  disable_c2_ace_try((SFc2_ace_tryInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c2_ace_try(void *chartInstanceVar)
{
  sf_gateway_c2_ace_try((SFc2_ace_tryInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c2_ace_try(SimStruct* S)
{
  return get_sim_state_c2_ace_try((SFc2_ace_tryInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c2_ace_try(SimStruct* S, const mxArray *st)
{
  set_sim_state_c2_ace_try((SFc2_ace_tryInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_terminate_c2_ace_try(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc2_ace_tryInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_ace_try_optimization_info();
    }

    finalize_c2_ace_try((SFc2_ace_tryInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc2_ace_try((SFc2_ace_tryInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c2_ace_try(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c2_ace_try((SFc2_ace_tryInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

static void mdlSetWorkWidths_c2_ace_try(SimStruct *S)
{
  /* Actual parameters from chart:
     M N
   */
  const char_T *rtParamNames[] = { "M", "N" };

  ssSetNumRunTimeParams(S,ssGetSFcnParamsCount(S));

  /* registration for M*/
  ssRegDlgParamAsRunTimeParam(S, 0, 0, rtParamNames[0], SS_DOUBLE);

  /* registration for N*/
  ssRegDlgParamAsRunTimeParam(S, 1, 1, rtParamNames[1], SS_DOUBLE);

  /* Set overwritable ports for inplace optimization */
  ssSetStatesModifiedOnlyInUpdate(S, 0);
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_ace_try_optimization_info(sim_mode_is_rtw_gen(S),
      sim_mode_is_modelref_sim(S), sim_mode_is_external(S));
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,2);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,2,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_set_chart_accesses_machine_info(S, sf_get_instance_specialization(),
      infoStruct, 2);
    sf_update_buildInfo(S, sf_get_instance_specialization(),infoStruct,2);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,2,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,2,4);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=4; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,2);
    sf_register_codegen_names_for_scoped_functions_defined_by_chart(S);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(2444531814U));
  ssSetChecksum1(S,(1773960703U));
  ssSetChecksum2(S,(497650186U));
  ssSetChecksum3(S,(2871295921U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSetStateSemanticsClassicAndSynchronous(S, true);
  ssSupportsMultipleExecInstances(S,0);
}

static void mdlRTW_c2_ace_try(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c2_ace_try(SimStruct *S)
{
  SFc2_ace_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_ace_tryInstanceStruct *)utMalloc(sizeof
    (SFc2_ace_tryInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc2_ace_tryInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c2_ace_try;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c2_ace_try;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c2_ace_try;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c2_ace_try;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c2_ace_try;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c2_ace_try;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c2_ace_try;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c2_ace_try;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c2_ace_try;
  chartInstance->chartInfo.mdlStart = mdlStart_c2_ace_try;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c2_ace_try;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
  mdl_start_c2_ace_try(chartInstance);
}

void c2_ace_try_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c2_ace_try(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c2_ace_try(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c2_ace_try(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c2_ace_try_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
