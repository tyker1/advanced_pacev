/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: Sel.c
 *
 * Code generated for Simulink model 'ace_try'.
 *
 * Model version                  : 1.57
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:12:51 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Sel.h"

/* Include model header file for global data */
#include "ace_try.h"
#include "ace_try_private.h"
#include "imohimohmglndjec_merge.h"

/* Forward declaration for local functions */
static void ace_try_baiedbimhlfcmopp_sort(real_T x[22], int32_T idx[22]);
static void ace_try_jecbimgldbaamglf_sort(real_T x[22], int32_T idx[22]);

/* Function for MATLAB Function: '<S1>/MATLAB Function' */
static void ace_try_baiedbimhlfcmopp_sort(real_T x[22], int32_T idx[22])
{
  int32_T nNaNs;
  int8_T idx4[4];
  int32_T ib;
  int32_T m;
  int8_T perm[4];
  int32_T i3;
  int32_T i4;
  int32_T tailOffset;
  int32_T nTail;
  ace_try_B.x4[0] = 0.0;
  idx4[0] = 0;
  ace_try_B.x4[1] = 0.0;
  idx4[1] = 0;
  ace_try_B.x4[2] = 0.0;
  idx4[2] = 0;
  ace_try_B.x4[3] = 0.0;
  idx4[3] = 0;
  memset(&idx[0], 0, 22U * sizeof(int32_T));
  memset(&ace_try_B.xwork[0], 0, 22U * sizeof(real_T));
  nNaNs = 0;
  ib = 0;
  for (m = 0; m < 22; m++) {
    if (rtIsNaN(x[m])) {
      idx[21 - nNaNs] = m + 1;
      ace_try_B.xwork[21 - nNaNs] = x[m];
      nNaNs++;
    } else {
      ib++;
      idx4[ib - 1] = (int8_T)(m + 1);
      ace_try_B.x4[ib - 1] = x[m];
      if (ib == 4) {
        ib = m - nNaNs;
        if (ace_try_B.x4[0] >= ace_try_B.x4[1]) {
          tailOffset = 1;
          nTail = 2;
        } else {
          tailOffset = 2;
          nTail = 1;
        }

        if (ace_try_B.x4[2] >= ace_try_B.x4[3]) {
          i3 = 3;
          i4 = 4;
        } else {
          i3 = 4;
          i4 = 3;
        }

        if (ace_try_B.x4[tailOffset - 1] >= ace_try_B.x4[i3 - 1]) {
          if (ace_try_B.x4[nTail - 1] >= ace_try_B.x4[i3 - 1]) {
            perm[0] = (int8_T)tailOffset;
            perm[1] = (int8_T)nTail;
            perm[2] = (int8_T)i3;
            perm[3] = (int8_T)i4;
          } else if (ace_try_B.x4[nTail - 1] >= ace_try_B.x4[i4 - 1]) {
            perm[0] = (int8_T)tailOffset;
            perm[1] = (int8_T)i3;
            perm[2] = (int8_T)nTail;
            perm[3] = (int8_T)i4;
          } else {
            perm[0] = (int8_T)tailOffset;
            perm[1] = (int8_T)i3;
            perm[2] = (int8_T)i4;
            perm[3] = (int8_T)nTail;
          }
        } else if (ace_try_B.x4[tailOffset - 1] >= ace_try_B.x4[i4 - 1]) {
          if (ace_try_B.x4[nTail - 1] >= ace_try_B.x4[i4 - 1]) {
            perm[0] = (int8_T)i3;
            perm[1] = (int8_T)tailOffset;
            perm[2] = (int8_T)nTail;
            perm[3] = (int8_T)i4;
          } else {
            perm[0] = (int8_T)i3;
            perm[1] = (int8_T)tailOffset;
            perm[2] = (int8_T)i4;
            perm[3] = (int8_T)nTail;
          }
        } else {
          perm[0] = (int8_T)i3;
          perm[1] = (int8_T)i4;
          perm[2] = (int8_T)tailOffset;
          perm[3] = (int8_T)nTail;
        }

        idx[ib - 3] = idx4[perm[0] - 1];
        idx[ib - 2] = idx4[perm[1] - 1];
        idx[ib - 1] = idx4[perm[2] - 1];
        idx[ib] = idx4[perm[3] - 1];
        x[ib - 3] = ace_try_B.x4[perm[0] - 1];
        x[ib - 2] = ace_try_B.x4[perm[1] - 1];
        x[ib - 1] = ace_try_B.x4[perm[2] - 1];
        x[ib] = ace_try_B.x4[perm[3] - 1];
        ib = 0;
      }
    }
  }

  if (ib > 0) {
    perm[1] = 0;
    perm[2] = 0;
    perm[3] = 0;
    switch (ib) {
     case 1:
      perm[0] = 1;
      break;

     case 2:
      if (ace_try_B.x4[0] >= ace_try_B.x4[1]) {
        perm[0] = 1;
        perm[1] = 2;
      } else {
        perm[0] = 2;
        perm[1] = 1;
      }
      break;

     default:
      if (ace_try_B.x4[0] >= ace_try_B.x4[1]) {
        if (ace_try_B.x4[1] >= ace_try_B.x4[2]) {
          perm[0] = 1;
          perm[1] = 2;
          perm[2] = 3;
        } else if (ace_try_B.x4[0] >= ace_try_B.x4[2]) {
          perm[0] = 1;
          perm[1] = 3;
          perm[2] = 2;
        } else {
          perm[0] = 3;
          perm[1] = 1;
          perm[2] = 2;
        }
      } else if (ace_try_B.x4[0] >= ace_try_B.x4[2]) {
        perm[0] = 2;
        perm[1] = 1;
        perm[2] = 3;
      } else if (ace_try_B.x4[1] >= ace_try_B.x4[2]) {
        perm[0] = 2;
        perm[1] = 3;
        perm[2] = 1;
      } else {
        perm[0] = 3;
        perm[1] = 2;
        perm[2] = 1;
      }
      break;
    }

    for (m = 22; m - 21 <= ib; m++) {
      idx[(m - nNaNs) - ib] = idx4[perm[m - 22] - 1];
      x[(m - nNaNs) - ib] = ace_try_B.x4[perm[m - 22] - 1];
    }
  }

  m = nNaNs >> 1;
  for (ib = 1; ib <= m; ib++) {
    tailOffset = idx[(ib - nNaNs) + 21];
    idx[(ib - nNaNs) + 21] = idx[22 - ib];
    idx[22 - ib] = tailOffset;
    x[(ib - nNaNs) + 21] = ace_try_B.xwork[22 - ib];
    x[22 - ib] = ace_try_B.xwork[(ib - nNaNs) + 21];
  }

  if ((nNaNs & 1) != 0) {
    x[(m - nNaNs) + 22] = ace_try_B.xwork[(m - nNaNs) + 22];
  }

  memset(&ace_try_B.iwork[0], 0, 22U * sizeof(int32_T));
  if (22 - nNaNs > 1) {
    memset(&ace_try_B.iwork[0], 0, 22U * sizeof(int32_T));
    ib = (22 - nNaNs) >> 2;
    m = 4;
    while (ib > 1) {
      if ((ib & 1) != 0) {
        ib--;
        tailOffset = m * ib;
        nTail = 22 - (nNaNs + tailOffset);
        if (nTail > m) {
          imohimohmglndjec_merge(idx, x, tailOffset, m, nTail - m,
            ace_try_B.iwork, ace_try_B.xwork);
        }
      }

      tailOffset = m << 1;
      ib >>= 1;
      for (nTail = 1; nTail <= ib; nTail++) {
        imohimohmglndjec_merge(idx, x, (nTail - 1) * tailOffset, m, m,
          ace_try_B.iwork, ace_try_B.xwork);
      }

      m = tailOffset;
    }

    if (22 - nNaNs > m) {
      imohimohmglndjec_merge(idx, x, 0, m, 22 - (nNaNs + m), ace_try_B.iwork,
        ace_try_B.xwork);
    }
  }

  if ((nNaNs > 0) && (22 - nNaNs > 0)) {
    for (m = 22; m - 21 <= nNaNs; m++) {
      ace_try_B.xwork[m - 22] = x[m - nNaNs];
      ace_try_B.iwork[m - 22] = idx[m - nNaNs];
    }

    for (m = 21 - nNaNs; m + 1 > 0; m--) {
      x[nNaNs + m] = x[m];
      idx[nNaNs + m] = idx[m];
    }

    for (m = 0; m + 1 <= nNaNs; m++) {
      x[m] = ace_try_B.xwork[m];
      idx[m] = ace_try_B.iwork[m];
    }
  }
}

/* Function for MATLAB Function: '<S1>/MATLAB Function' */
static void ace_try_jecbimgldbaamglf_sort(real_T x[22], int32_T idx[22])
{
  ace_try_baiedbimhlfcmopp_sort(x, idx);
}

/* Output and update for Simulink Function: '<Root>/Bandselection' */
void Sel(const real_T rtu_u[22], real_T rty_y[4], real_T rty_y1[4])
{
  /* MATLAB Function: '<S1>/MATLAB Function' incorporates:
   *  SignalConversion: '<S1>/TmpSignal ConversionAtuOutport1'
   */
  /* MATLAB Function 'Bandselection/MATLAB Function': '<S5>:1' */
  /* '<S5>:1:3' [sortedArray, IndexBeforeSort] = sort(a,'descend'); */
  memcpy(&ace_try_B.x[0], &rtu_u[0], 22U * sizeof(real_T));
  ace_try_jecbimgldbaamglf_sort(ace_try_B.x, ace_try_B.iidx);

  /* SignalConversion: '<S1>/TmpSignal ConversionAtyInport1' incorporates:
   *  MATLAB Function: '<S1>/MATLAB Function'
   */
  /* '<S5>:1:5' outAmp = sortedArray(1:N); */
  /* '<S5>:1:6' outIndex = IndexBeforeSort(1:N); */
  rty_y[0] = ace_try_B.x[0];

  /* SignalConversion: '<S1>/TmpSignal ConversionAty1Inport1' incorporates:
   *  MATLAB Function: '<S1>/MATLAB Function'
   */
  rty_y1[0] = ace_try_B.iidx[0];

  /* SignalConversion: '<S1>/TmpSignal ConversionAtyInport1' incorporates:
   *  MATLAB Function: '<S1>/MATLAB Function'
   */
  rty_y[1] = ace_try_B.x[1];

  /* SignalConversion: '<S1>/TmpSignal ConversionAty1Inport1' incorporates:
   *  MATLAB Function: '<S1>/MATLAB Function'
   */
  rty_y1[1] = ace_try_B.iidx[1];

  /* SignalConversion: '<S1>/TmpSignal ConversionAtyInport1' incorporates:
   *  MATLAB Function: '<S1>/MATLAB Function'
   */
  rty_y[2] = ace_try_B.x[2];

  /* SignalConversion: '<S1>/TmpSignal ConversionAty1Inport1' incorporates:
   *  MATLAB Function: '<S1>/MATLAB Function'
   */
  rty_y1[2] = ace_try_B.iidx[2];

  /* SignalConversion: '<S1>/TmpSignal ConversionAtyInport1' incorporates:
   *  MATLAB Function: '<S1>/MATLAB Function'
   */
  rty_y[3] = ace_try_B.x[3];

  /* SignalConversion: '<S1>/TmpSignal ConversionAty1Inport1' incorporates:
   *  MATLAB Function: '<S1>/MATLAB Function'
   */
  rty_y1[3] = ace_try_B.iidx[3];
  ace_try_DW.Bandselection_SubsysRanBC = 4;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
