/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: ace_try.c
 *
 * Code generated for Simulink model 'ace_try'.
 *
 * Model version                  : 1.57
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:12:51 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ace_try.h"
#include "ace_try_private.h"
#include "ace_try_dt.h"

/* Block signals (auto storage) */
B_ace_try_T ace_try_B;

/* Block states (auto storage) */
DW_ace_try_T ace_try_DW;

/* Real-time model */
RT_MODEL_ace_try_T ace_try_M_;
RT_MODEL_ace_try_T *const ace_try_M = &ace_try_M_;

/* Model step function */
void ace_try_step(void)
{
  /* Reset subsysRan breadcrumbs */
  srClearBC(ace_try_DW.Bandselection_SubsysRanBC);

  /* Reset subsysRan breadcrumbs */
  srClearBC(ace_try_DW.FFTFilterbank_SubsysRanBC);

  /* Reset subsysRan breadcrumbs */
  srClearBC(ace_try_DW.SpecturalEnvlopeDetection_Subsy);

  /* S-Function (alsa_audio_capture_sfcn): '<Root>/ALSA Audio Capture' */
  audioCapture(rtCP_ALSAAudioCapture_p1, ace_try_B.ALSAAudioCapture, 16U);
  for (ace_try_B.i = 0; ace_try_B.i < 16; ace_try_B.i++) {
    /* Gain: '<Root>/Gain' incorporates:
     *  S-Function (sdspmultiportsel): '<Root>/Multiport Selector'
     */
    ace_try_B.Gain[ace_try_B.i] = ace_try_P.Gain_Gain * (real_T)
      ace_try_B.ALSAAudioCapture[ace_try_B.i];
  }

  /* Buffer: '<Root>/Buffer' */
  ace_try_B.i = 0;
  while (ace_try_B.i < 112 - ace_try_DW.Buffer_CircBufIdx) {
    ace_try_B.Buffer[ace_try_B.i] =
      ace_try_DW.Buffer_CircBuff[ace_try_DW.Buffer_CircBufIdx + ace_try_B.i];
    ace_try_B.i++;
  }

  ace_try_B.yIdx = 112 - ace_try_DW.Buffer_CircBufIdx;
  ace_try_B.i = 0;
  while (ace_try_B.i < ace_try_DW.Buffer_CircBufIdx) {
    ace_try_B.Buffer[ace_try_B.yIdx + ace_try_B.i] =
      ace_try_DW.Buffer_CircBuff[ace_try_B.i];
    ace_try_B.i++;
  }

  ace_try_B.yIdx += ace_try_DW.Buffer_CircBufIdx;
  memcpy(&ace_try_B.Buffer[ace_try_B.yIdx], &ace_try_B.Gain[0], sizeof(real_T) <<
         4U);
  if (16 > 112 - ace_try_DW.Buffer_CircBufIdx) {
    ace_try_B.i = 0;
    while (ace_try_B.i < 112 - ace_try_DW.Buffer_CircBufIdx) {
      ace_try_DW.Buffer_CircBuff[ace_try_DW.Buffer_CircBufIdx + ace_try_B.i] =
        ace_try_B.Gain[ace_try_B.i];
      ace_try_B.i++;
    }

    ace_try_B.yIdx = -ace_try_DW.Buffer_CircBufIdx;
    ace_try_B.i = 0;
    while (ace_try_B.i < ace_try_DW.Buffer_CircBufIdx - 96) {
      ace_try_DW.Buffer_CircBuff[ace_try_B.i] = ace_try_B.Gain[(ace_try_B.yIdx +
        ace_try_B.i) + 112];
      ace_try_B.i++;
    }
  } else {
    for (ace_try_B.i = 0; ace_try_B.i < 16; ace_try_B.i++) {
      ace_try_DW.Buffer_CircBuff[ace_try_DW.Buffer_CircBufIdx + ace_try_B.i] =
        ace_try_B.Gain[ace_try_B.i];
    }
  }

  ace_try_DW.Buffer_CircBufIdx += 16;
  if (ace_try_DW.Buffer_CircBufIdx >= 112) {
    ace_try_DW.Buffer_CircBufIdx -= 112;
  }

  /* End of Buffer: '<Root>/Buffer' */

  /* MATLAB Function: '<Root>/MATLAB Function' */
  /* MATLAB Function 'MATLAB Function': '<S3>:1' */
  /* '<S3>:1:3' r_sqr = fftft(u); */
  fftft(ace_try_B.Buffer, ace_try_B.r_sqr);

  /*  Maginude Square from 128-Point FFT */
  /* '<S3>:1:4' a_env = fftEnv(r_sqr); */
  fftEnv(ace_try_B.r_sqr, ace_try_B.a_env);

  /*  Bandverteilung und Hullkurve Detection */
  /* '<S3>:1:5' [electrode,index] = Sel(a_env); */
  memcpy(&ace_try_B.a_env_m[0], &ace_try_B.a_env[0], 22U * sizeof(real_T));
  Sel(ace_try_B.a_env_m, ace_try_B.electrode_c, ace_try_B.b_index);

  /*  Selektion */
  /* '<S3>:1:7' output = zeros(M,1); */
  /* '<S3>:1:8' env_out = a_env; */
  /* '<S3>:1:9' for i = 1:N */
  /* '<S3>:1:10' output(index(i)) = electrode(i); */
  for (ace_try_B.i = 0; ace_try_B.i < 22; ace_try_B.i++) {
    ace_try_B.output[ace_try_B.i] = 0.0;
    ace_try_B.env_out[ace_try_B.i] = ace_try_B.a_env[ace_try_B.i];
  }

  ace_try_B.output[(int32_T)ace_try_B.b_index[0] - 1] = ace_try_B.electrode_c[0];
  ace_try_B.electrode[0] = ace_try_B.electrode_c[0];
  ace_try_B.index[0] = ace_try_B.b_index[0];

  /* '<S3>:1:10' output(index(i)) = electrode(i); */
  ace_try_B.output[(int32_T)ace_try_B.b_index[1] - 1] = ace_try_B.electrode_c[1];
  ace_try_B.electrode[1] = ace_try_B.electrode_c[1];
  ace_try_B.index[1] = ace_try_B.b_index[1];

  /* '<S3>:1:10' output(index(i)) = electrode(i); */
  ace_try_B.output[(int32_T)ace_try_B.b_index[2] - 1] = ace_try_B.electrode_c[2];
  ace_try_B.electrode[2] = ace_try_B.electrode_c[2];
  ace_try_B.index[2] = ace_try_B.b_index[2];

  /* '<S3>:1:10' output(index(i)) = electrode(i); */
  ace_try_B.output[(int32_T)ace_try_B.b_index[3] - 1] = ace_try_B.electrode_c[3];
  ace_try_B.electrode[3] = ace_try_B.electrode_c[3];
  ace_try_B.index[3] = ace_try_B.b_index[3];

  /* End of MATLAB Function: '<Root>/MATLAB Function' */

  /* External mode */
  rtExtModeUploadCheckTrigger(1);

  {                                    /* Sample time: [0.001s, 0.0s] */
    rtExtModeUpload(0, ace_try_M->Timing.taskTime0);
  }

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.001s, 0.0s] */
    if ((rtmGetTFinal(ace_try_M)!=-1) &&
        !((rtmGetTFinal(ace_try_M)-ace_try_M->Timing.taskTime0) >
          ace_try_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(ace_try_M, "Simulation finished");
    }

    if (rtmGetStopRequested(ace_try_M)) {
      rtmSetErrorStatus(ace_try_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  ace_try_M->Timing.taskTime0 =
    (++ace_try_M->Timing.clockTick0) * ace_try_M->Timing.stepSize0;
}

/* Model initialize function */
void ace_try_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)ace_try_M, 0,
                sizeof(RT_MODEL_ace_try_T));
  rtmSetTFinal(ace_try_M, 10.0);
  ace_try_M->Timing.stepSize0 = 0.001;

  /* External mode info */
  ace_try_M->Sizes.checksums[0] = (2379523012U);
  ace_try_M->Sizes.checksums[1] = (1681630816U);
  ace_try_M->Sizes.checksums[2] = (2325471491U);
  ace_try_M->Sizes.checksums[3] = (3811091292U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[8];
    ace_try_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = (sysRanDType *)&ace_try_DW.Bandselection_SubsysRanBC;
    systemRan[2] = (sysRanDType *)&ace_try_DW.Bandselection_SubsysRanBC;
    systemRan[3] = (sysRanDType *)&ace_try_DW.FFTFilterbank_SubsysRanBC;
    systemRan[4] = (sysRanDType *)&ace_try_DW.FFTFilterbank_SubsysRanBC;
    systemRan[5] = &rtAlwaysEnabled;
    systemRan[6] = (sysRanDType *)&ace_try_DW.SpecturalEnvlopeDetection_Subsy;
    systemRan[7] = (sysRanDType *)&ace_try_DW.SpecturalEnvlopeDetection_Subsy;
    rteiSetModelMappingInfoPtr(ace_try_M->extModeInfo,
      &ace_try_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(ace_try_M->extModeInfo, ace_try_M->Sizes.checksums);
    rteiSetTPtr(ace_try_M->extModeInfo, rtmGetTPtr(ace_try_M));
  }

  /* block I/O */
  (void) memset(((void *) &ace_try_B), 0,
                sizeof(B_ace_try_T));

  /* states (dwork) */
  (void) memset((void *)&ace_try_DW, 0,
                sizeof(DW_ace_try_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    ace_try_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 14;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.BTransTable = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.PTransTable = &rtPTransTable;
  }

  {
    int32_T i;

    /* Start for S-Function (alsa_audio_capture_sfcn): '<Root>/ALSA Audio Capture' */
    audioCaptureInit(rtCP_ALSAAudioCapture_p1, 16000U, 0.5, 16U);

    /* InitializeConditions for Buffer: '<Root>/Buffer' */
    for (i = 0; i < 112; i++) {
      ace_try_DW.Buffer_CircBuff[i] = ace_try_P.Buffer_ic;
    }

    ace_try_DW.Buffer_CircBufIdx = 0;

    /* End of InitializeConditions for Buffer: '<Root>/Buffer' */
  }
}

/* Model terminate function */
void ace_try_terminate(void)
{
  /* Terminate for S-Function (alsa_audio_capture_sfcn): '<Root>/ALSA Audio Capture' */
  audioCaptureTerminate(rtCP_ALSAAudioCapture_p1);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
