/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: ace_try.h
 *
 * Code generated for Simulink model 'ace_try'.
 *
 * Model version                  : 1.57
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:12:51 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_ace_try_h_
#define RTW_HEADER_ace_try_h_
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef ace_try_COMMON_INCLUDES_
# define ace_try_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "dt_info.h"
#include "ext_work.h"
#include "alsa_audio_capture_macro.h"
#endif                                 /* ace_try_COMMON_INCLUDES_ */

#include "ace_try_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Child system includes */
#include "Sel_private.h"
#include "Sel.h"
#include "fftEnv_private.h"
#include "fftEnv.h"
#include "fftft_private.h"
#include "fftft.h"
#include "rtGetInf.h"
#include "rt_nonfinite.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block signals (auto storage) */
typedef struct {
  creal_T spectrum[128];
  creal_T dcv0[128];
  real_T dv0[128];
  real_T dv1[128];
  real_T rtu_u[128];
  real_T r_sqr[128];
  real_T Buffer[128];                  /* '<Root>/Buffer' */
  real_T output[22];                   /* '<Root>/MATLAB Function' */
  real_T env_out[22];                  /* '<Root>/MATLAB Function' */
  real_T x[22];
  real_T xwork[22];
  real_T a_env[22];
  real_T a_env_m[22];
  real_T Gain[16];                     /* '<Root>/Gain' */
  int32_T iidx[22];
  int32_T iwork[22];
  int16_T ALSAAudioCapture[32];        /* '<Root>/ALSA Audio Capture' */
  real_T electrode[4];                 /* '<Root>/MATLAB Function' */
  real_T index[4];                     /* '<Root>/MATLAB Function' */
  real_T x4[4];
  real_T electrode_c[4];
  real_T b_index[4];
  creal_T spectrum_m;
  real_T temp_re;
  int32_T yIdx;
  int32_T i;
} B_ace_try_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T Buffer_CircBuff[112];         /* '<Root>/Buffer' */
  struct {
    void *LoggedData;
  } ToWorkspace_PWORK;                 /* '<Root>/To Workspace' */

  struct {
    void *LoggedData;
  } ToWorkspace1_PWORK;                /* '<Root>/To Workspace1' */

  struct {
    void *LoggedData;
  } ToWorkspace2_PWORK;                /* '<Root>/To Workspace2' */

  struct {
    void *LoggedData;
  } ToWorkspace3_PWORK;                /* '<Root>/To Workspace3' */

  struct {
    void *LoggedData;
  } ToWorkspace4_PWORK;                /* '<Root>/To Workspace4' */

  int32_T Buffer_CircBufIdx;           /* '<Root>/Buffer' */
  int8_T SpecturalEnvlopeDetection_Subsy;/* '<Root>/Spectural Envlope Detection' */
  int8_T FFTFilterbank_SubsysRanBC;    /* '<Root>/FFT Filterbank' */
  int8_T Bandselection_SubsysRanBC;    /* '<Root>/Bandselection' */
} DW_ace_try_T;

/* Parameters (auto storage) */
struct P_ace_try_T_ {
  real_T MATLABFunction_WindowFunc[128];/* Expression: WindowFunc
                                         * Referenced by: '<S2>/MATLAB Function'
                                         */
  real_T MATLABFunction_WindowSize;    /* Expression: WindowSize
                                        * Referenced by: '<S2>/MATLAB Function'
                                        */
  real_T MATLABFunction_FilterKoeff[2816];/* Expression: FilterKoeff
                                           * Referenced by: '<S4>/MATLAB Function'
                                           */
  real_T Gain_Gain;                    /* Expression: 100.0/32767
                                        * Referenced by: '<Root>/Gain'
                                        */
  real_T Buffer_ic;                    /* Expression: 0
                                        * Referenced by: '<Root>/Buffer'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_ace_try_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_ace_try_T ace_try_P;

/* Block signals (auto storage) */
extern B_ace_try_T ace_try_B;

/* Block states (auto storage) */
extern DW_ace_try_T ace_try_DW;

/* Model entry point functions */
extern void ace_try_initialize(void);
extern void ace_try_step(void);
extern void ace_try_terminate(void);

/* Real-time Model object */
extern RT_MODEL_ace_try_T *const ace_try_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'ace_try'
 * '<S1>'   : 'ace_try/Bandselection'
 * '<S2>'   : 'ace_try/FFT Filterbank'
 * '<S3>'   : 'ace_try/MATLAB Function'
 * '<S4>'   : 'ace_try/Spectural Envlope Detection'
 * '<S5>'   : 'ace_try/Bandselection/MATLAB Function'
 * '<S6>'   : 'ace_try/FFT Filterbank/MATLAB Function'
 * '<S7>'   : 'ace_try/Spectural Envlope Detection/MATLAB Function'
 */
#endif                                 /* RTW_HEADER_ace_try_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
