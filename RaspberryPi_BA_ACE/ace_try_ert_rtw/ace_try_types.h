/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: ace_try_types.h
 *
 * Code generated for Simulink model 'ace_try'.
 *
 * Model version                  : 1.57
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:12:51 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_ace_try_types_h_
#define RTW_HEADER_ace_try_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_ace_try_T_ P_ace_try_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_ace_try_T RT_MODEL_ace_try_T;

#endif                                 /* RTW_HEADER_ace_try_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
