function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <Root>/ALSA Audio Capture */
	this.urlHashMap["ace_try:41"] = "ace_try.c:46,247,264&ace_try.h:105&ace_try_private.h:37";
	/* <Root>/Bandselection */
	this.urlHashMap["ace_try:16"] = "ace_try.h:143&Sel.c:256";
	/* <Root>/Buffer */
	this.urlHashMap["ace_try:30"] = "ace_try.c:56,102,250,257&ace_try.h:95,119,140,161&ace_try_data.c:270";
	/* <Root>/Data Type Conversion */
	this.urlHashMap["ace_try:40"] = "msg=rtwMsg_notTraceable&block=ace_try:40";
	/* <Root>/FFT Filterbank */
	this.urlHashMap["ace_try:8"] = "ace_try.h:142&fftft.c:158";
	/* <Root>/Gain */
	this.urlHashMap["ace_try:38"] = "ace_try.c:49&ace_try.h:102,158&ace_try_data.c:267";
	/* <Root>/MATLAB Function */
	this.urlHashMap["ace_try:4"] = "ace_try.c:104,147&ace_try.h:96,97,106,107";
	/* <Root>/Multiport
Selector */
	this.urlHashMap["ace_try:39"] = "ace_try.c:50";
	/* <Root>/Spectural Envlope Detection */
	this.urlHashMap["ace_try:12"] = "ace_try.h:141&fftEnv.c:26";
	/* <Root>/To Workspace */
	this.urlHashMap["ace_try:23"] = "ace_try.h:122";
	/* <Root>/To Workspace1 */
	this.urlHashMap["ace_try:24"] = "ace_try.h:126";
	/* <Root>/To Workspace2 */
	this.urlHashMap["ace_try:32"] = "ace_try.h:130";
	/* <Root>/To Workspace3 */
	this.urlHashMap["ace_try:33"] = "ace_try.h:134";
	/* <Root>/To Workspace4 */
	this.urlHashMap["ace_try:37"] = "ace_try.h:138";
	/* <S1>/MATLAB Function */
	this.urlHashMap["ace_try:25"] = "Sel.c:31,250,259,268,275,280,285,290,295,300,305&imohimohmglndjec_merge.c:18";
	/* <S1>/u */
	this.urlHashMap["ace_try:18"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=ace_try:18";
	/* <S1>/y */
	this.urlHashMap["ace_try:19"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=ace_try:19";
	/* <S1>/y1 */
	this.urlHashMap["ace_try:26"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=ace_try:26";
	/* <S2>/MATLAB Function */
	this.urlHashMap["ace_try:21"] = "ace_try.h:149,152&fftft.c:30,163,207&ace_try_data.c:26,71&glngcbaalfkfppph_power.c:18";
	/* <S2>/u */
	this.urlHashMap["ace_try:10"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=ace_try:10";
	/* <S2>/y */
	this.urlHashMap["ace_try:11"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=ace_try:11";
	/* <S3>:1 */
	this.urlHashMap["ace_try:4:1"] = "ace_try.c:105";
	/* <S3>:1:3 */
	this.urlHashMap["ace_try:4:1:3"] = "ace_try.c:106";
	/* <S3>:1:4 */
	this.urlHashMap["ace_try:4:1:4"] = "ace_try.c:110";
	/* <S3>:1:5 */
	this.urlHashMap["ace_try:4:1:5"] = "ace_try.c:114";
	/* <S3>:1:7 */
	this.urlHashMap["ace_try:4:1:7"] = "ace_try.c:119";
	/* <S3>:1:8 */
	this.urlHashMap["ace_try:4:1:8"] = "ace_try.c:120";
	/* <S3>:1:9 */
	this.urlHashMap["ace_try:4:1:9"] = "ace_try.c:121";
	/* <S3>:1:10 */
	this.urlHashMap["ace_try:4:1:10"] = "ace_try.c:122,132,137,142";
	/* <S4>/MATLAB Function */
	this.urlHashMap["ace_try:22"] = "ace_try.h:155&fftEnv.c:37,48&ace_try_data.c:75";
	/* <S4>/u */
	this.urlHashMap["ace_try:14"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=ace_try:14";
	/* <S4>/y */
	this.urlHashMap["ace_try:15"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=ace_try:15";
	/* <S5>:1 */
	this.urlHashMap["ace_try:25:1"] = "Sel.c:262";
	/* <S5>:1:3 */
	this.urlHashMap["ace_try:25:1:3"] = "Sel.c:263";
	/* <S5>:1:5 */
	this.urlHashMap["ace_try:25:1:5"] = "Sel.c:270";
	/* <S5>:1:6 */
	this.urlHashMap["ace_try:25:1:6"] = "Sel.c:271";
	/* <S6>:1 */
	this.urlHashMap["ace_try:21:1"] = "fftft.c:166";
	/* <S6>:1:3 */
	this.urlHashMap["ace_try:21:1:3"] = "fftft.c:167";
	/* <S6>:1:4 */
	this.urlHashMap["ace_try:21:1:4"] = "fftft.c:168";
	/* <S6>:1:7 */
	this.urlHashMap["ace_try:21:1:7"] = "fftft.c:177";
	/* <S6>:1:9 */
	this.urlHashMap["ace_try:21:1:9"] = "fftft.c:178";
	/* <S7>:1 */
	this.urlHashMap["ace_try:22:1"] = "fftEnv.c:33";
	/* <S7>:1:3 */
	this.urlHashMap["ace_try:22:1:3"] = "fftEnv.c:34";
	/* <S7>:1:5 */
	this.urlHashMap["ace_try:22:1:5"] = "fftEnv.c:35";
	/* <S7>:1:7 */
	this.urlHashMap["ace_try:22:1:7"] = "fftEnv.c:41";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "ace_try"};
	this.sidHashMap["ace_try"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S1>"] = {sid: "ace_try:16"};
	this.sidHashMap["ace_try:16"] = {rtwname: "<S1>"};
	this.rtwnameHashMap["<S2>"] = {sid: "ace_try:8"};
	this.sidHashMap["ace_try:8"] = {rtwname: "<S2>"};
	this.rtwnameHashMap["<S3>"] = {sid: "ace_try:4"};
	this.sidHashMap["ace_try:4"] = {rtwname: "<S3>"};
	this.rtwnameHashMap["<S4>"] = {sid: "ace_try:12"};
	this.sidHashMap["ace_try:12"] = {rtwname: "<S4>"};
	this.rtwnameHashMap["<S5>"] = {sid: "ace_try:25"};
	this.sidHashMap["ace_try:25"] = {rtwname: "<S5>"};
	this.rtwnameHashMap["<S6>"] = {sid: "ace_try:21"};
	this.sidHashMap["ace_try:21"] = {rtwname: "<S6>"};
	this.rtwnameHashMap["<S7>"] = {sid: "ace_try:22"};
	this.sidHashMap["ace_try:22"] = {rtwname: "<S7>"};
	this.rtwnameHashMap["<Root>/ALSA Audio Capture"] = {sid: "ace_try:41"};
	this.sidHashMap["ace_try:41"] = {rtwname: "<Root>/ALSA Audio Capture"};
	this.rtwnameHashMap["<Root>/Bandselection"] = {sid: "ace_try:16"};
	this.sidHashMap["ace_try:16"] = {rtwname: "<Root>/Bandselection"};
	this.rtwnameHashMap["<Root>/Buffer"] = {sid: "ace_try:30"};
	this.sidHashMap["ace_try:30"] = {rtwname: "<Root>/Buffer"};
	this.rtwnameHashMap["<Root>/Data Type Conversion"] = {sid: "ace_try:40"};
	this.sidHashMap["ace_try:40"] = {rtwname: "<Root>/Data Type Conversion"};
	this.rtwnameHashMap["<Root>/FFT Filterbank"] = {sid: "ace_try:8"};
	this.sidHashMap["ace_try:8"] = {rtwname: "<Root>/FFT Filterbank"};
	this.rtwnameHashMap["<Root>/Gain"] = {sid: "ace_try:38"};
	this.sidHashMap["ace_try:38"] = {rtwname: "<Root>/Gain"};
	this.rtwnameHashMap["<Root>/MATLAB Function"] = {sid: "ace_try:4"};
	this.sidHashMap["ace_try:4"] = {rtwname: "<Root>/MATLAB Function"};
	this.rtwnameHashMap["<Root>/Multiport Selector"] = {sid: "ace_try:39"};
	this.sidHashMap["ace_try:39"] = {rtwname: "<Root>/Multiport Selector"};
	this.rtwnameHashMap["<Root>/NT-specific Demo2"] = {sid: "ace_try:7"};
	this.sidHashMap["ace_try:7"] = {rtwname: "<Root>/NT-specific Demo2"};
	this.rtwnameHashMap["<Root>/Spectural Envlope Detection"] = {sid: "ace_try:12"};
	this.sidHashMap["ace_try:12"] = {rtwname: "<Root>/Spectural Envlope Detection"};
	this.rtwnameHashMap["<Root>/To Workspace"] = {sid: "ace_try:23"};
	this.sidHashMap["ace_try:23"] = {rtwname: "<Root>/To Workspace"};
	this.rtwnameHashMap["<Root>/To Workspace1"] = {sid: "ace_try:24"};
	this.sidHashMap["ace_try:24"] = {rtwname: "<Root>/To Workspace1"};
	this.rtwnameHashMap["<Root>/To Workspace2"] = {sid: "ace_try:32"};
	this.sidHashMap["ace_try:32"] = {rtwname: "<Root>/To Workspace2"};
	this.rtwnameHashMap["<Root>/To Workspace3"] = {sid: "ace_try:33"};
	this.sidHashMap["ace_try:33"] = {rtwname: "<Root>/To Workspace3"};
	this.rtwnameHashMap["<Root>/To Workspace4"] = {sid: "ace_try:37"};
	this.sidHashMap["ace_try:37"] = {rtwname: "<Root>/To Workspace4"};
	this.rtwnameHashMap["<S1>/Sel"] = {sid: "ace_try:17"};
	this.sidHashMap["ace_try:17"] = {rtwname: "<S1>/Sel"};
	this.rtwnameHashMap["<S1>/MATLAB Function"] = {sid: "ace_try:25"};
	this.sidHashMap["ace_try:25"] = {rtwname: "<S1>/MATLAB Function"};
	this.rtwnameHashMap["<S1>/u"] = {sid: "ace_try:18"};
	this.sidHashMap["ace_try:18"] = {rtwname: "<S1>/u"};
	this.rtwnameHashMap["<S1>/y"] = {sid: "ace_try:19"};
	this.sidHashMap["ace_try:19"] = {rtwname: "<S1>/y"};
	this.rtwnameHashMap["<S1>/y1"] = {sid: "ace_try:26"};
	this.sidHashMap["ace_try:26"] = {rtwname: "<S1>/y1"};
	this.rtwnameHashMap["<S2>/fftft"] = {sid: "ace_try:9"};
	this.sidHashMap["ace_try:9"] = {rtwname: "<S2>/fftft"};
	this.rtwnameHashMap["<S2>/MATLAB Function"] = {sid: "ace_try:21"};
	this.sidHashMap["ace_try:21"] = {rtwname: "<S2>/MATLAB Function"};
	this.rtwnameHashMap["<S2>/u"] = {sid: "ace_try:10"};
	this.sidHashMap["ace_try:10"] = {rtwname: "<S2>/u"};
	this.rtwnameHashMap["<S2>/y"] = {sid: "ace_try:11"};
	this.sidHashMap["ace_try:11"] = {rtwname: "<S2>/y"};
	this.rtwnameHashMap["<S3>:1"] = {sid: "ace_try:4:1"};
	this.sidHashMap["ace_try:4:1"] = {rtwname: "<S3>:1"};
	this.rtwnameHashMap["<S3>:1:3"] = {sid: "ace_try:4:1:3"};
	this.sidHashMap["ace_try:4:1:3"] = {rtwname: "<S3>:1:3"};
	this.rtwnameHashMap["<S3>:1:4"] = {sid: "ace_try:4:1:4"};
	this.sidHashMap["ace_try:4:1:4"] = {rtwname: "<S3>:1:4"};
	this.rtwnameHashMap["<S3>:1:5"] = {sid: "ace_try:4:1:5"};
	this.sidHashMap["ace_try:4:1:5"] = {rtwname: "<S3>:1:5"};
	this.rtwnameHashMap["<S3>:1:7"] = {sid: "ace_try:4:1:7"};
	this.sidHashMap["ace_try:4:1:7"] = {rtwname: "<S3>:1:7"};
	this.rtwnameHashMap["<S3>:1:8"] = {sid: "ace_try:4:1:8"};
	this.sidHashMap["ace_try:4:1:8"] = {rtwname: "<S3>:1:8"};
	this.rtwnameHashMap["<S3>:1:9"] = {sid: "ace_try:4:1:9"};
	this.sidHashMap["ace_try:4:1:9"] = {rtwname: "<S3>:1:9"};
	this.rtwnameHashMap["<S3>:1:10"] = {sid: "ace_try:4:1:10"};
	this.sidHashMap["ace_try:4:1:10"] = {rtwname: "<S3>:1:10"};
	this.rtwnameHashMap["<S4>/fftEnv"] = {sid: "ace_try:13"};
	this.sidHashMap["ace_try:13"] = {rtwname: "<S4>/fftEnv"};
	this.rtwnameHashMap["<S4>/MATLAB Function"] = {sid: "ace_try:22"};
	this.sidHashMap["ace_try:22"] = {rtwname: "<S4>/MATLAB Function"};
	this.rtwnameHashMap["<S4>/u"] = {sid: "ace_try:14"};
	this.sidHashMap["ace_try:14"] = {rtwname: "<S4>/u"};
	this.rtwnameHashMap["<S4>/y"] = {sid: "ace_try:15"};
	this.sidHashMap["ace_try:15"] = {rtwname: "<S4>/y"};
	this.rtwnameHashMap["<S5>:1"] = {sid: "ace_try:25:1"};
	this.sidHashMap["ace_try:25:1"] = {rtwname: "<S5>:1"};
	this.rtwnameHashMap["<S5>:1:3"] = {sid: "ace_try:25:1:3"};
	this.sidHashMap["ace_try:25:1:3"] = {rtwname: "<S5>:1:3"};
	this.rtwnameHashMap["<S5>:1:5"] = {sid: "ace_try:25:1:5"};
	this.sidHashMap["ace_try:25:1:5"] = {rtwname: "<S5>:1:5"};
	this.rtwnameHashMap["<S5>:1:6"] = {sid: "ace_try:25:1:6"};
	this.sidHashMap["ace_try:25:1:6"] = {rtwname: "<S5>:1:6"};
	this.rtwnameHashMap["<S6>:1"] = {sid: "ace_try:21:1"};
	this.sidHashMap["ace_try:21:1"] = {rtwname: "<S6>:1"};
	this.rtwnameHashMap["<S6>:1:3"] = {sid: "ace_try:21:1:3"};
	this.sidHashMap["ace_try:21:1:3"] = {rtwname: "<S6>:1:3"};
	this.rtwnameHashMap["<S6>:1:4"] = {sid: "ace_try:21:1:4"};
	this.sidHashMap["ace_try:21:1:4"] = {rtwname: "<S6>:1:4"};
	this.rtwnameHashMap["<S6>:1:7"] = {sid: "ace_try:21:1:7"};
	this.sidHashMap["ace_try:21:1:7"] = {rtwname: "<S6>:1:7"};
	this.rtwnameHashMap["<S6>:1:9"] = {sid: "ace_try:21:1:9"};
	this.sidHashMap["ace_try:21:1:9"] = {rtwname: "<S6>:1:9"};
	this.rtwnameHashMap["<S7>:1"] = {sid: "ace_try:22:1"};
	this.sidHashMap["ace_try:22:1"] = {rtwname: "<S7>:1"};
	this.rtwnameHashMap["<S7>:1:3"] = {sid: "ace_try:22:1:3"};
	this.sidHashMap["ace_try:22:1:3"] = {rtwname: "<S7>:1:3"};
	this.rtwnameHashMap["<S7>:1:5"] = {sid: "ace_try:22:1:5"};
	this.sidHashMap["ace_try:22:1:5"] = {rtwname: "<S7>:1:5"};
	this.rtwnameHashMap["<S7>:1:7"] = {sid: "ace_try:22:1:7"};
	this.sidHashMap["ace_try:22:1:7"] = {rtwname: "<S7>:1:7"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
