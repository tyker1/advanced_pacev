/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: fftEnv_private.h
 *
 * Code generated for Simulink model 'ace_try'.
 *
 * Model version                  : 1.57
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:12:51 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_fftEnv_private_h_
#define RTW_HEADER_fftEnv_private_h_
#include <math.h>
#ifndef ace_try_COMMON_INCLUDES_
# define ace_try_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "dt_info.h"
#include "ext_work.h"
#include "alsa_audio_capture_macro.h"
#endif                                 /* ace_try_COMMON_INCLUDES_ */

/* Shared type includes */
#include "multiword_types.h"
#endif                                 /* RTW_HEADER_fftEnv_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
