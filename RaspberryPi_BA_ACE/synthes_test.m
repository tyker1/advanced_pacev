%test_synthese

L = length(elektrode);

sig = zeros(1,L*SamplePerFrame);

fs = Samplefrequence;
tl = 0;
tr = 0;
for i = 1:L
    sig_sin =zeros(1,SamplePerFrame);
    tr = tl+SamplePerFrame*(1/fs);
    t = linspace(tl,tr,SamplePerFrame);
    for j = 1:N
        sig_sin = sig_sin + elektrode(i,j)*sin(MidFreq(elektrode_index(i,j))*2*pi*t);
    end
    tl = tr + 1/fs;
    sig((i-1)*SamplePerFrame+1:i*SamplePerFrame) = sig_sin;
end

sound(sig,Samplefrequence);