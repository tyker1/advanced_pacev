getIdx = @(idx) mod(idx,2) + 1;

freqs = [200, 300];
phis = [0,0];

fs = 44100;
frameSize = 512;

phi = [];
loops = 50;
f = [];
for i = 1:loops
    if (i == 1)      
        idx1 = getIdx(mod(round(rand*10),2));
    else
        idx1 = idx2;
    end
    idx2 = getIdx(mod(round(rand*10),2));
    f1 = freqs(idx1);
    f2 = freqs(idx2);
    phiStart = phis(idx1);
    
    if (i == 1)
        f = [f1,f2];
    else
        f = [f,f2];
    end
    phiEnd = phiStart + frameSize/fs*2*pi*f2;
    intPhi = phiInterpolation(f1,f2,phiStart,0,frameSize,fs);
    phi = [phi,intPhi];
    phis(idx2) = intPhi(end) + 2*pi*f2/fs;
end