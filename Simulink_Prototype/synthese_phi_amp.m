function [ synAudio ] = synthese_phi_amp( Tracks,  MidFreq, fs, N)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

Frames = length(Tracks);
synAudio = zeros(1,Frames*N);
lastPhi = zeros(1,length(MidFreq));
for i = 1:Frames
    FStart = (i-1)*N + 1;
    FEnd = i*N;
    track = Tracks(i);
    CosKomp = length(track.F1);
    FrameSig = zeros(1,N);
    for j = 1:CosKomp
        idxF2 = MidFreq == track.F2(j);
        [Amp,Phi] = AmpFreqInt(track.A1(j),track.A2(j),track.F1(j),track.F2(j), N, fs, MidFreq, lastPhi);
        teilSig = Amp.*sin(Phi);
        %teilSig = 0.1*sin(Phi);
        if (track.A2(j) == 0)
            lastPhi(idxF2) = 0;
        else
            lastPhi(idxF2) = Phi(end)+ 2 *pi * track.F2(j) * 1 / fs;
        end
        FrameSig = FrameSig + teilSig;
    end
    synAudio(FStart:FEnd) = FrameSig;
end

end

