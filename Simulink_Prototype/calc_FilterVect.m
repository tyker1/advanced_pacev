function [ FilterVect ] = calc_FilterVect( fs,Aufloesung_koeff,Aufloesung )
%Parameter
%   fs: Abtastfrequenz
%   Aufloesung_koeff: Anzahl der Elektrode je Elektrodenpaar
%   Aufloesung: gewunschte Frequenzaufl�sung bei FFT

CW = @(f) 25+75*(1+1.4*(f/1000).^2).^0.69;
%Aufloesung = 50;
fftlen=2^(nextpow2(fs/Aufloesung))*Aufloesung_koeff;

BinBW = fs/fftlen;
BinFreq = 0:(fftlen-1);
BinFreq = BinFreq .* BinBW;

idx = round(150/BinBW)+1; % Erste MittelFrequenz bei 250Hz

totalFt = 0;
AimFt = 24*Aufloesung_koeff;

FilterVect = zeros(AimFt,3);

while (idx < fftlen/2+2) && (totalFt < AimFt)
     Bandwidth = CW(BinFreq(idx))/Aufloesung_koeff;
     Fmid = BinFreq(idx);
     StartBin = round((Fmid-(Bandwidth-BinBW)/2)/BinBW)+1;
     EndBin = round((Fmid+(Bandwidth-BinBW)/2)/BinBW)+1;

     totalFt = totalFt+1;
     MidFreq = mean(BinFreq(StartBin:EndBin));
     FilterVect(totalFt,:) = [MidFreq EndBin-StartBin+1  1];
     
     % Sucht nach naechste MittelFrequenz
     idx = EndBin + 1;
     while (idx < fftlen/2) && (totalFt < AimFt)
         newBW = CW(BinFreq(idx))/Aufloesung_koeff;
         newFmid = BinFreq(idx);
         newStartFreq = newFmid - max(BinBW/2,newBW/2);
         EndFreq = BinFreq(EndBin) + BinBW/2;
         
         if (newStartFreq - EndFreq) > -BinBW/2.5
             break;
         else
             idx = idx + 1;
         end
     end
end

end

