function [ ampInt,phiInt ] = AmpFreqInt( a1, a2, f1, f2, n, fs, MidF )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

persistent lastPhi;

if isempty(lastPhi)
    lastPhi = zeros(1,length(MidF));
end
ampInt = amplitudeInterpolation(a1, a2, n);
idxF1 = MidF == f1;
idxF2 = MidF == f2;
p1 = lastPhi(idxF1);
p2 = n/fs*2*pi*f2+p1;
%p2 = mod(p2,2*pi);
phiInt = phasenInterpolation(f1, f2, p1, p2, n, fs);
%lastPhi(idxF2) = phiInt(end) + 2*f2*pi*1/fs;
if (a2 == 0)
    lastPhi(idxF2) = 0;
else
    lastPhi(idxF2) = phiInt(end) + 2*fs*pi*1/fs;
end

end

function [IntAmp] = amplitudeInterpolation(a1, a2, Len)

    n = 0:Len-1;
    delta = (a2-a1)/Len;
    IntAmp = a1 + delta .* n;
end


function [IntPhi] = phasenInterpolation(f1, f2, p1, p2, Len ,fs)
    %IntPhi = f1;
    
    n = 0:Len-1;
    omega1 = f1*2*pi * 1/fs;
    omega2 = f2*2*pi * 1/fs;
    e = p1;
    k = omega1;
    M = ((p1 + omega1 * Len - p2) + (omega2-omega1)*Len/2) * 1/(2*pi);
    M = ceil(M);
    MatQ = [ 3/Len^2, - 1/Len; -2/Len^3, 1/Len^2];
    MatV = [p2 - p1 - omega1 * Len + 2*pi*M; omega2 - omega1];
    temp = MatQ * MatV;
    alpha = temp(1);
    beta = temp(2);
    IntPhi = e + k .* n + alpha .* n.^2 + beta .* n.^3;
    IntPhi = mod(IntPhi,2*pi);
end