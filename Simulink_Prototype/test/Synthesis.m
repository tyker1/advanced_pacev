function audio_out= Synthesis(Tracks, sinM, NRek, LastFrame, NextFrame, b) 
% Sinusoidale Synthese
% Eingabeparameter:
% Tracks: Srukturen mit den Frequenzen, Ampl und Phasen der Tracks
% FS : Abtastfrequentder 
% FName, L�nge des Blocks verwendet f�r die Analyse
% Nrek: Anzahl der zu erzugten Werte (bei einzelfehler NFrame, sonst NRek)
% NFFT: Die FFT L�nge verwendet bei der analyse
% merg: merging-L�nge
% b: B�ndelfehler: b=0 --> Einzelfehler; b= 1--> 2 Pakete fehlern
% R�ckgabewert: Rekonstruktionssignal
%
% In dieser Version wird die Amplitude der Frequenzspur bei Death
% und Birth beibehalten
%
%%
global FS
global FFTLength;
global NFrame;
global merg;


%% Initialisierungen
% anzBlock = length(Tracks);            % Anzahl der Bl�cke
audio_out = [];
audio_tmp = 0;

%% Block i:  f�r jede Schwingung Parameter ermitteln
anzSin = length(Tracks.F1);     %  Anzahl der Sinus-Schwingungen in Segment i
if anzSin                       % Es sind Tracks vorhanden
%% Genaue Bestimmung der Amplitude Phase und Frequenz f�r die Synthese
% ergibt bessere Ergebnisse --> bleibt
    % NFF = L�nge Frame2Frame: Anfang Block k-1 bis Anfang Block k+1
    NFF = NFrame + (NRek - 2*merg); 
    [A_start, A_end,F_start, F_end, Phi_start, Phi_end] = adjustment_freq_Amp_Phase(Tracks, sinM, FS, LastFrame, NextFrame, NFF);
    
    for k=1:anzSin
        %% Amplitudeninterpolation Linear
        AmpTrack = Lin_Ampl_Interp(A_start(k), A_end(k), NRek, merg, b);
        
        %% Phaseninterpolation linear ;  Kubisch oder Hermite
        % PhaseTrack = Lin_Phase_Interp(F_start(k), F_end(k), Phi_start(k), NRek, FS);
        % PhaseTrack = Lin_Phase_Interp2(F_start(k), F_end(k), Phi_start(k),Phi_end(k), FS, NRek, merg, b);
        % Hermite Interpolation
        % PhaseTrack1 = Hermite3_Phase(F_start(k), F_end(k), Phi_start(k),Phi_end(k), FS, NRek, merg, b);
        
        % Kubische Phaseninterpolation nach McAulay
        PhaseTrack = Cub_Phase_Interp(F_start(k), F_end(k), Phi_start(k),Phi_end(k), FS, NRek, merg, b);%              plot(PhaseTrack, 'r');
   
        %% Sinus-Schwingungen erzeugen  
        x =  AmpTrack.*cos(PhaseTrack);
        audio_tmp = audio_tmp + x;
        close all
    end
    audio_out  = audio_tmp;
    audio_tmp = 0;
else 
    audio_out =  zeros(1, NRek);
    disp('Null-Segment, da keine Tracks !!!!  ')
end
end

%%
% ***********************************************************************
% Methode zur linearen Amplitudeninterpolation 
% Erwartet 2 Werte mit den start und Ziel Amplituden
% liefert alle Amplituden eines Tracks
% Amplituden A1 und A2 beziehen sich jeweils (durch Hamming) auf den
% Zentrum des Blockes --> es m�ssen neue Bezugsamplituden berechnent werden
% Astart und Aend
% ***********************************************************************
function AmpTrack = Lin_Ampl_Interp(A1, A2, Nrek, merg, b) % ---> OK getestet
   
    NFrame = (Nrek -2*merg)/(b+1);  % Bl�ckel�nge vor und nach der L�cke
                                    % Bei B�ndelfehler nur die H�lfte
    N = Nrek -2*merg;               % L�nge der L�cke
    NCC = N + NFrame;               % von Zentrum zu Zentr.= Nl�cke +NFram
    deltaA = (A2 - A1)/NCC;
%     Astart = A1 +((NFrame/2) - merg)*deltaA;
    Astart = A1 +((NFrame/2) - merg+1)*deltaA; % 09.04.14 --> Ok bleibt
    AmpTrack = Astart + deltaA * [0:Nrek-1];
end

%%
% ***********************************************************************
%  Methode zur linearen Phaseninterpolation 
% Erwartet 2 Frequenzen und 2 Phasen jeweils Start und End-Wert
% Liefert einen Vektor mit Phasen
% Hier werden die Start und endphasen nicht neu berechnet
% ***********************************************************************
 function [PhaseTrack] = Lin_Phase_Interp(F1, F2, Phi1, N, FS)
    deltaF = (F2 - F1)/(N-1);
    f = F1 + deltaF*(0:N-1);
    PhaseTrack = Phi1 + 2*pi*f/FS.*[0:N-1];
 end 
 
 %%
 % ***********************************************************************
 % Lineare Phaseninterpolation Version 2 mit Ber�cksichtigung der Blockl�nge
 % und neu Berechnung der Start und Endphase an den Blockgrenzen
 % ***********************************************************************
 function [PhaseTrack] = Lin_Phase_Interp2 (f1,f2, p1, p2, FS,Nrek,merg,b)
 % f1, f2, p1, p2 jeweils Frequenzen und Phasen aus dem letzten und
 % n�chsten Block
 % p1 und p2 beziehen sich auf den anfang des Blockes
 % N = Rekonstuktionsl�nge 1024 +2*merg

    NFrame = (Nrek -2*merg)/(b+1);  % Bl�ckel�nge vor und nach der L�cke
                                    % Bei B�ndelfehler nur die H�lfte
    N = Nrek -2*merg;               % L�nge der L�cke
    pstart = p1 + 2*pi*f1*(NFrame - merg +1)/FS ;
    pstart = mod(pstart,2*pi);
    deltaF = (f2 - f1)/(Nrek-1);
	f = f1 + deltaF*(0:Nrek-1);
    PhaseTrack = pstart + (2*pi*f/FS).*[0:Nrek-1];
	%PhaseTrack = mod(PhaseTrack , 2*pi);
 end	
 %%
 % ***********************************************************************
 % Lineare Phaseninterpolation Version 2 mit Ber�cksichtigung der Blockl�nge
 % und neu Berechnung der Start und Endphase an den Blockgrenzen
 % ***********************************************************************
 function [PhaseTrack] = Phase_Interp_gleicheFreq (f1,f2, p1, p2, FS,Nrek,merg,b)
 % f1, f2, p1, p2 jeweils Frequenzen und Phasen aus dem letzten und
 % n�chsten Block
 % p1 und p2 beziehen sich auf den anfang des Blockes
 % N = Rekonstuktionsl�nge 1024 +2*merg

%% Version 1
%     NFrame = (Nrek -2*merg)/(b+1);  % Bl�ckel�nge vor und nach der L�cke
%                                     % Bei B�ndelfehler nur die H�lfte
%     N = Nrek -2*merg;               % L�nge der L�cke
%     pstart = p1 + 2*pi*f1*(NFrame - merg +1)/FS ;
%     
%     pstart = p1 + 2*pi*f1*(NFrame - merg +1)/FS ;
%     
%     pstart = mod(pstart,2*pi);
%     %deltaF = (f2 - f1)/(Nrek-1);
% 	%f = f1 + deltaF*(0:Nrek-1);
%     PhaseTrack = pstart + (2*pi*f1/FS).*[0:Nrek-1];
% 	%PhaseTrack = mod(PhaseTrack , 2*pi);
%% Version 2
   NFrame = (Nrek -2*merg)/(b+1);  % Bl�ckel�nge vor und nach der L�cke
%                                  % Bei B�ndelfehler nur die H�lfte
    N = Nrek -2*merg;               % L�nge der L�cke
    omega1 =  2*pi*f1/FS;
    p = p1 + omega1 *[0:(NFrame+N+merg)];
    %pstart = p1 + omega1*(NFrame - merg +1) ;
    %pstart = mod(pstart,2*pi);
    a = NFrame-merg+1 ;
    b = a + Nrek-1;
    PhaseTrack = p(a:b);
	PhaseTrack = mod(PhaseTrack , 2*pi);
 end	
 

%%
% *************************************************************************
% % Rein Kubische Phaseninterpolation	f�r den gesamten Bereich 		
% % Software Version 2014-6
% % ***********************************************************************
 function [Phi] = Cub_Phase_Interp (f1, f2, p1, p2, FS, Nrek, merg, b)

     % L�nge mit 2x merging  
    omega1 = 2*pi*f1/FS;
    omega2 = 2*pi*f2/FS;
       
    NFrame = (Nrek -2*merg)/(b+1);  % Bl�ckel�nge vor und nach der L�cke
                                    % Bei B�ndelfehler nur die H�lfte
 	L = Nrek - 2*merg;				% tats�chliche Blockl�nge ohne Merging
  
 	%Anfangsparameter initialisieren
  	pstart = p1 + omega1*(NFrame - merg) ; % Lewei mit 0 starten
    pstart = mod (pstart, 2*pi);
      
   	pend = p2 + (merg-1)*omega2;         % End-Phase f�r den Rek. block
 	pend = mod (pend,2*pi);             %
 
 	% Phasenunwrapping 
    param1 = pend - pstart - omega1*Nrek + omega2;   % Version 2014-6-test5
    %param1 = pend - pstart - omega1*Nrek;   % Version 2014-6-test5
    param1 = mod(param1, 2*pi); % 16.04
    
    %M = round((  (pstart -pend) +(omega1+omega2)*(Nrek/2) )/(2*pi))
     %param1 = pend - pstart - omega1*Nrek + 2*pi*M;   % Version 2014-6-test5
      
  	param1 =  mod(param1, 2*pi);
  	param1 = unwrap (0, param1);

    param2 = omega2 - omega1;
%     param2 = unwrap (0, param2);
 
    K = omega1;
    alpha = (3 * param1 / (Nrek^2) ) - (param2/Nrek);
  	beta  = -( param1*2/(Nrek^3)) +  (param2/(Nrek^2));

    n = 0:Nrek-1;
    Phi = pstart + K*n + alpha*n.^2 + beta*n.^3;
    Phi = mod(Phi, 2*pi);
    
%     plot(n,Phi)
%     hold on
%    Phi= mod(Phi,2*pi);
   
%    plot(n, Phi)
%     grid on
%     legend('vor mod 2pi', 'nach mod 2Pi')
%     hold off
%     close all

 end
 
 %%
 %%
% *************************************************************************
% %  Kubische Phaseninterpolation	mit der Matlab Funktion Spline 		
% % Software Version 2014-7
% % ***********************************************************************
 function [Phi, X, Y] = Spline_Phase (f1, f2, p1, p2, FS, Nrek, merg, b)

     % L�nge mit 2x merging  
    omega1 = 2*pi*f1/FS;
    omega2 = 2*pi*f2/FS;
       
    NFrame = (Nrek -2*merg)/(b+1);  % Bl�ckel�nge vor und nach der L�cke
                                    % Bei B�ndelfehler nur die H�lfte
 	L = Nrek - 2*merg;				% tats�chliche L�ckenl�nge ohne Merging
  
 	%Anfangsparameter initialisieren
  	pstart = p1 + omega1*(NFrame - merg) ;
    pstart = mod (pstart, 2*pi);
      
   	pend = p2 + (merg-1)*omega2;         % End-Phase f�r den Rek. block
 	pend = mod (pend,2*pi);
 
 	%Phi = pstart + 
    
    n1 = NFrame-merg;
    n2 = NFrame+L;
    n3 = NFrame+L+merg-1;
    X =  [0, n1, n2, n3];
    Y = [p1, pstart, p2, pend];
    n = 0:n3;
    p_interp = spline(X,Y, n);
    %Phi = p_interp(n1:n3);
    Phi = p_interp;
        

 end
 
 %%
% *************************************************************************
% %  Hermite Phaseninterpolatio kombiniert mit der lienaren Interpolation 
%    der Frequenz mit der Matlab Funktion pchip 		
% % Software Version 2014-7
% % ***********************************************************************
 function [Phi, X, Y] = Hermite2_Phase (f1, f2, p1, p2, FS, Nrek, merg, b)
  
 
    omega1 = 2*pi*f1/FS;
    omega2 = 2*pi*f2/FS;
       
    NFrame = (Nrek -2*merg)/(b+1);  % Bl�ckel�nge vor und nach der L�cke
                                    % Bei B�ndelfehler nur die H�lfte
 	L = Nrek - 2*merg;				% tats�chliche L�ckenl�nge ohne Merging
  
 	
    deltaOmega = (omega2 - omega1)/(Nrek-1);
    omega = omega1 + deltaOmega*(0:Nrek-1);
    
    %Anfangsparameter initialisieren
  	pstart = p1 + omega1*(NFrame - merg) ;
    pstart = unwrap (0, mod(pstart, 2*pi));
      
   	pend = p2 + (merg-1)*omega2;         % End-Phase f�r den Rek. block
   	pend = unwrap (0, mod(pend, 2*pi));
 	
    Phase = pstart + omega.*[0:Nrek-1];
    Phase = mod(Phase, 2*pi);
    
    % Datenpunkte f�r die Interpolation festlegen
    x = [100 200 300 400 500 600  700 800];
    X = [1,         x  ,        Nrek-merg,      Nrek-1];
    Y = [pstart,    Phase(x),   p2,             pend];
    
    for (i=1:length(Y)) 
        Y(i) = unwrap(0, Y(i));
    end
%     Y = mod(Y, 2*pi);
%       plot(X,Y, 'o')
%       grid on
%       hold on
     n = 0:(Nrek-1);
     Phi = pchip(X, Y, n);
%       plot(n, Phi)
%       legend('Datenpunkte', 'Hermite')
%     

     
    % Vgl kubisch --> Phi = pstart + K*n + alpha*n.^2 + beta*n.^3;
    
    
%     n1 = NFrame-merg;
%     n2 = NFrame+L;
%     n3 = NFrame+L+merg-1;
%     X =  [0, n1, n2, n3];
%     Y = [p1, pstart, p2, pend];
%     n = 0:n3;
%     p_interp = pchip(X,Y, n);
%     %Phi = p_interp(n1:n3);
%     Phi = p_interp;
        
 end
 
 %%
% *************************************************************************
% %  Hermite Phaseninterpolation	mit der Matlab Funktion pchip 		
% % Software Version 2014-7
% % ***********************************************************************
 function [Phi, X, Y] = Hermite_Phase (f1, f2, p1, p2, FS, Nrek, merg, b)

     % L�nge mit 2x merging  
    omega1 = 2*pi*f1;%/FS;
    omega2 = 2*pi*f2;%/FS;
       
    NFrame = (Nrek -2*merg)/(b+1);  % Bl�ckel�nge vor und nach der L�cke
                                    % Bei B�ndelfehler nur die H�lfte
 	L = Nrek - 2*merg;				% tats�chliche L�ckenl�nge ohne Merging
  
 	%Anfangsparameter initialisieren
  	pstart = p1 + omega1*(NFrame - merg) ;
    %pstart = mod (pstart, 2*pi);
    pstart = unwrap (0, mod(pstart, 2*pi));
      
   	pend = p2 + (merg-1)*omega2;         % End-Phase f�r den Rek. block
   	pend = unwrap (0, mod(pend, 2*pi));
 	%pend = mod (pend,2*pi);
 
	% Parameter f�r die Hermite Interpolation 
    % allgemein gilf�r die Phase : Phi(n) = phi_0 + w*n;
    % w = 2*pi*f/FS;
    % s(x) = a*(x-x1)^2*(x-x2) + b*(x-x1)^2 + c(x-x1) +d;
    % x --> n = 0: Nrek-1;
    % s/x) =  Phi /8n) --Y die interpolierte Phase
    % s(x1) = y1 --> y1 = pstart (startphase)
    % s(x2) = y2  --> y2 = pend
    % s-diff die erste ableitung der Funktion s(x) nach x
    % s-diff(x1) =  m1 -->  m1 = w1;
    % s-diff(x2) =  m2 -->  m2 = w2;
    % daraus ergeben sich folgende Parameter:
    % c = m1 ----> c = w1;     d= y1 --> d = pstart;
    % a = (1/h^2) +(m2+m1 -(2 *(y2-y1)/h))
    % b =  (1/h)*((y2-y1)/h - m1)
    % h = x2-x1
    
    x1 = 0;
    x2 = Nrek-1;
    h = x2 -x1;
    m1 = omega1;
    m2 = omega2;
    y1 = pstart;
    y2 = pend;
    a = (1/h^2) +(m2+m1 -(2 *(y2-y1)/h));
    b =  (y2-y1)/h^2 - (m1/h);%(1/h)*((y2-y1)/h - m1);
    c = m1;
    d = y1;
    n = 0: Nrek-1;
    Phi1 = a*(n-x1).^2.*(n-x2) + b*(n-x1).^2 + c*(n-x1) + d;
    plot (Phi1)
    hold on 
    grid on
    for (i = 1: Nrek)
        n = i-1 ; % da Matlab ab 1 anf�ngt
        Phi(i) = a*(n-x1)^2*(n-x2) + b*(n-x1)^2 + c*(n-x1) + d;
    end
    plot (Phi, 'r')
   
   % Phi = mod(Phi, 2*pi); 

%     X = [1 100 200 300  400  500 600 700  800 900  1000 1100 1200 Nrek-1];
%     Y = Phi(X);
% %     Y = mod(Y, 2*pi);
% %      plot(X,Y, 'o')
% %      grid on
% %      hold on
%      Phi = pchip(X, Y, n);
% %      plot(n, Phi)
%      legend('Datenpunkte', 'Hermite')
%     

     
    % Vgl kubisch --> Phi = pstart + K*n + alpha*n.^2 + beta*n.^3;
    
    
%     n1 = NFrame-merg;
%     n2 = NFrame+L;
%     n3 = NFrame+L+merg-1;
%     X =  [0, n1, n2, n3];
%     Y = [p1, pstart, p2, pend];
%     n = 0:n3;
%     p_interp = pchip(X,Y, n);
%     %Phi = p_interp(n1:n3);
%     Phi = p_interp;
        

 end
 
  %%
% *************************************************************************
% %  Hermite Phaseninterpolation 
%	 Herleitung in Wikipedia
% % Software Version 2014-7
% % ***********************************************************************
 function [Phi, X, Y] = Hermite3_Phase (f1, f2, p1, p2, FS, Nrek, merg, b)
  
 
    omega1 = 2*pi*f1; %/FS;
    omega2 = 2*pi*f2; %/FS;
       
    NFrame = (Nrek -2*merg)/(b+1);  % Bl�ckel�nge vor und nach der L�cke
                                    % Bei B�ndelfehler nur die H�lfte
 	L = Nrek - 2*merg;				% tats�chliche L�ckenl�nge ohne Merging
  
 	
%     deltaOmega = (omega2 - omega1)/(Nrek-1);
%     omega = omega1 + deltaOmega*(0:Nrek-1);
    
    %Anfangsparameter initialisieren
  	pstart = p1 + omega1*(NFrame - merg)/FS ;
   % pstart = unwrap (0, mod(pstart, 2*pi));
   pstart = mod(pstart, 2*pi);
          
   	pend = p2 + (merg-1)*omega2/FS;         % End-Phase f�r den Rek. block
 %  	pend = unwrap (0, mod(pend, 2*pi));
    pend = mod(pend, 2*pi);
 	
%     
    p0 = pstart;
    p1 = pend;
    m0 = omega1;
    m1 = omega2;
    %t =  [0:Nrek-1]/(Nrek-1);
    t =  [0:Nrek-1]/(Nrek-1);
    
    Phi =  (2*t.^3 - 3*t.^2 + 1)*p0 + (t.^3 -2*t.^2 +t)*m0 + (-2*t.^3+3*t.^2)*p1 +(t.^3 -t.^2)*m1;
  %  Phi = mod(Phi, 2*pi);
% plot (Phi)
% hold on 
%     
% c =  osculate([0,1], [pstart, pend], [omega1, omega2]);
% 
% Phi = c(1)*t.^3 + c(2)*t.^2 +c(3)*t + c(4);
% plot (Phi, 'r')
% hold on
% X = [0,1]


       
 end

 %%
 function  p2 = unwrap( p1, p2)
    dp = p2-p1;
 	if (dp < -pi)
 		p2 = p2+2*pi;
    elseif (dp > pi)
 		p2 = p2- 2*pi;
    end
 end
 
