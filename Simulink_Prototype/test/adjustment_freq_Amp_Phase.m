function [A_start, A_end,F_start, F_end, Phi_start, Phi_end] = adjustment_freq_Amp_Phase(Tracks, sinM, FS, LastFrame, NextFrame, NFF)
 
global FFTLength;

%% normale Tracks

F_start = Tracks.f1;
A_start1 = Tracks.a1;
[A_start, Phi_start] = getFineAmplPhase(LastFrame, F_start, FS);  
A_start = A_start1;     %--> liefert bessere ergebnisse auch ODG
                

F_end  = Tracks.f2;
A_end1 = Tracks.a2;
[A_end, Phi_end]   = getFineAmplPhase(NextFrame, F_end, FS); 
A_end = A_end1;         %--> liefert bessere ergebnisse auch ODG
         
%% Birth --> Ak =0; Fk = Fk+1; phik = Phi_k+1 - F_k+1.* Nrek
% Amplitude der Frequenz bei Birth: mit 0 starten
posBirth = find(~Tracks.a1);
if (posBirth)
    Phi_start(posBirth)= Phi_end(posBirth) - F_end(posBirth)*NFF;
    Phi_start(posBirth) = mod(Phi_start(posBirth), 2*pi);
end
%% Death
% Amplitude der Frequenz bei Death: mit 0 enden
% Ak, omega_k; Phi_k -->  A_k+1 = 0 omega_k+1 = omega_k ; Phi_k+1 =
% Phi_k +omega_k*NFF
posDeath = find(~Tracks.a2);
if (posDeath)
    A_end(posDeath)=0;  
    F_end(posDeath)  = F_start(posDeath);
    Phi_end(posDeath)= Phi_start(posDeath) + F_start(posDeath)*NFF;
    Phi_end(posDeath) = mod(Phi_end(posDeath), 2*pi);
end


end

