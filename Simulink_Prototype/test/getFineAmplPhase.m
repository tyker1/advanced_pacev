function [ ampl, phi ] = getFineAmplPhase( Xsignal, f, FS )
%GETFINEAMPLPHASE Summary of this function goes here
%   Detailed explanation goes here
% f : Frequenz

ampl = [];
phi = [];
nx = length(Xsignal);
nf = length(f);
for i=1:nf
    [re, im] = MMSE(Xsignal, nx, f(i), FS);
    phi(i)  =  atan2(im, re);
    ampl(i) = sqrt(re^2+im^2);
end
   
end

%*************************************************************************
% neue Funktion MMSE
%*************************************************************************
function [re, im] = MMSE(X, nx, freq, FS)
	rVal = 1;	% cos(phase);
	iVal = 0;	% sin(phase);

	phase= 2*pi * freq / FS;
   	rInc = cos(phase);
	iInc = sin(phase);

    sinTab = [];
    cosTab = [];
    
%     for i= 1:nx 
% 		sinTab(i) = iVal;
% 		cosTab(i) = rVal;
% 			
% 		%move us to the next spot on the unit circle
% 		temp = rVal*rInc - iVal*iInc;
% 		iVal = rVal*iInc + iVal*rInc;
% 		rVal = temp; 
%     end
	 n = 0:nx-1;
     sinTab = sin(phase*n);
     cosTab = cos(phase*n);

%     win =[];
%     for n=1:nx
% 		win(n) = 0.5 - 0.5 * cos((2*pi*(n-1))/(nx-1));
%     end
     
    win = hann(nx);
	[re, im, temp] = CalcFactors(X,cosTab,sinTab,win,nx);
	
end
%%
%*************************************************************************
% neue Version --> Beginn
 function [re, im, temp] = CalcFactors(x,cosTab,sinTab,win,nx)
	% x in: input signal 
	% cos,		 cosine for given line 
	% sin,		 sine for given line 
	% win,		 window for corr. and error 
 	winLen = length(win);	%in: window length 
% 	 re,        factor for cosine 
% 	 im,        factor for sine 
% 	 temp       neg. signal power 
% 	
 
     ccSum = 0;
     ssSum = 0;
     csSum = 0;
     cxSum = 0;
     sxSum = 0;

% % gültige Version 2014-3	
% for i = 1:winLen
% 		 xCos = x(i)*cos(i);% Originalsignal gewichtet mit Harmonischen Cosinus
% 		 xSin = x(i)*sin(i);%// Originalsignal gewichtet mit Harmonischen Sinus
% 		 if (win==0)
% 			wi_2 = 1;
% 		 else
% 			wi_2 = win(i)*win(i);			% Leistung des Hanning-Fensters
%          end
% 		 ccSum = ccSum + wi_2*cos(i)*cos(i);   % Leistung des Cosinus
% 		 ssSum = ssSum + wi_2*sin(i)*sin(i);   % leistung des Sinus
% 		 csSum = csSum + wi_2*cos(i)*sin(i);   % Aufsummierung von der Leistung des Fensters mit sinus und cosinus
% 		 cxSum = cxSum + wi_2*xCos;            % Leistung des Fensters gewichtet mit harmonischen Cosinus
% 		 sxSum = sxSum + wi_2*xSin;            % Leistung des Fensters gewichtet mit harmonischen Cosinus
%      end
% 	
%% modifizierte Version 2014-3
% 	 
		
        n = 0:winLen;
        wi_2 = win.^2; 			% Leistung des Hanning-Fensters       
        
        cosTab = cosTab';
        sinTab = sinTab';
        xCos = x'.*cosTab;% Originalsignal gewichtet mit Harmonischen Cosinus        
        xSin = x'.*sinTab;%// Originalsignal gewichtet mit Harmonischen Sinus
		 
     
         CC = wi_2.*cosTab.^2; 
         ccSum = sum(CC);
         
		 SS = wi_2.*sinTab.^2;
         ssSum = sum(SS); 
        
         CS = wi_2.*cosTab.*sinTab;
 		 csSum = sum(CS); 

         CX = wi_2.*xCos;
         cxSum = sum(CX); 

         SX = wi_2.*xSin;
         sxSum = sum(SX);
		 
     
	 % höchstwahrscheinlich Determinante
	 %det = max(ccSum*ssSum - csSum*csSum, IL_MINdouble); % IL_MINdouble ((double)1e-35)
     det = max(ccSum*ssSum - csSum*csSum, 1e-35); % IL_MINdouble ((double)1e-35)
	 cosFact =(ssSum*cxSum - csSum*sxSum)/det;
	 sinFact =(-ccSum*sxSum + csSum*cxSum)/det;
	 qeDiff = -(cosFact)*cxSum +(sinFact)*sxSum;
     
     % Ausgabe
     re = cosFact ;
     im = sinFact;
     temp = qeDiff;
    %return ;

%}
 end


% neue Version Ende
% ********************************** 
%%


% /***********************************************/
% function [re, im, temp] = CalcFactors(x,cos,sin,win,nx);
% % void CalcFactors(
% % 	double	*x,			// in: input signal 
% % 	double	*cos,		// in: cosine for given line 
% % 	double	*sin,		// in: sine for given line 
% % 	double	*win,		// in: window for corr. and error 
% % 	int	winLen,			// in: window length 
% % 	double	*cosFact,	// out: factor for cosine 
% % 	double	*sinFact,	// out: factor for sine 
% % 	double	*qeDiff)	// out: neg. signal power 
% % 	
% % {
% % 	 int i;
% % 	 double xCos, xSin, wi_2/*, wi_ex*/;
% % 	 double ccSum, ssSum, csSum, cxSum, sxSum;
% % 	 double det;
% % 	 
%  	ccSum = 0;
%     ssSum = 0;
%     csSum = 0;
%     cxSum = 0;
%     sxSum = 0;
%     winLen = length(win);  
% % 	 for (i = 0; i < winLen; i++)
% %********************************
% %folgende Block ohne Schleife implementieren!!!!!!!!!!!!!!!!!!!!!!!
%     for  i=1:winLen
%  		 xCos = x(i)*cos(i); % Originalsignal gewichtet mit Harm. Cosinus
%  		 xSin = x(i)*sin(i); % Originalsignal gewichtet mit Harm. Sinus
%  		 if (win==0)
%  			wi_2 = 1;
%  		 else
%  			%wi_2 = win(i)*win(i); % Leistung des Hanning-Fensters
%             wi_2 = win(i)*win(i); % Leistung des Hanning-Fensters
% 
%          end
%  		 ccSum = ccSum+ wi_2*cos(i)*cos(i); % leistung des Cosinus
%  		 ssSum = ssSum + wi_2*sin(i)*sin(i); % leistung des Sinus
%  		 csSum = csSum +wi_2*cos(i)*sin(i); % Aufsummierung von der Leistung des Fensters mit sinus und cosinus
%  		 cxSum = cxSum+wi_2*xCos;		% Leistung des Fensters gewichtet mit harmonischen Cosinus
%  		 sxSum = sxSum+ wi_2*xSin;		% Leistung des Fensters gewichtet mit harmonischen Cosinus
% % 
%     end
% %***************************
% % 	 }
% % 	 
% % 	 // höchstwahrscheinlich Determinante
%  	 det = max(ccSum*ssSum - csSum*csSum, 1e-35); 
%  	 %cosFact =(ssSum*cxSum - csSum*sxSum)/det;
%      re =(ssSum*cxSum - csSum*sxSum)/det;
%  	 %sinFact =(-ccSum*sxSum + csSum*cxSum)/det;
%      im=(-ccSum*sxSum + csSum*cxSum)/det;
%  	 %qeDiff = -(*cosFact)*cxSum +(*sinFact)*sxSum;
%      temp = -re*cxSum +im*sxSum;
% end
% 
