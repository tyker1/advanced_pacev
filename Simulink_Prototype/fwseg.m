function fws = fwseg(clean_speech, processed_speech,sample_rate)

% ----------------------------------------------------------------------
% Check the length of the clean and processed speech.  Must be the same.
% ----------------------------------------------------------------------

clean_length      = length(clean_speech);
processed_length  = length(processed_speech);
if (clean_length ~= processed_length)
  disp('Error: Files  must have same length.');
  return
end

% ----------------------------------------------------------------------
% Global Variables
% ----------------------------------------------------------------------

winlength   = round(30*sample_rate/1000); 	   % window length in samples
skiprate    = floor(winlength/4);		   % window skip in samples
max_freq    = sample_rate/2;	   % maximum bandwidth
num_crit    = 25;		   % number of critical bands
USE_25=1;
n_fft       = 2^nextpow2(2*winlength);
n_fftby2    = n_fft/2;		   % FFT size/2
gamma=0.2;  % power exponent

% ----------------------------------------------------------------------
% Critical Band Filter Definitions (Center Frequency and Bandwidths in Hz)
% ----------------------------------------------------------------------

cent_freq(1)  = 50.0000;   bandwidth(1)  = 70.0000;
cent_freq(2)  = 120.000;   bandwidth(2)  = 70.0000;
cent_freq(3)  = 190.000;   bandwidth(3)  = 70.0000;
cent_freq(4)  = 260.000;   bandwidth(4)  = 70.0000;
cent_freq(5)  = 330.000;   bandwidth(5)  = 70.0000;
cent_freq(6)  = 400.000;   bandwidth(6)  = 70.0000;
cent_freq(7)  = 470.000;   bandwidth(7)  = 70.0000;
cent_freq(8)  = 540.000;   bandwidth(8)  = 77.3724;
cent_freq(9)  = 617.372;   bandwidth(9)  = 86.0056;
cent_freq(10) = 703.378;   bandwidth(10) = 95.3398;
cent_freq(11) = 798.717;   bandwidth(11) = 105.411;
cent_freq(12) = 904.128;   bandwidth(12) = 116.256;
cent_freq(13) = 1020.38;   bandwidth(13) = 127.914;
cent_freq(14) = 1148.30;   bandwidth(14) = 140.423;
cent_freq(15) = 1288.72;   bandwidth(15) = 153.823;
cent_freq(16) = 1442.54;   bandwidth(16) = 168.154;
cent_freq(17) = 1610.70;   bandwidth(17) = 183.457;
cent_freq(18) = 1794.16;   bandwidth(18) = 199.776;
cent_freq(19) = 1993.93;   bandwidth(19) = 217.153;
cent_freq(20) = 2211.08;   bandwidth(20) = 235.631;
cent_freq(21) = 2446.71;   bandwidth(21) = 255.255;
cent_freq(22) = 2701.97;   bandwidth(22) = 276.072;
cent_freq(23) = 2978.04;   bandwidth(23) = 298.126;
cent_freq(24) = 3276.17;   bandwidth(24) = 321.465;
cent_freq(25) = 3597.63;   bandwidth(25) = 346.136;

% articulation index weights
W=[0.003, 0.003, 0.003, 0.007, 0.010, 0.016, 0.016, 0.017, 0.017,...
    0.022, 0.027, 0.028, 0.030, 0.032, 0.034, 0.035, 0.037, 0.036,...
    0.036, 0.033, 0.030, 0.029, 0.027, 0.026, 0.026];

    if USE_25==0  % use 13 bands
        % ----- lump adjacent filters together ----------------
        k=2;
        cent_freq2 = zeros(1,13);
        bandwidth2 = zeros(1,13);
        W2 = zeros(1,13);
        cent_freq2(1)=cent_freq(1);
        bandwidth2(1)=bandwidth(1)+bandwidth(2);
        W2(1)=W(1);
        for i=2:13
            cent_freq2(i)=cent_freq2(i-1)+bandwidth2(i-1);
            bandwidth2(i)=bandwidth(k)+bandwidth(k+1);
            W2(i)=0.5*(W(k)+W(k+1));
            k=k+2;
        end

        sumW=sum(W2);
        bw_min      = bandwidth2 (1);	   % minimum critical bandwidth
    else
        sumW=sum(W);
        bw_min=bandwidth(1);
    end


% ----------------------------------------------------------------------
% Set up the critical band filters.  Note here that Gaussianly shaped
% filters are used.  Also, the sum of the filter weights are equivalent
% for each critical band filter.  Filter less than -30 dB and set to
% zero.
% ----------------------------------------------------------------------

    crit_filter = zeros(num_crit,n_fftby2);
    all_f0 = zeros(1,num_crit);
    min_factor = exp (-30.0 / (2.0 * 2.303));       % -30 dB point of filter
    if USE_25==0

        num_crit=length(cent_freq2);

        for i = 1:num_crit
            f0 = (cent_freq2 (i) / max_freq) * (n_fftby2);
            all_f0(i) = floor(f0);
            bw = (bandwidth2 (i) / max_freq) * (n_fftby2);
            norm_factor = log(bw_min) - log(bandwidth2(i));
            j = 0:1:n_fftby2-1;
            crit_filter(i,:) = exp (-11 *(((j - floor(f0)) ./bw).^2) + norm_factor);
            crit_filter(i,:) = crit_filter(i,:).*(crit_filter(i,:) > min_factor);
        end

    else
        for i = 1:num_crit
            f0 = (cent_freq (i) / max_freq) * (n_fftby2);
            all_f0(i) = floor(f0);
            bw = (bandwidth (i) / max_freq) * (n_fftby2);
            norm_factor = log(bw_min) - log(bandwidth(i));
            j = 0:1:n_fftby2-1;
            crit_filter(i,:) = exp (-11 *(((j - floor(f0)) ./bw).^2) + norm_factor);
            crit_filter(i,:) = crit_filter(i,:).*(crit_filter(i,:) > min_factor);
        end
    end



    num_frames = floor(clean_length/skiprate-(winlength/skiprate)); % number of frames
    start      = 1;					% starting sample
    window     = 0.5*(1 - cos(2*pi*(1:winlength)'/(winlength+1)));

    distortion = zeros(1,num_frames);

    for frame_count = 1:num_frames

       % ----------------------------------------------------------
       % (1) Get the Frames for the test and reference speech. 
       %     Multiply by Hanning Window.
       % ----------------------------------------------------------

       clean_frame = clean_speech(start:start+winlength-1);
       processed_frame = processed_speech(start:start+winlength-1);
       clean_frame = clean_frame.*window';
       processed_frame = processed_frame.*window';

       % ----------------------------------------------------------
       % (2) Compute the magnitude Spectrum of Clean and Processed
       % ----------------------------------------------------------


           clean_spec     = abs(fft(clean_frame,n_fft))./n_fft*2;
           processed_spec = abs(fft(processed_frame,n_fft))./n_fft*2; 


       % ----------------------------------------------------------
       % (3) Compute Filterbank Output Energies 
       % ----------------------------------------------------------

       clean_energy = clean_spec(1:n_fftby2)*crit_filter';
       processed_energy = processed_spec(1:n_fftby2)*crit_filter';
       error_energy = (clean_energy - processed_energy).^2;
       error_energy(error_energy<eps) = eps;
       W_freq = clean_energy.^gamma;
       SNRlog=10*log10((clean_energy.^2)./error_energy);



       fwSNR=sum(W_freq.*SNRlog)/sum(W_freq);

       distortion(frame_count)=min(max(fwSNR,-10),35);

       start = start + skiprate;

    end
    fws = mean(distortion);
end

