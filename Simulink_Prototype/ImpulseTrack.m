function [ Tracks ] = ImpulseTrack( Elektrode2, MidFreq , numElektrode)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

Frames = length(Elektrode2(:,1));
Tracks = struct('A1',{[]},'A2',{[]},'F1',{[]},'F2',{[]});

for i = 1:Frames-1
    currentFrame = Elektrode2(i,:);
    nextFrame = Elektrode2(i+1,:);
    idxCurrent = find (currentFrame);
    idxNext = find(nextFrame);
    ampCurrent = currentFrame(idxCurrent);
    ampNext = nextFrame(idxNext);
    normCurrent = idxCurrent ./ 8;
    normNext = idxNext ./ 8;
    VectCurrent = ones(1,numElektrode).*numElektrode + 1;
    VectCurrent(ceil(normCurrent)) = normCurrent;
    VectNext = ones(1,numElektrode).*numElektrode + 1;
    VectNext(ceil(normNext)) = normNext;
    idx = 1;
    idxC = 1;
    idxN = 1;
    A1 = [];
    A2 = [];
    F1 = [];
    F2 = [];
    for j = 1:numElektrode
        iCurrent = VectCurrent(j);
        iNext = VectNext(j);
        if (iCurrent <= j) && (iNext <= j)
            A1(idx) = ampCurrent(idxC);
            A2(idx) = ampNext(idxN);
            F1(idx) = MidFreq(idxCurrent(idxC));
            F2(idx) = MidFreq(idxNext(idxN));
            idx = idx + 1;
            idxC = idxC + 1;
            idxN = idxN + 1;
        else if (iCurrent <= j) && (iNext > j)
                A1(idx) = ampCurrent(idxC);
                A2(idx) = 0;
                F1(idx) = MidFreq(idxCurrent(idxC));
                F2(idx) = F1(idx);
                idx = idx + 1;
                idxC = idxC + 1;
            else if (iNext <= j) && (iCurrent > j)
                    A1(idx) = 0;
                    A2(idx) = ampNext(idxN);
                    F1(idx) = MidFreq(idxNext(idxN));
                    F2(idx) = F1(idx);
                    idx = idx + 1;
                    idxN = idxN + 1;
                end
            end
        end
    end
    Tracks(i).A1 = A1;
    Tracks(i).A2 = A2;
    Tracks(i).F1 = F1;
    Tracks(i).F2 = F2;
end

%Letzter Frame
currentFrame = Elektrode2(Frames,:);
idxCurrent = find(currentFrame);
ampCurrent = currentFrame(idxCurrent);
LenCurrent = length(idxCurrent);
for i = 1:LenCurrent
    Tracks(Frames).A1(i) = ampCurrent(i);
    Tracks(Frames).A2(i) = 0;
    Tracks(Frames).F1(i) = MidFreq(idxCurrent(i));
    Tracks(Frames).F2(i) = Tracks(Frames).F1(i);
end
end

