function [IntPhi] = phiInterpolation(f1, f2, p1, p2, Len ,fs)
    %IntPhi = f1;
    
    n = 0:Len-1;
    omega1 = f1*2*pi * 1/fs;
    omega2 = f2*2*pi * 1/fs;
    e = p1;
    k = omega1;
    M = ((p1 + omega1 * Len - p2) + (omega2-omega1)*Len/2) * 1/(2*pi);
    M = ceil(M);

%     param1 = p2 - p1 - omega1*Len ; %+ omega2;   % Version 2014-6-test5    
%     param1 =  mod(param1, 2*pi);
%    	param1 = unwrap (0, param1);
%     
%     param2 = omega2 - omega1;
%     param2 = unwrap (0, param2);
% 
%     alpha = (3 * param1 / (Len^2) ) - (param2/Len);
%   	beta  = -( param1*2/(Len^3)) +  (param2/(Len^2));
% 
%     IntPhi = e + k*n + alpha*n.^2 + beta*n.^3;
%     IntPhi = mod(IntPhi, 2*pi);
    
    
    MatQ = [ 3/Len^2, - 1/Len; -2/Len^3, 1/Len^2];
    MatV = [p2 - p1 - omega1 * Len + 2*pi*M; omega2 - omega1];
    %MatV = mod(MatV,2*pi);
    temp = MatQ * MatV;
    alpha = temp(1);
    beta = temp(2);
    IntPhi = e + k .* n + alpha .* n.^2 + beta .* n.^3;
   % IntPhi = mod(IntPhi,2*pi);

    %%
%     %Anfangsparameter initialisieren
%   	pstart = p1 + omega1*(NFrame - merg) ; % Lewei mit 0 starten
%     pstart = mod (pstart, 2*pi);
%       
%    	pend = p2 + (merg-1)*omega2;         % End-Phase f�r den Rek. block
%  	pend = mod (pend,2*pi);             %
%  
%  	% Phasenunwrapping 
%     param1 = pend - pstart - omega1*Nrek + omega2;   % Version 2014-6-test5
%     %param1 = pend - pstart - omega1*Nrek;   % Version 2014-6-test5
%     param1 = mod(param1, 2*pi); % 16.04
%     
%     %M = round((  (pstart -pend) +(omega1+omega2)*(Nrek/2) )/(2*pi))
%      %param1 = pend - pstart - omega1*Nrek + 2*pi*M;   % Version 2014-6-test5
%       
%   	param1 =  mod(param1, 2*pi);
%   	param1 = unwrap (0, param1);
% 
%     param2 = omega2 - omega1;
% %     param2 = unwrap (0, param2);
%  
%     K = omega1;
%     alpha = (3 * param1 / (Nrek^2) ) - (param2/Nrek);
%   	beta  = -( param1*2/(Nrek^3)) +  (param2/(Nrek^2));
% 
%     n = 0:Nrek-1;
%     Phi = pstart + K*n + alpha*n.^2 + beta*n.^3;
%     Phi = mod(Phi, 2*pi);
%     
    
    
    %%
end