/* Include files */

#include "pacev_try_sfun.h"
#include "c2_pacev_try.h"
#include "mwmathutil.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "pacev_try_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);
static const mxArray* sf_opaque_get_hover_data_for_msg(void *chartInstance,
  int32_T msgSSID);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c2_const_M                     (192.0)
#define c2_const_N                     (6.0)
#define c2_b_M                         (192.0)
#define c2_b_N                         (6.0)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c2_debug_family_names[14] = { "r_sqr", "a_env", "i", "idx",
  "M", "N", "nargin", "nargout", "u", "electrode", "index", "output", "env_out",
  "stimulation_elektrode" };

/* Function Declarations */
static void initialize_c2_pacev_try(SFc2_pacev_tryInstanceStruct *chartInstance);
static void initialize_params_c2_pacev_try(SFc2_pacev_tryInstanceStruct
  *chartInstance);
static void enable_c2_pacev_try(SFc2_pacev_tryInstanceStruct *chartInstance);
static void disable_c2_pacev_try(SFc2_pacev_tryInstanceStruct *chartInstance);
static void c2_update_debugger_state_c2_pacev_try(SFc2_pacev_tryInstanceStruct
  *chartInstance);
static const mxArray *get_sim_state_c2_pacev_try(SFc2_pacev_tryInstanceStruct
  *chartInstance);
static void set_sim_state_c2_pacev_try(SFc2_pacev_tryInstanceStruct
  *chartInstance, const mxArray *c2_st);
static void finalize_c2_pacev_try(SFc2_pacev_tryInstanceStruct *chartInstance);
static void sf_gateway_c2_pacev_try(SFc2_pacev_tryInstanceStruct *chartInstance);
static void mdl_start_c2_pacev_try(SFc2_pacev_tryInstanceStruct *chartInstance);
static void c2_chartstep_c2_pacev_try(SFc2_pacev_tryInstanceStruct
  *chartInstance);
static void initSimStructsc2_pacev_try(SFc2_pacev_tryInstanceStruct
  *chartInstance);
static void c2_fftft_invoke(SFc2_pacev_tryInstanceStruct *chartInstance, real_T
  c2_b_u[4096], real_T c2_y[4096]);
static void c2_fftEnv_invoke(SFc2_pacev_tryInstanceStruct *chartInstance, real_T
  c2_b_u[4096], real_T c2_y[192]);
static void c2_Sel_invoke(SFc2_pacev_tryInstanceStruct *chartInstance, real_T
  c2_b_u[192], real_T c2_y[6], real_T c2_y1[6]);
static void init_script_number_translation(uint32_T c2_machineNumber, uint32_T
  c2_chartNumber, uint32_T c2_instanceNumber);
static const mxArray *c2_sf_marshallOut(void *chartInstanceVoid, void *c2_inData);
static void c2_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_stimulation_elektrode, const char_T *c2_identifier, real_T
  c2_y[1152]);
static void c2_b_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId, real_T c2_y[1152]);
static void c2_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_b_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static void c2_c_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_env_out, const char_T *c2_identifier, real_T c2_y[192]);
static void c2_d_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId, real_T c2_y[192]);
static void c2_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_c_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static void c2_e_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_index, const char_T *c2_identifier, real_T c2_y[6]);
static void c2_f_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId, real_T c2_y[6]);
static void c2_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_d_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static const mxArray *c2_e_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static real_T c2_g_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_f_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static void c2_h_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId, real_T c2_y[4096]);
static void c2_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static real_T c2_mod(SFc2_pacev_tryInstanceStruct *chartInstance, real_T c2_x);
static const mxArray *c2_g_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static int32_T c2_i_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static uint8_T c2_j_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_is_active_c2_pacev_try, const char_T *c2_identifier);
static uint8_T c2_k_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId);
static void init_dsm_address_info(SFc2_pacev_tryInstanceStruct *chartInstance);
static void init_simulink_io_address(SFc2_pacev_tryInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c2_pacev_try(SFc2_pacev_tryInstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    initSimStructsc2_pacev_try(chartInstance);
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  chartInstance->c2_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c2_is_active_c2_pacev_try = 0U;
}

static void initialize_params_c2_pacev_try(SFc2_pacev_tryInstanceStruct
  *chartInstance)
{
  real_T c2_d0;
  real_T c2_d1;
  sf_mex_import_named("M", sf_mex_get_sfun_param(chartInstance->S, 0, 0), &c2_d0,
                      0, 0, 0U, 0, 0U, 0);
  chartInstance->c2_M = c2_d0;
  sf_mex_import_named("N", sf_mex_get_sfun_param(chartInstance->S, 1, 0), &c2_d1,
                      0, 0, 0U, 0, 0U, 0);
  chartInstance->c2_N = c2_d1;
}

static void enable_c2_pacev_try(SFc2_pacev_tryInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c2_pacev_try(SFc2_pacev_tryInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c2_update_debugger_state_c2_pacev_try(SFc2_pacev_tryInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static const mxArray *get_sim_state_c2_pacev_try(SFc2_pacev_tryInstanceStruct
  *chartInstance)
{
  const mxArray *c2_st;
  const mxArray *c2_y = NULL;
  const mxArray *c2_b_y = NULL;
  const mxArray *c2_c_y = NULL;
  const mxArray *c2_d_y = NULL;
  const mxArray *c2_e_y = NULL;
  const mxArray *c2_f_y = NULL;
  uint8_T c2_hoistedGlobal;
  const mxArray *c2_g_y = NULL;
  c2_st = NULL;
  c2_st = NULL;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_createcellmatrix(6, 1), false);
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", *chartInstance->c2_electrode, 0, 0U,
    1U, 0U, 1, 6), false);
  sf_mex_setcell(c2_y, 0, c2_b_y);
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", *chartInstance->c2_env_out, 0, 0U,
    1U, 0U, 1, 192), false);
  sf_mex_setcell(c2_y, 1, c2_c_y);
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", *chartInstance->c2_index, 0, 0U, 1U,
    0U, 1, 6), false);
  sf_mex_setcell(c2_y, 2, c2_d_y);
  c2_e_y = NULL;
  sf_mex_assign(&c2_e_y, sf_mex_create("y", *chartInstance->c2_output, 0, 0U, 1U,
    0U, 1, 192), false);
  sf_mex_setcell(c2_y, 3, c2_e_y);
  c2_f_y = NULL;
  sf_mex_assign(&c2_f_y, sf_mex_create("y",
    *chartInstance->c2_stimulation_elektrode, 0, 0U, 1U, 0U, 2, 192, 6), false);
  sf_mex_setcell(c2_y, 4, c2_f_y);
  c2_hoistedGlobal = chartInstance->c2_is_active_c2_pacev_try;
  c2_g_y = NULL;
  sf_mex_assign(&c2_g_y, sf_mex_create("y", &c2_hoistedGlobal, 3, 0U, 0U, 0U, 0),
                false);
  sf_mex_setcell(c2_y, 5, c2_g_y);
  sf_mex_assign(&c2_st, c2_y, false);
  return c2_st;
}

static void set_sim_state_c2_pacev_try(SFc2_pacev_tryInstanceStruct
  *chartInstance, const mxArray *c2_st)
{
  const mxArray *c2_b_u;
  real_T c2_dv0[6];
  int32_T c2_i0;
  real_T c2_dv1[192];
  int32_T c2_i1;
  real_T c2_dv2[6];
  int32_T c2_i2;
  real_T c2_dv3[192];
  int32_T c2_i3;
  real_T c2_dv4[1152];
  int32_T c2_i4;
  chartInstance->c2_doneDoubleBufferReInit = true;
  c2_b_u = sf_mex_dup(c2_st);
  c2_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell("electrode",
    c2_b_u, 0)), "electrode", c2_dv0);
  for (c2_i0 = 0; c2_i0 < 6; c2_i0++) {
    (*chartInstance->c2_electrode)[c2_i0] = c2_dv0[c2_i0];
  }

  c2_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell("env_out",
    c2_b_u, 1)), "env_out", c2_dv1);
  for (c2_i1 = 0; c2_i1 < 192; c2_i1++) {
    (*chartInstance->c2_env_out)[c2_i1] = c2_dv1[c2_i1];
  }

  c2_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell("index", c2_b_u,
    2)), "index", c2_dv2);
  for (c2_i2 = 0; c2_i2 < 6; c2_i2++) {
    (*chartInstance->c2_index)[c2_i2] = c2_dv2[c2_i2];
  }

  c2_c_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell("output",
    c2_b_u, 3)), "output", c2_dv3);
  for (c2_i3 = 0; c2_i3 < 192; c2_i3++) {
    (*chartInstance->c2_output)[c2_i3] = c2_dv3[c2_i3];
  }

  c2_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "stimulation_elektrode", c2_b_u, 4)), "stimulation_elektrode", c2_dv4);
  for (c2_i4 = 0; c2_i4 < 1152; c2_i4++) {
    (*chartInstance->c2_stimulation_elektrode)[c2_i4] = c2_dv4[c2_i4];
  }

  chartInstance->c2_is_active_c2_pacev_try = c2_j_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("is_active_c2_pacev_try", c2_b_u, 5)),
    "is_active_c2_pacev_try");
  sf_mex_destroy(&c2_b_u);
  c2_update_debugger_state_c2_pacev_try(chartInstance);
  sf_mex_destroy(&c2_st);
}

static void finalize_c2_pacev_try(SFc2_pacev_tryInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c2_pacev_try(SFc2_pacev_tryInstanceStruct *chartInstance)
{
  int32_T c2_i5;
  int32_T c2_i6;
  int32_T c2_i7;
  int32_T c2_i8;
  int32_T c2_i9;
  int32_T c2_i10;
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
  for (c2_i5 = 0; c2_i5 < 4096; c2_i5++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c2_u)[c2_i5], 0U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
  }

  chartInstance->c2_sfEvent = CALL_EVENT;
  c2_chartstep_c2_pacev_try(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY(_pacev_tryMachineNumber_,
    chartInstance->chartNumber, chartInstance->instanceNumber);
  for (c2_i6 = 0; c2_i6 < 6; c2_i6++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c2_electrode)[c2_i6], 1U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
  }

  for (c2_i7 = 0; c2_i7 < 6; c2_i7++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c2_index)[c2_i7], 2U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
  }

  for (c2_i8 = 0; c2_i8 < 192; c2_i8++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c2_output)[c2_i8], 3U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
  }

  for (c2_i9 = 0; c2_i9 < 192; c2_i9++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c2_env_out)[c2_i9], 4U, 1U, 0U,
                          chartInstance->c2_sfEvent, false);
  }

  for (c2_i10 = 0; c2_i10 < 1152; c2_i10++) {
    _SFD_DATA_RANGE_CHECK((*chartInstance->c2_stimulation_elektrode)[c2_i10], 5U,
                          1U, 0U, chartInstance->c2_sfEvent, false);
  }
}

static void mdl_start_c2_pacev_try(SFc2_pacev_tryInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c2_chartstep_c2_pacev_try(SFc2_pacev_tryInstanceStruct
  *chartInstance)
{
  int32_T c2_i11;
  uint32_T c2_debug_family_var_map[14];
  real_T c2_b_u[4096];
  real_T c2_r_sqr[4096];
  real_T c2_a_env[192];
  real_T c2_i;
  real_T c2_idx;
  real_T c2_c_M;
  real_T c2_c_N;
  real_T c2_nargin = 3.0;
  real_T c2_nargout = 5.0;
  real_T c2_b_electrode[6];
  real_T c2_b_index[6];
  real_T c2_b_output[192];
  real_T c2_b_env_out[192];
  real_T c2_b_stimulation_elektrode[1152];
  int32_T c2_i12;
  real_T c2_c_u[4096];
  real_T c2_dv5[4096];
  int32_T c2_i13;
  int32_T c2_i14;
  real_T c2_b_r_sqr[4096];
  real_T c2_dv6[192];
  int32_T c2_i15;
  int32_T c2_i16;
  real_T c2_b_a_env[192];
  real_T c2_c_electrode[6];
  real_T c2_c_index[6];
  int32_T c2_i17;
  int32_T c2_i18;
  int32_T c2_i19;
  int32_T c2_i20;
  int32_T c2_b_i;
  int32_T c2_i21;
  int32_T c2_c_i;
  int32_T c2_i22;
  real_T c2_d2;
  int32_T c2_i23;
  real_T c2_A;
  real_T c2_b_A;
  int32_T c2_i24;
  real_T c2_x;
  real_T c2_b_x;
  real_T c2_c_x;
  real_T c2_d_x;
  real_T c2_y;
  real_T c2_b_y;
  int32_T c2_i25;
  real_T c2_c_A;
  real_T c2_e_x;
  real_T c2_f_x;
  int32_T c2_i26;
  real_T c2_c_y;
  real_T c2_d_A;
  real_T c2_g_x;
  real_T c2_h_x;
  real_T c2_d_y;
  real_T c2_e_A;
  real_T c2_i_x;
  real_T c2_j_x;
  real_T c2_e_y;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
  for (c2_i11 = 0; c2_i11 < 4096; c2_i11++) {
    c2_b_u[c2_i11] = (*chartInstance->c2_u)[c2_i11];
  }

  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 14U, 14U, c2_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c2_r_sqr, 0U, c2_f_sf_marshallOut,
    c2_e_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c2_a_env, 1U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_i, 2U, c2_e_sf_marshallOut,
    c2_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_idx, 3U, c2_e_sf_marshallOut,
    c2_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c2_c_M, 4U, c2_e_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML(&c2_c_N, 5U, c2_e_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 6U, c2_e_sf_marshallOut,
    c2_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 7U, c2_e_sf_marshallOut,
    c2_d_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML(c2_b_u, 8U, c2_d_sf_marshallOut);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c2_b_electrode, 9U, c2_c_sf_marshallOut,
    c2_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c2_b_index, 10U, c2_c_sf_marshallOut,
    c2_c_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c2_b_output, 11U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c2_b_env_out, 12U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(c2_b_stimulation_elektrode, 13U,
    c2_sf_marshallOut, c2_sf_marshallIn);
  c2_c_N = c2_b_N;
  c2_c_M = c2_b_M;
  CV_EML_FCN(0, 0);
  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 3);
  for (c2_i12 = 0; c2_i12 < 4096; c2_i12++) {
    c2_c_u[c2_i12] = c2_b_u[c2_i12];
  }

  c2_fftft_invoke(chartInstance, c2_c_u, c2_dv5);
  for (c2_i13 = 0; c2_i13 < 4096; c2_i13++) {
    c2_r_sqr[c2_i13] = c2_dv5[c2_i13];
  }

  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 4);
  for (c2_i14 = 0; c2_i14 < 4096; c2_i14++) {
    c2_b_r_sqr[c2_i14] = c2_r_sqr[c2_i14];
  }

  c2_fftEnv_invoke(chartInstance, c2_b_r_sqr, c2_dv6);
  for (c2_i15 = 0; c2_i15 < 192; c2_i15++) {
    c2_a_env[c2_i15] = c2_dv6[c2_i15];
  }

  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 5);
  for (c2_i16 = 0; c2_i16 < 192; c2_i16++) {
    c2_b_a_env[c2_i16] = c2_a_env[c2_i16];
  }

  c2_Sel_invoke(chartInstance, c2_b_a_env, c2_c_electrode, c2_c_index);
  for (c2_i17 = 0; c2_i17 < 6; c2_i17++) {
    c2_b_electrode[c2_i17] = c2_c_electrode[c2_i17];
  }

  for (c2_i18 = 0; c2_i18 < 6; c2_i18++) {
    c2_b_index[c2_i18] = c2_c_index[c2_i18];
  }

  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 7);
  for (c2_i19 = 0; c2_i19 < 192; c2_i19++) {
    c2_b_output[c2_i19] = 0.0;
  }

  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 8);
  for (c2_i20 = 0; c2_i20 < 192; c2_i20++) {
    c2_b_env_out[c2_i20] = c2_a_env[c2_i20];
  }

  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 10);
  c2_i = 1.0;
  c2_b_i = 0;
  while (c2_b_i < 6) {
    c2_i = 1.0 + (real_T)c2_b_i;
    CV_EML_FOR(0, 1, 0, 1);
    _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 11);
    c2_b_output[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 298, 16, MAX_uint32_T, (int32_T)sf_integer_check
      (chartInstance->S, 1U, 298U, 16U, c2_b_index[sf_eml_array_bounds_check
       (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 298, 16, MAX_uint32_T,
        (int32_T)sf_integer_check(chartInstance->S, 1U, 298U, 16U, c2_i), 1, 6)
       - 1]), 1, 192) - 1] = c2_b_electrode[sf_eml_array_bounds_check
      (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 317, 12, MAX_uint32_T,
       (int32_T)sf_integer_check(chartInstance->S, 1U, 317U, 12U, c2_i), 1, 6) -
      1];
    c2_b_i++;
    _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
  }

  CV_EML_FOR(0, 1, 0, 0);
  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 19);
  for (c2_i21 = 0; c2_i21 < 1152; c2_i21++) {
    c2_b_stimulation_elektrode[c2_i21] = 0.0;
  }

  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 20);
  c2_idx = 1.0;
  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 22);
  c2_i = 1.0;
  c2_c_i = 0;
  while (c2_c_i < 192) {
    c2_i = 1.0 + (real_T)c2_c_i;
    CV_EML_FOR(0, 1, 1, 1);
    _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 23);
    c2_d2 = c2_b_output[sf_eml_array_bounds_check(sfGlobalDebugInstanceStruct,
      chartInstance->S, 1U, 725, 9, MAX_uint32_T, (int32_T)sf_integer_check
      (chartInstance->S, 1U, 725U, 9U, c2_i), 1, 192) - 1];
    if (CV_EML_IF(0, 1, 0, CV_RELATIONAL_EVAL(4U, 0U, 0, c2_d2, 0.0, -1, 1U,
          c2_d2 != 0.0))) {
      _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 25);
      if (CV_EML_IF(0, 1, 1, CV_RELATIONAL_EVAL(4U, 0U, 1, c2_mod(chartInstance,
             c2_i), 1.0, -1, 0U, c2_mod(chartInstance, c2_i) == 1.0))) {
        _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 27);
        c2_b_A = c2_i - 1.0;
        c2_b_x = c2_b_A;
        c2_d_x = c2_b_x;
        c2_b_y = c2_d_x / 2.0;
        c2_b_stimulation_elektrode[(sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 851, 36,
           MAX_uint32_T, (int32_T)sf_integer_check(chartInstance->S, 1U, 851U,
          36U, c2_b_y + 1.0), 1, 192) + 192 * (sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 851, 36,
           MAX_uint32_T, (int32_T)sf_integer_check(chartInstance->S, 1U, 851U,
          36U, c2_idx), 1, 6) - 1)) - 1] = c2_b_output[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 890, 9,
           MAX_uint32_T, (int32_T)sf_integer_check(chartInstance->S, 1U, 890U,
            9U, c2_i), 1, 192) - 1];
        _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 28);
        c2_idx++;
      } else {
        _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 31);
        c2_A = c2_i;
        c2_x = c2_A;
        c2_c_x = c2_x;
        c2_y = c2_c_x / 2.0;
        c2_c_A = c2_b_output[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1017, 9,
           MAX_uint32_T, (int32_T)sf_integer_check(chartInstance->S, 1U, 1017U,
            9U, c2_i), 1, 192) - 1];
        c2_e_x = c2_c_A;
        c2_f_x = c2_e_x;
        c2_c_y = c2_f_x / 2.0;
        c2_b_stimulation_elektrode[(sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1006, 3,
           MAX_uint32_T, (int32_T)sf_integer_check(chartInstance->S, 1U, 1006U,
          3U, c2_y), 1, 192) + 192 * (sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1010, 3,
           MAX_uint32_T, (int32_T)sf_integer_check(chartInstance->S, 1U, 1010U,
          3U, c2_idx), 1, 6) - 1)) - 1] = c2_c_y;
        _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 32);
        c2_d_A = c2_i;
        c2_g_x = c2_d_A;
        c2_h_x = c2_g_x;
        c2_d_y = c2_h_x / 2.0;
        c2_e_A = c2_b_output[sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1077, 9,
           MAX_uint32_T, (int32_T)sf_integer_check(chartInstance->S, 1U, 1077U,
            9U, c2_i), 1, 192) - 1];
        c2_i_x = c2_e_A;
        c2_j_x = c2_i_x;
        c2_e_y = c2_j_x / 2.0;
        c2_b_stimulation_elektrode[(sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1064, 5,
           MAX_uint32_T, (int32_T)sf_integer_check(chartInstance->S, 1U, 1064U,
          5U, c2_d_y + 1.0), 1, 192) + 192 * (sf_eml_array_bounds_check
          (sfGlobalDebugInstanceStruct, chartInstance->S, 1U, 1070, 3,
           MAX_uint32_T, (int32_T)sf_integer_check(chartInstance->S, 1U, 1070U,
          3U, c2_idx), 1, 6) - 1)) - 1] = c2_e_y;
        _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, 33);
        c2_idx++;
      }
    }

    c2_c_i++;
    _SF_MEX_LISTEN_FOR_CTRL_C(chartInstance->S);
  }

  CV_EML_FOR(0, 1, 1, 0);
  _SFD_EML_CALL(0U, chartInstance->c2_sfEvent, -33);
  _SFD_SYMBOL_SCOPE_POP();
  for (c2_i22 = 0; c2_i22 < 6; c2_i22++) {
    (*chartInstance->c2_electrode)[c2_i22] = c2_b_electrode[c2_i22];
  }

  for (c2_i23 = 0; c2_i23 < 6; c2_i23++) {
    (*chartInstance->c2_index)[c2_i23] = c2_b_index[c2_i23];
  }

  for (c2_i24 = 0; c2_i24 < 192; c2_i24++) {
    (*chartInstance->c2_output)[c2_i24] = c2_b_output[c2_i24];
  }

  for (c2_i25 = 0; c2_i25 < 192; c2_i25++) {
    (*chartInstance->c2_env_out)[c2_i25] = c2_b_env_out[c2_i25];
  }

  for (c2_i26 = 0; c2_i26 < 1152; c2_i26++) {
    (*chartInstance->c2_stimulation_elektrode)[c2_i26] =
      c2_b_stimulation_elektrode[c2_i26];
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, chartInstance->c2_sfEvent);
}

static void initSimStructsc2_pacev_try(SFc2_pacev_tryInstanceStruct
  *chartInstance)
{
  (void)chartInstance;
}

static void c2_fftft_invoke(SFc2_pacev_tryInstanceStruct *chartInstance, real_T
  c2_b_u[4096], real_T c2_y[4096])
{
  _ssFcnCallExecArgInfo c2_args[2];
  c2_args[0U].dataPtr = (void *)c2_b_u;
  c2_args[1U].dataPtr = (void *)c2_y;
  slcsInvokeSimulinkFunction(chartInstance->S, "fftft", &c2_args[0U]);
}

static void c2_fftEnv_invoke(SFc2_pacev_tryInstanceStruct *chartInstance, real_T
  c2_b_u[4096], real_T c2_y[192])
{
  _ssFcnCallExecArgInfo c2_args[2];
  c2_args[0U].dataPtr = (void *)c2_b_u;
  c2_args[1U].dataPtr = (void *)c2_y;
  slcsInvokeSimulinkFunction(chartInstance->S, "fftEnv", &c2_args[0U]);
}

static void c2_Sel_invoke(SFc2_pacev_tryInstanceStruct *chartInstance, real_T
  c2_b_u[192], real_T c2_y[6], real_T c2_y1[6])
{
  _ssFcnCallExecArgInfo c2_args[3];
  c2_args[0U].dataPtr = (void *)c2_b_u;
  c2_args[1U].dataPtr = (void *)c2_y;
  c2_args[2U].dataPtr = (void *)c2_y1;
  slcsInvokeSimulinkFunction(chartInstance->S, "Sel", &c2_args[0U]);
}

static void init_script_number_translation(uint32_T c2_machineNumber, uint32_T
  c2_chartNumber, uint32_T c2_instanceNumber)
{
  (void)(c2_machineNumber);
  (void)(c2_chartNumber);
  (void)(c2_instanceNumber);
}

static const mxArray *c2_sf_marshallOut(void *chartInstanceVoid, void *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  int32_T c2_i27;
  int32_T c2_i28;
  const mxArray *c2_y = NULL;
  int32_T c2_i29;
  real_T c2_b_u[1152];
  SFc2_pacev_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_pacev_tryInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  c2_i27 = 0;
  for (c2_i28 = 0; c2_i28 < 6; c2_i28++) {
    for (c2_i29 = 0; c2_i29 < 192; c2_i29++) {
      c2_b_u[c2_i29 + c2_i27] = (*(real_T (*)[1152])c2_inData)[c2_i29 + c2_i27];
    }

    c2_i27 += 192;
  }

  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", c2_b_u, 0, 0U, 1U, 0U, 2, 192, 6),
                false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static void c2_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_stimulation_elektrode, const char_T *c2_identifier, real_T
  c2_y[1152])
{
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_stimulation_elektrode),
                        &c2_thisId, c2_y);
  sf_mex_destroy(&c2_b_stimulation_elektrode);
}

static void c2_b_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId, real_T c2_y[1152])
{
  real_T c2_dv7[1152];
  int32_T c2_i30;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_b_u), c2_dv7, 1, 0, 0U, 1, 0U, 2, 192,
                6);
  for (c2_i30 = 0; c2_i30 < 1152; c2_i30++) {
    c2_y[c2_i30] = c2_dv7[c2_i30];
  }

  sf_mex_destroy(&c2_b_u);
}

static void c2_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_b_stimulation_elektrode;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  real_T c2_y[1152];
  int32_T c2_i31;
  int32_T c2_i32;
  int32_T c2_i33;
  SFc2_pacev_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_pacev_tryInstanceStruct *)chartInstanceVoid;
  c2_b_stimulation_elektrode = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_stimulation_elektrode),
                        &c2_thisId, c2_y);
  sf_mex_destroy(&c2_b_stimulation_elektrode);
  c2_i31 = 0;
  for (c2_i32 = 0; c2_i32 < 6; c2_i32++) {
    for (c2_i33 = 0; c2_i33 < 192; c2_i33++) {
      (*(real_T (*)[1152])c2_outData)[c2_i33 + c2_i31] = c2_y[c2_i33 + c2_i31];
    }

    c2_i31 += 192;
  }

  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_b_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  int32_T c2_i34;
  const mxArray *c2_y = NULL;
  real_T c2_b_u[192];
  SFc2_pacev_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_pacev_tryInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  for (c2_i34 = 0; c2_i34 < 192; c2_i34++) {
    c2_b_u[c2_i34] = (*(real_T (*)[192])c2_inData)[c2_i34];
  }

  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", c2_b_u, 0, 0U, 1U, 0U, 1, 192), false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static void c2_c_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_env_out, const char_T *c2_identifier, real_T c2_y[192])
{
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_env_out), &c2_thisId,
                        c2_y);
  sf_mex_destroy(&c2_b_env_out);
}

static void c2_d_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId, real_T c2_y[192])
{
  real_T c2_dv8[192];
  int32_T c2_i35;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_b_u), c2_dv8, 1, 0, 0U, 1, 0U, 1, 192);
  for (c2_i35 = 0; c2_i35 < 192; c2_i35++) {
    c2_y[c2_i35] = c2_dv8[c2_i35];
  }

  sf_mex_destroy(&c2_b_u);
}

static void c2_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_b_env_out;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  real_T c2_y[192];
  int32_T c2_i36;
  SFc2_pacev_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_pacev_tryInstanceStruct *)chartInstanceVoid;
  c2_b_env_out = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_env_out), &c2_thisId,
                        c2_y);
  sf_mex_destroy(&c2_b_env_out);
  for (c2_i36 = 0; c2_i36 < 192; c2_i36++) {
    (*(real_T (*)[192])c2_outData)[c2_i36] = c2_y[c2_i36];
  }

  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_c_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  int32_T c2_i37;
  const mxArray *c2_y = NULL;
  real_T c2_b_u[6];
  SFc2_pacev_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_pacev_tryInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  for (c2_i37 = 0; c2_i37 < 6; c2_i37++) {
    c2_b_u[c2_i37] = (*(real_T (*)[6])c2_inData)[c2_i37];
  }

  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", c2_b_u, 0, 0U, 1U, 0U, 1, 6), false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static void c2_e_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_index, const char_T *c2_identifier, real_T c2_y[6])
{
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_index), &c2_thisId, c2_y);
  sf_mex_destroy(&c2_b_index);
}

static void c2_f_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId, real_T c2_y[6])
{
  real_T c2_dv9[6];
  int32_T c2_i38;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_b_u), c2_dv9, 1, 0, 0U, 1, 0U, 1, 6);
  for (c2_i38 = 0; c2_i38 < 6; c2_i38++) {
    c2_y[c2_i38] = c2_dv9[c2_i38];
  }

  sf_mex_destroy(&c2_b_u);
}

static void c2_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_b_index;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  real_T c2_y[6];
  int32_T c2_i39;
  SFc2_pacev_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_pacev_tryInstanceStruct *)chartInstanceVoid;
  c2_b_index = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_index), &c2_thisId, c2_y);
  sf_mex_destroy(&c2_b_index);
  for (c2_i39 = 0; c2_i39 < 6; c2_i39++) {
    (*(real_T (*)[6])c2_outData)[c2_i39] = c2_y[c2_i39];
  }

  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_d_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  int32_T c2_i40;
  const mxArray *c2_y = NULL;
  real_T c2_b_u[4096];
  SFc2_pacev_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_pacev_tryInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  for (c2_i40 = 0; c2_i40 < 4096; c2_i40++) {
    c2_b_u[c2_i40] = (*(real_T (*)[4096])c2_inData)[c2_i40];
  }

  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", c2_b_u, 0, 0U, 1U, 0U, 2, 4096, 1),
                false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static const mxArray *c2_e_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  real_T c2_b_u;
  const mxArray *c2_y = NULL;
  SFc2_pacev_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_pacev_tryInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  c2_b_u = *(real_T *)c2_inData;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_b_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static real_T c2_g_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId)
{
  real_T c2_y;
  real_T c2_d3;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_b_u), &c2_d3, 1, 0, 0U, 0, 0U, 0);
  c2_y = c2_d3;
  sf_mex_destroy(&c2_b_u);
  return c2_y;
}

static void c2_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_nargout;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  real_T c2_y;
  SFc2_pacev_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_pacev_tryInstanceStruct *)chartInstanceVoid;
  c2_nargout = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_g_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_nargout), &c2_thisId);
  sf_mex_destroy(&c2_nargout);
  *(real_T *)c2_outData = c2_y;
  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_f_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  int32_T c2_i41;
  const mxArray *c2_y = NULL;
  real_T c2_b_u[4096];
  SFc2_pacev_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_pacev_tryInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  for (c2_i41 = 0; c2_i41 < 4096; c2_i41++) {
    c2_b_u[c2_i41] = (*(real_T (*)[4096])c2_inData)[c2_i41];
  }

  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", c2_b_u, 0, 0U, 1U, 0U, 1, 4096), false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static void c2_h_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId, real_T c2_y[4096])
{
  real_T c2_dv10[4096];
  int32_T c2_i42;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_b_u), c2_dv10, 1, 0, 0U, 1, 0U, 1,
                4096);
  for (c2_i42 = 0; c2_i42 < 4096; c2_i42++) {
    c2_y[c2_i42] = c2_dv10[c2_i42];
  }

  sf_mex_destroy(&c2_b_u);
}

static void c2_e_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_r_sqr;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  real_T c2_y[4096];
  int32_T c2_i43;
  SFc2_pacev_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_pacev_tryInstanceStruct *)chartInstanceVoid;
  c2_r_sqr = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_r_sqr), &c2_thisId, c2_y);
  sf_mex_destroy(&c2_r_sqr);
  for (c2_i43 = 0; c2_i43 < 4096; c2_i43++) {
    (*(real_T (*)[4096])c2_outData)[c2_i43] = c2_y[c2_i43];
  }

  sf_mex_destroy(&c2_mxArrayInData);
}

const mxArray *sf_c2_pacev_try_get_eml_resolved_functions_info(void)
{
  const mxArray *c2_nameCaptureInfo = NULL;
  c2_nameCaptureInfo = NULL;
  sf_mex_assign(&c2_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c2_nameCaptureInfo;
}

static real_T c2_mod(SFc2_pacev_tryInstanceStruct *chartInstance, real_T c2_x)
{
  real_T c2_r;
  real_T c2_b_x;
  real_T c2_c_x;
  real_T c2_d_x;
  real_T c2_e_x;
  real_T c2_f_x;
  boolean_T c2_b;
  boolean_T c2_b0;
  real_T c2_g_x;
  boolean_T c2_b_b;
  boolean_T c2_b1;
  boolean_T c2_c_b;
  boolean_T c2_rEQ0;
  (void)chartInstance;
  c2_b_x = c2_x;
  c2_c_x = c2_b_x;
  c2_d_x = c2_c_x;
  c2_e_x = c2_d_x;
  c2_f_x = c2_e_x;
  c2_b = muDoubleScalarIsInf(c2_f_x);
  c2_b0 = !c2_b;
  c2_g_x = c2_e_x;
  c2_b_b = muDoubleScalarIsNaN(c2_g_x);
  c2_b1 = !c2_b_b;
  c2_c_b = (c2_b0 && c2_b1);
  if (c2_c_b) {
    if (c2_d_x == 0.0) {
      c2_r = 0.0;
    } else {
      c2_r = muDoubleScalarRem(c2_d_x, 2.0);
      c2_rEQ0 = (c2_r == 0.0);
      if (c2_rEQ0) {
        c2_r = 0.0;
      } else {
        if (c2_d_x < 0.0) {
          c2_r += 2.0;
        }
      }
    }
  } else {
    c2_r = rtNaN;
  }

  return c2_r;
}

static const mxArray *c2_g_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  int32_T c2_b_u;
  const mxArray *c2_y = NULL;
  SFc2_pacev_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_pacev_tryInstanceStruct *)chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  c2_b_u = *(int32_T *)c2_inData;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_b_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static int32_T c2_i_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId)
{
  int32_T c2_y;
  int32_T c2_i44;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_b_u), &c2_i44, 1, 6, 0U, 0, 0U, 0);
  c2_y = c2_i44;
  sf_mex_destroy(&c2_b_u);
  return c2_y;
}

static void c2_f_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_b_sfEvent;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  int32_T c2_y;
  SFc2_pacev_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_pacev_tryInstanceStruct *)chartInstanceVoid;
  c2_b_sfEvent = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_i_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_sfEvent),
    &c2_thisId);
  sf_mex_destroy(&c2_b_sfEvent);
  *(int32_T *)c2_outData = c2_y;
  sf_mex_destroy(&c2_mxArrayInData);
}

static uint8_T c2_j_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_is_active_c2_pacev_try, const char_T *c2_identifier)
{
  uint8_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_k_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_b_is_active_c2_pacev_try), &c2_thisId);
  sf_mex_destroy(&c2_b_is_active_c2_pacev_try);
  return c2_y;
}

static uint8_T c2_k_emlrt_marshallIn(SFc2_pacev_tryInstanceStruct *chartInstance,
  const mxArray *c2_b_u, const emlrtMsgIdentifier *c2_parentId)
{
  uint8_T c2_y;
  uint8_T c2_u0;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_b_u), &c2_u0, 1, 3, 0U, 0, 0U, 0);
  c2_y = c2_u0;
  sf_mex_destroy(&c2_b_u);
  return c2_y;
}

static void init_dsm_address_info(SFc2_pacev_tryInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address(SFc2_pacev_tryInstanceStruct *chartInstance)
{
  chartInstance->c2_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c2_u = (real_T (*)[4096])ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c2_electrode = (real_T (*)[6])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c2_index = (real_T (*)[6])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c2_output = (real_T (*)[192])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c2_env_out = (real_T (*)[192])ssGetOutputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c2_stimulation_elektrode = (real_T (*)[1152])
    ssGetOutputPortSignal_wrapper(chartInstance->S, 5);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c2_pacev_try_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(1093689388U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(725292063U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3266650161U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(415984227U);
}

mxArray* sf_c2_pacev_try_get_post_codegen_info(void);
mxArray *sf_c2_pacev_try_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("WSoO92BYwnjSTowEzYoYpE");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(4096);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,5,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,1,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(6);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,1,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(6);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,1,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(192);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,1,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(192);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(192);
      pr[1] = (double)(6);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo = sf_c2_pacev_try_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c2_pacev_try_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c2_pacev_try_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("late");
  mxArray *fallbackReason = mxCreateString("client_server");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("Sel");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c2_pacev_try_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c2_pacev_try_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString(
      "vfVN7uVwzUwpT7b7dTFvqE");
    mwSize exp_dims[2] = { 3, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);

    {
      mxArray* mxFcnName = mxCreateString("Sel");
      mxSetCell(mxExportedFunctionsUsedByThisChart, 0, mxFcnName);
    }

    {
      mxArray* mxFcnName = mxCreateString("fftEnv");
      mxSetCell(mxExportedFunctionsUsedByThisChart, 1, mxFcnName);
    }

    {
      mxArray* mxFcnName = mxCreateString("fftft");
      mxSetCell(mxExportedFunctionsUsedByThisChart, 2, mxFcnName);
    }

    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c2_pacev_try(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x6'type','srcId','name','auxInfo'{{M[1],M[5],T\"electrode\",},{M[1],M[10],T\"env_out\",},{M[1],M[6],T\"index\",},{M[1],M[7],T\"output\",},{M[1],M[11],T\"stimulation_elektrode\",},{M[8],M[0],T\"is_active_c2_pacev_try\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 6, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c2_pacev_try_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc2_pacev_tryInstanceStruct *chartInstance = (SFc2_pacev_tryInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _pacev_tryMachineNumber_,
           2,
           1,
           1,
           0,
           8,
           0,
           0,
           0,
           0,
           0,
           &chartInstance->chartNumber,
           &chartInstance->instanceNumber,
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation(_pacev_tryMachineNumber_,
          chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,_pacev_tryMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _pacev_tryMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,1,1,0,"u");
          _SFD_SET_DATA_PROPS(1,2,0,1,"electrode");
          _SFD_SET_DATA_PROPS(2,2,0,1,"index");
          _SFD_SET_DATA_PROPS(3,2,0,1,"output");
          _SFD_SET_DATA_PROPS(4,2,0,1,"env_out");
          _SFD_SET_DATA_PROPS(5,2,0,1,"stimulation_elektrode");
          _SFD_SET_DATA_PROPS(6,10,0,0,"M");
          _SFD_SET_DATA_PROPS(7,10,0,0,"N");
          _SFD_STATE_INFO(0,0,2);
          _SFD_CH_SUBSTATE_COUNT(0);
          _SFD_CH_SUBSTATE_DECOMP(0);
        }

        _SFD_CV_INIT_CHART(0,0,0,0);

        {
          _SFD_CV_INIT_STATE(0,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(0,1,1,0,2,0,0,0,2,0,0,0);
        _SFD_CV_INIT_EML_FCN(0,0,"eML_blk_kernel",0,-1,1140);
        _SFD_CV_INIT_EML_IF(0,1,0,721,740,-1,1136);
        _SFD_CV_INIT_EML_IF(0,1,1,787,805,936,1128);
        _SFD_CV_INIT_EML_FOR(0,1,0,282,294,334);
        _SFD_CV_INIT_EML_FOR(0,1,1,705,717,1140);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,0,725,739,-1,1);
        _SFD_CV_INIT_EML_RELATIONAL(0,1,1,791,804,-1,0);

        {
          unsigned int dimVector[2];
          dimVector[0]= 4096U;
          dimVector[1]= 1U;
          _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c2_d_sf_marshallOut,(MexInFcnForType)NULL);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 6U;
          _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c2_c_sf_marshallOut,(MexInFcnForType)
            c2_c_sf_marshallIn);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 6U;
          _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c2_c_sf_marshallOut,(MexInFcnForType)
            c2_c_sf_marshallIn);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 192U;
          _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c2_b_sf_marshallOut,(MexInFcnForType)
            c2_b_sf_marshallIn);
        }

        {
          unsigned int dimVector[1];
          dimVector[0]= 192U;
          _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,1,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c2_b_sf_marshallOut,(MexInFcnForType)
            c2_b_sf_marshallIn);
        }

        {
          unsigned int dimVector[2];
          dimVector[0]= 192U;
          dimVector[1]= 6U;
          _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,2,&(dimVector[0]),0,0,0,0.0,
            1.0,0,0,(MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)
            c2_sf_marshallIn);
        }

        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_e_sf_marshallOut,(MexInFcnForType)c2_d_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_e_sf_marshallOut,(MexInFcnForType)c2_d_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _pacev_tryMachineNumber_,chartInstance->chartNumber,
        chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc2_pacev_tryInstanceStruct *chartInstance = (SFc2_pacev_tryInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(0U, (void *)chartInstance->c2_u);
        _SFD_SET_DATA_VALUE_PTR(1U, (void *)chartInstance->c2_electrode);
        _SFD_SET_DATA_VALUE_PTR(2U, (void *)chartInstance->c2_index);
        _SFD_SET_DATA_VALUE_PTR(3U, (void *)chartInstance->c2_output);
        _SFD_SET_DATA_VALUE_PTR(6U, (void *)&chartInstance->c2_M);
        _SFD_SET_DATA_VALUE_PTR(7U, (void *)&chartInstance->c2_N);
        _SFD_SET_DATA_VALUE_PTR(4U, (void *)chartInstance->c2_env_out);
        _SFD_SET_DATA_VALUE_PTR(5U, (void *)
          chartInstance->c2_stimulation_elektrode);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "sFhZDvswkiVKcJJJqctUK2B";
}

static void sf_opaque_initialize_c2_pacev_try(void *chartInstanceVar)
{
  chart_debug_initialization(((SFc2_pacev_tryInstanceStruct*) chartInstanceVar
    )->S,0);
  initialize_params_c2_pacev_try((SFc2_pacev_tryInstanceStruct*)
    chartInstanceVar);
  initialize_c2_pacev_try((SFc2_pacev_tryInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c2_pacev_try(void *chartInstanceVar)
{
  enable_c2_pacev_try((SFc2_pacev_tryInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c2_pacev_try(void *chartInstanceVar)
{
  disable_c2_pacev_try((SFc2_pacev_tryInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c2_pacev_try(void *chartInstanceVar)
{
  sf_gateway_c2_pacev_try((SFc2_pacev_tryInstanceStruct*) chartInstanceVar);
}

static const mxArray* sf_opaque_get_sim_state_c2_pacev_try(SimStruct* S)
{
  return get_sim_state_c2_pacev_try((SFc2_pacev_tryInstanceStruct *)
    sf_get_chart_instance_ptr(S));     /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c2_pacev_try(SimStruct* S, const mxArray *st)
{
  set_sim_state_c2_pacev_try((SFc2_pacev_tryInstanceStruct*)
    sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_terminate_c2_pacev_try(void *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc2_pacev_tryInstanceStruct*) chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_pacev_try_optimization_info();
    }

    finalize_c2_pacev_try((SFc2_pacev_tryInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc2_pacev_try((SFc2_pacev_tryInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c2_pacev_try(SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c2_pacev_try((SFc2_pacev_tryInstanceStruct*)
      sf_get_chart_instance_ptr(S));
  }
}

static void mdlSetWorkWidths_c2_pacev_try(SimStruct *S)
{
  /* Actual parameters from chart:
     M N
   */
  const char_T *rtParamNames[] = { "M", "N" };

  ssSetNumRunTimeParams(S,ssGetSFcnParamsCount(S));

  /* registration for M*/
  ssRegDlgParamAsRunTimeParam(S, 0, 0, rtParamNames[0], SS_DOUBLE);

  /* registration for N*/
  ssRegDlgParamAsRunTimeParam(S, 1, 1, rtParamNames[1], SS_DOUBLE);

  /* Set overwritable ports for inplace optimization */
  ssSetStatesModifiedOnlyInUpdate(S, 0);
  ssMdlUpdateIsEmpty(S, 1);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct = load_pacev_try_optimization_info(sim_mode_is_rtw_gen(S),
      sim_mode_is_modelref_sim(S), sim_mode_is_external(S));
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,2);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,2,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_set_chart_accesses_machine_info(S, sf_get_instance_specialization(),
      infoStruct, 2);
    sf_update_buildInfo(S, sf_get_instance_specialization(),infoStruct,2);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,2,1);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,2,5);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=5; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 1; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,2);
    sf_register_codegen_names_for_scoped_functions_defined_by_chart(S);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(3070460810U));
  ssSetChecksum1(S,(730053149U));
  ssSetChecksum2(S,(2253687242U));
  ssSetChecksum3(S,(88671743U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSetStateSemanticsClassicAndSynchronous(S, true);
  ssSupportsMultipleExecInstances(S,0);
}

static void mdlRTW_c2_pacev_try(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c2_pacev_try(SimStruct *S)
{
  SFc2_pacev_tryInstanceStruct *chartInstance;
  chartInstance = (SFc2_pacev_tryInstanceStruct *)utMalloc(sizeof
    (SFc2_pacev_tryInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof(SFc2_pacev_tryInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway = sf_opaque_gateway_c2_pacev_try;
  chartInstance->chartInfo.initializeChart = sf_opaque_initialize_c2_pacev_try;
  chartInstance->chartInfo.terminateChart = sf_opaque_terminate_c2_pacev_try;
  chartInstance->chartInfo.enableChart = sf_opaque_enable_c2_pacev_try;
  chartInstance->chartInfo.disableChart = sf_opaque_disable_c2_pacev_try;
  chartInstance->chartInfo.getSimState = sf_opaque_get_sim_state_c2_pacev_try;
  chartInstance->chartInfo.setSimState = sf_opaque_set_sim_state_c2_pacev_try;
  chartInstance->chartInfo.getSimStateInfo = sf_get_sim_state_info_c2_pacev_try;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c2_pacev_try;
  chartInstance->chartInfo.mdlStart = mdlStart_c2_pacev_try;
  chartInstance->chartInfo.mdlSetWorkWidths = mdlSetWorkWidths_c2_pacev_try;
  chartInstance->chartInfo.callGetHoverDataForMsg = NULL;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
  mdl_start_c2_pacev_try(chartInstance);
}

void c2_pacev_try_method_dispatcher(SimStruct *S, int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c2_pacev_try(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c2_pacev_try(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c2_pacev_try(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c2_pacev_try_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
