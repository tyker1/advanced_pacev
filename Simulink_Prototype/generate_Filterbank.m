function [ FilterVect, fftLen, waveletLevel ] = generate_Filterbank( fs,numElektrode, numVirtChannel )
%GENERATE_FILTERBANK Summary of this function goes here
%   Detailed explanation goes here


midF_base = [150,250,350,450,570,700,840,1000,1170,1370,1600,1850,2150,2500,2900,3400,4000,4800,5800,7000,8500,10500,13500];
CW = @(f) 25+75*(1+1.4*(f/1000).^2).^0.69;

CritBand = 2:24;
newCritBand = 2:1/numVirtChannel:numElektrode;

Interp_Bark = griddedInterpolant(CritBand,midF_base,'pchip','nearest');
MidF = Interp_Bark(newCritBand);

BW_MidF = CW(MidF);
%% Nicht unterteilen
scale = MidF(2) - MidF(1);

waveletLevel = nextpow2(fs/scale);
fftLen = 2^waveletLevel;
FFTFreq = 0:fftLen-1;
%% FFTBins berechnen
FilterVect = zeros(length(MidF),3);
BinWidth = fs/fftLen;
FFTFreq = FFTFreq .* BinWidth;
for i = 1:length(MidF)
    Freq = FFTFreq(floor(MidF(i)/BinWidth)+1);
    BW_Analog = CW(Freq);
    BW_Bin = round((BW_Analog - BinWidth)/(2*BinWidth));
    FilterVect(i,:) = [Freq; BW_Bin*2; 1];
end

%%unterteilen



end

