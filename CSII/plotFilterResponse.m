function [ filterkoeff ] = plotFilterResponse( midFreq, ERB )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

Wg = @(g,p) (1+p*g).*exp(-1*p*g);

p = midFreq/ERB*4;

f = 1:20000;
f = f - midFreq;
f = f / midFreq;

filterkoeff = Wg(f,1);

plot(filterkoeff);
end

