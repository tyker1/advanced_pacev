%% Filter Implementierung

ERB = @(f) 6.23*f.^2+93.39*f+28.52;

FilterKoeff = @(g,p) (1+p.*g).*exp(-1*p.*g);

ERB2F = @(erb)  676170.4./(47.06538-exp(0.08950404.*erb))-14678.49;

testerb= 3:30;

freq = ERB2F(testerb);
