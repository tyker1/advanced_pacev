function [ sampleErg ] = sampleEnvelope(envelopeErg, numChannel, frameSize, OverlapLen, SampleSize, StimulationRate, j, fs)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

sampleErg = zeros(1, numChannel);

sampleWindowSize = fs / StimulationRate;

if (mod(sampleWindowSize,2) == 0)
    midIdx = sampleWindowSize/2;
else
    midIdx = (sampleWindowSize-1)/2+1;
end

if (mod(SampleSize,2) == 0)
    startIdx = midIdx-(SampleSize/2)+1;
    endIdx = midIdx + SampleSize/2;
else
    startIdx = midIdx-(SampleSize-1)/2;
    endIdx = midIdx + ( SampleSize-1)/2;
end

startIdx = startIdx + OverlapLen + (j-1)*sampleWindowSize;
endIdx = endIdx + OverlapLen + (j-1)*sampleWindowSize;

for i = 1:numChannel
    sampleErg(i) = mean(envelopeErg(startIdx:endIdx,i));
end

end

