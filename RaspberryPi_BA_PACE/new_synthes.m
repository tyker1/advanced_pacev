L = length(elektrode);

sig = zeros(1,L*SamplePerFrame);
fs = Samplefrequence;
% tl = zeros(1,M);
% tr = zeros(1,M);
tl = zeros(1,M);

for i = 1:L
    sig_sin =zeros(1,SamplePerFrame);
    for j = 1:M
        amp1 = elektrode2(i,j);
        if (i == L)
            amp2 = 0;
        else
            amp2 = elektrode2(i+1,j);
        end
        if (amp1 ~= 0) || (amp2 ~= 0)
            tr = tl(j) + SamplePerFrame/fs;
            t = linspace(tl(j),tr,SamplePerFrame);
            tl(j) = tr + 1/fs;
            temp_amp = amp_interp(amp1,amp2,SamplePerFrame);
            sig_sin = sig_sin + temp_amp.* sin(2*pi*MidFreq(j).*t);
        end
    end
    sig((i-1)*SamplePerFrame+1:i*SamplePerFrame) = sig_sin;
end

[normal_SNR,segSNR] = SNRberechnen(origin',sig,fs);

fprintf("SNR = %.4f\n seg SNR = %.4f\n",normal_SNR,segSNR);
function vect_amp = amp_interp(a1,a2,amp_N)

n = 0:amp_N-1;

delta = (a2-a1) / amp_N;

vect_amp = a1 + n.*delta;

end


function [snrValue, ssnrValue]= SNRberechnen (in, out, fs)

% l1 = length(in);
% l2 = length(out);
% l = min (l1,l2);
% in = in(1:l);
% out = out(1:l);

%snrValue =  snr (in, out - in) ;
snrValue = 20*log10(norm(in(:))/norm(in(:) - out(:)));

ssnrValue = segsnr( in, out , fs);
% ssnrValue = seg_snr( in, out);

end


%*************************************************************************
%  Funktion segsnr :  berechnet segment orientierter SNr
function  segSnr = segsnr (in, out, fs)

    s = [];
    smin = -10 ;
    smax = 35;
    N = fix(0.02*fs); % 20 ms
    lsignal = length(in);
    if ~eq(length(in),length(out))
        error('Eingans- und Ausgangssignal nicht gleich lang --> segSNR kann nicht berechnet werden')
    end
    M = fix (lsignal/N);
    for i = 1 : M
        first = N*(i-1)+1;
        last  = first + N-1;
        x = in(first:last);
        xe = out(first:last);
        noise = x-xe;
        sum_x = sum(x.^2);
        sum_noise = sum(noise.^2);
        if sum_noise > 0
            si = sum_x/sum_noise;
            if si >0.0
                logsnr = 10*log10(si);
                if (logsnr>-10) & (logsnr < 64)
                    s = [s logsnr];
                end
            end
        end
    end
    segSnr =  mean(s);
end
