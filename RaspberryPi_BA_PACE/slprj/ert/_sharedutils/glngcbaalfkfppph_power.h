/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: glngcbaalfkfppph_power.h
 *
 * Code generated for Simulink model 'pace_try'.
 *
 * Model version                  : 1.184
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:37:21 2017
 */

#ifndef SHARE_glngcbaalfkfppph_power
#define SHARE_glngcbaalfkfppph_power
#include "rtwtypes.h"

extern void glngcbaalfkfppph_power(const real_T a[128], real_T y[128]);

#endif

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
