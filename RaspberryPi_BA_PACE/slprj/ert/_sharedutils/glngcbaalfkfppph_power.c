/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: glngcbaalfkfppph_power.c
 *
 * Code generated for Simulink model 'pace_try'.
 *
 * Model version                  : 1.184
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:37:21 2017
 */

#include "rtwtypes.h"
#include "glngcbaalfkfppph_power.h"

/* Function for MATLAB Function: '<S2>/MATLAB Function' */
void glngcbaalfkfppph_power(const real_T a[128], real_T y[128])
{
  int32_T k;
  for (k = 0; k < 128; k++) {
    y[k] = a[k] * a[k];
  }
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
