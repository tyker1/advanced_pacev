/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: rt_powd_snf.h
 *
 * Code generated for Simulink model 'pace_try'.
 *
 * Model version                  : 1.184
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:37:21 2017
 */

#ifndef SHARE_rt_powd_snf
#define SHARE_rt_powd_snf
#include "rtwtypes.h"

extern real_T rt_powd_snf(real_T u0, real_T u1);

#endif

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
