/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: Sel.c
 *
 * Code generated for Simulink model 'pace_try'.
 *
 * Model version                  : 1.184
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:37:21 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Sel.h"

/* Include model header file for global data */
#include "pace_try.h"
#include "pace_try_private.h"
#include "rt_powd_snf.h"

/* Output and update for Simulink Function: '<Root>/Bandselection' */
void Sel(const real_T rtu_u[22], real_T rty_y[11], real_T rty_y1[11])
{
  real_T max_idx;
  int32_T j;
  real_T It;
  real_T Li;
  int32_T b_idx;
  int32_T b_ii;
  int32_T k;
  int32_T ii_size_idx_1;
  boolean_T exitg1;

  /* MATLAB Function: '<S1>/MATLAB Function' incorporates:
   *  SignalConversion: '<S1>/TmpSignal ConversionAtuOutport1'
   */
  /*  [sortedArray, IndexBeforeSort] = sort(a,'descend'); */
  /*   */
  /*  outAmp = sortedArray(1:N); */
  /*  outIndex = IndexBeforeSort(1:N); */
  /* MATLAB Function 'Bandselection/MATLAB Function': '<S5>:1' */
  /* '<S5>:1:8' outAmp = zeros(1,N); */
  /* '<S5>:1:9' outIndex = zeros(1,N); */
  /* '<S5>:1:11' Mask_a = zeros(1,N); */
  memset(&pace_try_B.outIndex[0], 0, 11U * sizeof(real_T));
  memset(&pace_try_B.Mask_a[0], 0, 11U * sizeof(real_T));

  /* '<S5>:1:12' a_db = 10*log10(floor(a*32767)); */
  for (k = 0; k < 22; k++) {
    pace_try_B.a_db[k] = log10(floor(rtu_u[k] * 32767.0)) * 10.0;
  }

  /* '<S5>:1:13' for i = 1:N */
  for (k = 0; k < 11; k++) {
    /* '<S5>:1:14' max = -65536; */
    pace_try_B.b_max = -65536.0;

    /* '<S5>:1:15' max_idx = 1; */
    max_idx = 1.0;

    /* '<S5>:1:16' for j = 1:M */
    for (j = 0; j < (int32_T)pace_try_P.MATLABFunction_M; j++) {
      /* '<S5>:1:17' mask_effect = power_law(Mask_a, j, i-1, Labs, sl, sr, av, alpha); */
      /* '<S5>:1:35' if (Len == 0) */
      if (k == 0) {
        /* '<S5>:1:36' Lt = 0; */
        It = 0.0;
      } else {
        /*  I = 10^(L/10); */
        /*  It(zi) = [Iabs(zi)^alpha + Sum(Ii(zj)^alpha)]^(1/alpha) */
        /* '<S5>:1:41' It = (10^(Labs(z)/10)).^alpha; */
        It = rt_powd_snf(rt_powd_snf(10.0, pace_try_P.MATLABFunction_Labs[j] /
          10.0), pace_try_P.MATLABFunction_alpha);

        /* '<S5>:1:42' for idx = 1:Len */
        for (b_idx = 0; b_idx < k; b_idx++) {
          /* '<S5>:1:43' It = It + (10.^(spread_func(a(idx),idx, z, sl, sr, av)/10)).^alpha; */
          /* '<S5>:1:51' if (i > z) */
          if (1.0 + (real_T)b_idx > 1.0 + (real_T)j) {
            /* '<S5>:1:52' Li = a - av - sl*(i-z); */
            Li = (pace_try_B.Mask_a[b_idx] - pace_try_P.MATLABFunction_av) -
              ((1.0 + (real_T)b_idx) - (1.0 + (real_T)j)) *
              pace_try_P.MATLABFunction_sl;
          } else {
            /* '<S5>:1:53' else */
            /* '<S5>:1:54' Li = a - av - sr*(z-i); */
            Li = (pace_try_B.Mask_a[b_idx] - pace_try_P.MATLABFunction_av) -
              ((1.0 + (real_T)j) - (1.0 + (real_T)b_idx)) *
              pace_try_P.MATLABFunction_sr;
          }

          /* '<S5>:1:56' if (Li < 0) */
          if (Li < 0.0) {
            /* '<S5>:1:57' Li = 0; */
            Li = 0.0;
          }

          /*  function [outAmp,outIndex] = m_sel(a, N, Labs, sl, sr, av, alpha, M) */
          /*  % [sortedArray, IndexBeforeSort] = sort(a,'descend'); */
          /*  %  */
          /*  % outAmp = sortedArray(1:N); */
          /*  % outIndex = IndexBeforeSort(1:N); */
          /*   */
          /*  outAmp = zeros(1,N); */
          /*  outIndex = zeros(1,N); */
          /*   */
          /*  Mask_a = zeros(1,N); */
          /*  for i = 1:N */
          /*     max = -1; */
          /*     max_idx = 0; */
          /*     for j = 1:M */
          /*         mask_effect = power_law(Mask_a, j, i-1, Labs, sl, sr, av, alpha); */
          /*         delta = a(j) - mask_effect; */
          /*          */
          /*         if ((delta > max) && (isempty(find(outIndex == j, 1)))) */
          /*             max = delta; */
          /*             max_idx = j; */
          /*         end */
          /*     end */
          /*      */
          /*     Mask_a(i) = 10*log10(a(max_idx)); % Einheit dB */
          /*     outAmp(i) = a(max_idx); */
          /*     outIndex(i) = max_idx; */
          /*  end */
          /*   */
          /*  end */
          /*   */
          /*  function Lt = power_law(a, z, Len, Labs, sl, sr, av, alpha) */
          /*           */
          /*          if (Len == 0) */
          /*              Lt = 0; */
          /*              return; */
          /*          end */
          /*          % I = 10^(L/10); */
          /*          % Lt(zi) = [Iabs(zi)^alpha + Sum(Ii(zj)^alpha)]^(1/alpha) */
          /*          Lt = (10^(Labs(z)/10)).^alpha; */
          /*          for idx = 1:Len */
          /*              Lt = Lt + (10.^(spread_func(a(idx),idx, z, sl, sr, av)/10)).^alpha; */
          /*          end */
          /*           */
          /*          Lt = Lt.^(1/alpha); */
          /*  end */
          /*       */
          /*  function Li = spread_func(a, i, z, sl, sr, av) */
          /*      if (i > z) */
          /*          Li = a - av - sl*(i-z); */
          /*      else */
          /*          Li = a - av - sr*(z-i); */
          /*      end */
          /*  end */
          It += rt_powd_snf(rt_powd_snf(10.0, Li / 10.0),
                            pace_try_P.MATLABFunction_alpha);
        }

        /* '<S5>:1:46' It = It.^(1/alpha); */
        It = rt_powd_snf(It, 1.0 / pace_try_P.MATLABFunction_alpha);

        /* '<S5>:1:47' Lt = 10*log10(It); */
        It = 10.0 * log10(It);
      }

      /* '<S5>:1:18' delta = a_db(j) - mask_effect; */
      It = pace_try_B.a_db[j] - It;

      /* '<S5>:1:20' if ((delta > max) && (isempty(find(outIndex == j, 1)))) */
      if (It > pace_try_B.b_max) {
        for (b_idx = 0; b_idx < 11; b_idx++) {
          pace_try_B.x[b_idx] = (1.0 + (real_T)j == pace_try_B.outIndex[b_idx]);
        }

        b_idx = 0;
        ii_size_idx_1 = 1;
        b_ii = 1;
        exitg1 = false;
        while ((!exitg1) && (b_ii < 12)) {
          if (pace_try_B.x[b_ii - 1]) {
            b_idx = 1;
            exitg1 = true;
          } else {
            b_ii++;
          }
        }

        if (b_idx == 0) {
          ii_size_idx_1 = 0;
        }

        if (ii_size_idx_1 == 0) {
          /* '<S5>:1:21' max = delta; */
          pace_try_B.b_max = It;

          /* '<S5>:1:22' max_idx = j; */
          max_idx = 1.0 + (real_T)j;
        }
      }
    }

    /* '<S5>:1:26' Mask_a(i) = a_db(max_idx); */
    pace_try_B.Mask_a[k] = pace_try_B.a_db[(int32_T)max_idx - 1];

    /*  Einheit dB */
    /* '<S5>:1:27' outAmp(i) = a(max_idx); */
    pace_try_B.outAmp[k] = rtu_u[(int32_T)max_idx - 1];

    /* '<S5>:1:28' outIndex(i) = max_idx; */
    pace_try_B.outIndex[k] = max_idx;

    /* SignalConversion: '<S1>/TmpSignal ConversionAtyInport1' incorporates:
     *  SignalConversion: '<S1>/TmpSignal ConversionAtuOutport1'
     */
    rty_y[k] = pace_try_B.outAmp[k];

    /* SignalConversion: '<S1>/TmpSignal ConversionAty1Inport1' */
    rty_y1[k] = pace_try_B.outIndex[k];
  }

  /* End of MATLAB Function: '<S1>/MATLAB Function' */
  pace_try_DW.Bandselection_SubsysRanBC = 4;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
