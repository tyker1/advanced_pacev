/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: fftft.c
 *
 * Code generated for Simulink model 'pace_try'.
 *
 * Model version                  : 1.184
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:37:21 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "fftft.h"

/* Include model header file for global data */
#include "pace_try.h"
#include "pace_try_private.h"
#include "glngcbaalfkfppph_power.h"

/* Forward declaration for local functions */
static void pace_try_jecbcbaicjecppph_fft(const real_T x[128], creal_T y[128]);

/* Function for MATLAB Function: '<S2>/MATLAB Function' */
static void pace_try_jecbcbaicjecppph_fft(const real_T x[128], creal_T y[128])
{
  int32_T ix;
  int32_T ju;
  int32_T iy;
  int32_T i;
  int32_T k;
  int32_T istart;
  int32_T j;
  int32_T ihi;
  boolean_T tst;
  static const real_T b[65] = { 1.0, 0.99879545620517241, 0.99518472667219693,
    0.989176509964781, 0.98078528040323043, 0.970031253194544,
    0.95694033573220882, 0.94154406518302081, 0.92387953251128674,
    0.90398929312344334, 0.881921264348355, 0.85772861000027212,
    0.83146961230254524, 0.80320753148064494, 0.773010453362737,
    0.74095112535495922, 0.70710678118654757, 0.67155895484701833,
    0.63439328416364549, 0.59569930449243336, 0.55557023301960218,
    0.51410274419322166, 0.47139673682599764, 0.42755509343028208,
    0.38268343236508978, 0.33688985339222005, 0.29028467725446233,
    0.24298017990326387, 0.19509032201612825, 0.14673047445536175,
    0.0980171403295606, 0.049067674327418015, 0.0, -0.049067674327418015,
    -0.0980171403295606, -0.14673047445536175, -0.19509032201612825,
    -0.24298017990326387, -0.29028467725446233, -0.33688985339222005,
    -0.38268343236508978, -0.42755509343028208, -0.47139673682599764,
    -0.51410274419322166, -0.55557023301960218, -0.59569930449243336,
    -0.63439328416364549, -0.67155895484701833, -0.70710678118654757,
    -0.74095112535495922, -0.773010453362737, -0.80320753148064494,
    -0.83146961230254524, -0.85772861000027212, -0.881921264348355,
    -0.90398929312344334, -0.92387953251128674, -0.94154406518302081,
    -0.95694033573220882, -0.970031253194544, -0.98078528040323043,
    -0.989176509964781, -0.99518472667219693, -0.99879545620517241, -1.0 };

  static const real_T c[65] = { 0.0, -0.049067674327418015, -0.0980171403295606,
    -0.14673047445536175, -0.19509032201612825, -0.24298017990326387,
    -0.29028467725446233, -0.33688985339222005, -0.38268343236508978,
    -0.42755509343028208, -0.47139673682599764, -0.51410274419322166,
    -0.55557023301960218, -0.59569930449243336, -0.63439328416364549,
    -0.67155895484701833, -0.70710678118654757, -0.74095112535495922,
    -0.773010453362737, -0.80320753148064494, -0.83146961230254524,
    -0.85772861000027212, -0.881921264348355, -0.90398929312344334,
    -0.92387953251128674, -0.94154406518302081, -0.95694033573220882,
    -0.970031253194544, -0.98078528040323043, -0.989176509964781,
    -0.99518472667219693, -0.99879545620517241, -1.0, -0.99879545620517241,
    -0.99518472667219693, -0.989176509964781, -0.98078528040323043,
    -0.970031253194544, -0.95694033573220882, -0.94154406518302081,
    -0.92387953251128674, -0.90398929312344334, -0.881921264348355,
    -0.85772861000027212, -0.83146961230254524, -0.80320753148064494,
    -0.773010453362737, -0.74095112535495922, -0.70710678118654757,
    -0.67155895484701833, -0.63439328416364549, -0.59569930449243336,
    -0.55557023301960218, -0.51410274419322166, -0.47139673682599764,
    -0.42755509343028208, -0.38268343236508978, -0.33688985339222005,
    -0.29028467725446233, -0.24298017990326387, -0.19509032201612825,
    -0.14673047445536175, -0.0980171403295606, -0.049067674327418015, -0.0 };

  real_T temp_im;
  real_T twid_re;
  real_T twid_im;
  ix = 0;
  ju = 0;
  iy = 0;
  for (k = 0; k < 127; k++) {
    y[iy].re = x[ix];
    y[iy].im = 0.0;
    iy = 128;
    tst = true;
    while (tst) {
      iy >>= 1;
      ju ^= iy;
      tst = ((ju & iy) == 0);
    }

    iy = ju;
    ix++;
  }

  y[iy].re = x[ix];
  y[iy].im = 0.0;
  for (ix = 0; ix <= 127; ix += 2) {
    pace_try_B.temp_re = y[ix + 1].re;
    temp_im = y[ix + 1].im;
    y[ix + 1].re = y[ix].re - y[ix + 1].re;
    y[ix + 1].im = y[ix].im - y[ix + 1].im;
    y[ix].re += pace_try_B.temp_re;
    y[ix].im += temp_im;
  }

  ix = 2;
  ju = 4;
  k = 32;
  iy = 125;
  while (k > 0) {
    for (i = 0; i < iy; i += ju) {
      pace_try_B.temp_re = y[i + ix].re;
      temp_im = y[i + ix].im;
      y[i + ix].re = y[i].re - pace_try_B.temp_re;
      y[i + ix].im = y[i].im - temp_im;
      y[i].re += pace_try_B.temp_re;
      y[i].im += temp_im;
    }

    istart = 1;
    for (j = k; j < 64; j += k) {
      twid_re = b[j];
      twid_im = c[j];
      i = istart;
      ihi = istart + iy;
      while (i < ihi) {
        pace_try_B.temp_re = y[i + ix].re * twid_re - y[i + ix].im * twid_im;
        temp_im = y[i + ix].im * twid_re + y[i + ix].re * twid_im;
        y[i + ix].re = y[i].re - pace_try_B.temp_re;
        y[i + ix].im = y[i].im - temp_im;
        y[i].re += pace_try_B.temp_re;
        y[i].im += temp_im;
        i += ju;
      }

      istart++;
    }

    k /= 2;
    ix = ju;
    ju += ju;
    iy -= ix;
  }
}

/* Output and update for Simulink Function: '<Root>/FFT Filterbank' */
void fftft(const real_T rtu_u[128], real_T rty_y[128])
{
  int32_T i;

  /* MATLAB Function: '<S2>/MATLAB Function' incorporates:
   *  SignalConversion: '<S2>/TmpSignal ConversionAtuOutport1'
   */
  /*  Berechnet FFT von signal, unter Verwendung von Fensterfunktion */
  /*  (WindowFunc), WindowSize entspricht der Fenstergröße */
  /*  Diese Erzeugt eine FIR Filterbank mit Anzahl der Filters identisch zu */
  /*  WindowSize */
  /* MATLAB Function 'FFT Filterbank/MATLAB Function': '<S6>:1' */
  /* '<S6>:1:7' windowedSig = signal .* WindowFunc; */
  /* Anwendung der Fensterfunktion */
  /* '<S6>:1:8' spectrum = fft(windowedSig)./WindowSize*2; */
  for (i = 0; i < 128; i++) {
    pace_try_B.rtu_u[i] = rtu_u[i] * pace_try_P.MATLABFunction_WindowFunc[i];
  }

  pace_try_jecbcbaicjecppph_fft(pace_try_B.rtu_u, pace_try_B.dcv0);

  /*  FFT Berechnen und Amplituden korigieren */
  /* '<S6>:1:9' r_sqr = imag(spectrum).^2 + real(spectrum).^2; */
  /* Power Sum bilden */
  /* '<S6>:1:11' fftres = r_sqr; */
  for (i = 0; i < 128; i++) {
    if (pace_try_B.dcv0[i].im == 0.0) {
      pace_try_B.re = pace_try_B.dcv0[i].re /
        pace_try_P.MATLABFunction_WindowSize;
      pace_try_B.im = 0.0;
    } else if (pace_try_B.dcv0[i].re == 0.0) {
      pace_try_B.re = 0.0;
      pace_try_B.im = pace_try_B.dcv0[i].im /
        pace_try_P.MATLABFunction_WindowSize;
    } else {
      pace_try_B.re = pace_try_B.dcv0[i].re /
        pace_try_P.MATLABFunction_WindowSize;
      pace_try_B.im = pace_try_B.dcv0[i].im /
        pace_try_P.MATLABFunction_WindowSize;
    }

    pace_try_B.spectrum_m.re = 2.0 * pace_try_B.re;
    pace_try_B.spectrum_m.im = 2.0 * pace_try_B.im;
    pace_try_B.rtu_u[i] = pace_try_B.spectrum_m.im;
    pace_try_B.spectrum[i] = pace_try_B.spectrum_m;
  }

  glngcbaalfkfppph_power(pace_try_B.rtu_u, pace_try_B.dv0);
  for (i = 0; i < 128; i++) {
    pace_try_B.rtu_u[i] = pace_try_B.spectrum[i].re;
  }

  glngcbaalfkfppph_power(pace_try_B.rtu_u, pace_try_B.dv1);

  /* SignalConversion: '<S2>/TmpSignal ConversionAtyInport1' incorporates:
   *  MATLAB Function: '<S2>/MATLAB Function'
   */
  for (i = 0; i < 128; i++) {
    rty_y[i] = pace_try_B.dv0[i] + pace_try_B.dv1[i];
  }

  /* End of SignalConversion: '<S2>/TmpSignal ConversionAtyInport1' */
  pace_try_DW.FFTFilterbank_SubsysRanBC = 4;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
