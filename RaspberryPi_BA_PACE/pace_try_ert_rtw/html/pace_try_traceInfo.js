function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <Root>/ALSA Audio Capture */
	this.urlHashMap["pace_try:42"] = "pace_try.c:321,531,548&pace_try.h:111&pace_try_private.h:37";
	/* <Root>/Ausgewaehlte Elektroden */
	this.urlHashMap["pace_try:33"] = "pace_try.h:143";
	/* <Root>/Ausgewaehlte Elektroden1 */
	this.urlHashMap["pace_try:40"] = "pace_try.h:147";
	/* <Root>/Bandselection */
	this.urlHashMap["pace_try:16"] = "pace_try.h:168&Sel.c:27";
	/* <Root>/Buffer */
	this.urlHashMap["pace_try:30"] = "pace_try.c:331,377,534,541&pace_try.h:95,140,165,204&pace_try_data.c:296";
	/* <Root>/Data Type Conversion */
	this.urlHashMap["pace_try:43"] = "msg=rtwMsg_notTraceable&block=pace_try:43";
	/* <Root>/Elektrode Map */
	this.urlHashMap["pace_try:23"] = "pace_try.h:151";
	/* <Root>/FFT Filterbank */
	this.urlHashMap["pace_try:8"] = "pace_try.h:167&fftft.c:158";
	/* <Root>/Gain */
	this.urlHashMap["pace_try:45"] = "pace_try.c:324&pace_try.h:97,201&pace_try_data.c:293";
	/* <Root>/Gesammte Einhuellende */
	this.urlHashMap["pace_try:37"] = "pace_try.h:155";
	/* <Root>/Indicis der  Elektroden */
	this.urlHashMap["pace_try:24"] = "pace_try.h:159";
	/* <Root>/Main Function */
	this.urlHashMap["pace_try:4"] = "pace_try.c:39,88,379,431&pace_try.h:98,99,100,101,102";
	/* <Root>/Multiport
Selector */
	this.urlHashMap["pace_try:44"] = "pace_try.c:325";
	/* <Root>/Original datei */
	this.urlHashMap["pace_try:38"] = "pace_try.h:163";
	/* <Root>/Spectural Envlope Detection */
	this.urlHashMap["pace_try:12"] = "pace_try.h:166&fftEnv.c:26";
	/* <S1>/MATLAB Function */
	this.urlHashMap["pace_try:25"] = "pace_try.h:105,106,174,177,180,183,186,189&Sel.c:40,223&pace_try_data.c:26,36,39,42,45,48";
	/* <S1>/u */
	this.urlHashMap["pace_try:18"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=pace_try:18";
	/* <S1>/y */
	this.urlHashMap["pace_try:19"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=pace_try:19";
	/* <S1>/y1 */
	this.urlHashMap["pace_try:26"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=pace_try:26";
	/* <S2>/MATLAB Function */
	this.urlHashMap["pace_try:21"] = "pace_try.h:192,195&fftft.c:30,163,214&pace_try_data.c:52,97&glngcbaalfkfppph_power.c:18";
	/* <S2>/u */
	this.urlHashMap["pace_try:10"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=pace_try:10";
	/* <S2>/y */
	this.urlHashMap["pace_try:11"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=pace_try:11";
	/* <S3>:1 */
	this.urlHashMap["pace_try:4:1"] = "pace_try.c:380";
	/* <S3>:1:3 */
	this.urlHashMap["pace_try:4:1:3"] = "pace_try.c:381";
	/* <S3>:1:4 */
	this.urlHashMap["pace_try:4:1:4"] = "pace_try.c:385";
	/* <S3>:1:5 */
	this.urlHashMap["pace_try:4:1:5"] = "pace_try.c:389";
	/* <S3>:1:6 */
	this.urlHashMap["pace_try:4:1:6"] = "pace_try.c:393";
	/* <S3>:1:9 */
	this.urlHashMap["pace_try:4:1:9"] = "pace_try.c:395";
	/* <S3>:1:10 */
	this.urlHashMap["pace_try:4:1:10"] = "pace_try.c:398";
	/* <S3>:1:15 */
	this.urlHashMap["pace_try:4:1:15"] = "pace_try.c:400";
	/* <S3>:1:11 */
	this.urlHashMap["pace_try:4:1:11"] = "pace_try.c:402";
	/* <S3>:1:16 */
	this.urlHashMap["pace_try:4:1:16"] = "pace_try.c:410";
	/* <S3>:1:17 */
	this.urlHashMap["pace_try:4:1:17"] = "pace_try.c:413";
	/* <S3>:1:18 */
	this.urlHashMap["pace_try:4:1:18"] = "pace_try.c:415";
	/* <S3>:1:19 */
	this.urlHashMap["pace_try:4:1:19"] = "pace_try.c:416";
	/* <S3>:1:20 */
	this.urlHashMap["pace_try:4:1:20"] = "pace_try.c:421";
	/* <S4>/MATLAB Function */
	this.urlHashMap["pace_try:22"] = "pace_try.h:198&fftEnv.c:37,48&pace_try_data.c:101";
	/* <S4>/u */
	this.urlHashMap["pace_try:14"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=pace_try:14";
	/* <S4>/y */
	this.urlHashMap["pace_try:15"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=pace_try:15";
	/* <S5>:1 */
	this.urlHashMap["pace_try:25:1"] = "Sel.c:47";
	/* <S5>:1:8 */
	this.urlHashMap["pace_try:25:1:8"] = "Sel.c:48";
	/* <S5>:1:9 */
	this.urlHashMap["pace_try:25:1:9"] = "Sel.c:49";
	/* <S5>:1:11 */
	this.urlHashMap["pace_try:25:1:11"] = "Sel.c:50";
	/* <S5>:1:12 */
	this.urlHashMap["pace_try:25:1:12"] = "Sel.c:54";
	/* <S5>:1:13 */
	this.urlHashMap["pace_try:25:1:13"] = "Sel.c:59";
	/* <S5>:1:14 */
	this.urlHashMap["pace_try:25:1:14"] = "Sel.c:61";
	/* <S5>:1:15 */
	this.urlHashMap["pace_try:25:1:15"] = "Sel.c:64";
	/* <S5>:1:16 */
	this.urlHashMap["pace_try:25:1:16"] = "Sel.c:67";
	/* <S5>:1:17 */
	this.urlHashMap["pace_try:25:1:17"] = "Sel.c:69";
	/* <S5>:1:35 */
	this.urlHashMap["pace_try:25:1:35"] = "Sel.c:70";
	/* <S5>:1:36 */
	this.urlHashMap["pace_try:25:1:36"] = "Sel.c:72";
	/* <S5>:1:41 */
	this.urlHashMap["pace_try:25:1:41"] = "Sel.c:77";
	/* <S5>:1:42 */
	this.urlHashMap["pace_try:25:1:42"] = "Sel.c:81";
	/* <S5>:1:43 */
	this.urlHashMap["pace_try:25:1:43"] = "Sel.c:83";
	/* <S5>:1:51 */
	this.urlHashMap["pace_try:25:1:51"] = "Sel.c:84";
	/* <S5>:1:52 */
	this.urlHashMap["pace_try:25:1:52"] = "Sel.c:86";
	/* <S5>:1:53 */
	this.urlHashMap["pace_try:25:1:53"] = "Sel.c:91";
	/* <S5>:1:54 */
	this.urlHashMap["pace_try:25:1:54"] = "Sel.c:92";
	/* <S5>:1:56 */
	this.urlHashMap["pace_try:25:1:56"] = "Sel.c:98";
	/* <S5>:1:57 */
	this.urlHashMap["pace_try:25:1:57"] = "Sel.c:100";
	/* <S5>:1:46 */
	this.urlHashMap["pace_try:25:1:46"] = "Sel.c:161";
	/* <S5>:1:47 */
	this.urlHashMap["pace_try:25:1:47"] = "Sel.c:164";
	/* <S5>:1:18 */
	this.urlHashMap["pace_try:25:1:18"] = "Sel.c:168";
	/* <S5>:1:20 */
	this.urlHashMap["pace_try:25:1:20"] = "Sel.c:171";
	/* <S5>:1:21 */
	this.urlHashMap["pace_try:25:1:21"] = "Sel.c:195";
	/* <S5>:1:22 */
	this.urlHashMap["pace_try:25:1:22"] = "Sel.c:198";
	/* <S5>:1:26 */
	this.urlHashMap["pace_try:25:1:26"] = "Sel.c:204";
	/* <S5>:1:27 */
	this.urlHashMap["pace_try:25:1:27"] = "Sel.c:208";
	/* <S5>:1:28 */
	this.urlHashMap["pace_try:25:1:28"] = "Sel.c:211";
	/* <S6>:1 */
	this.urlHashMap["pace_try:21:1"] = "fftft.c:170";
	/* <S6>:1:7 */
	this.urlHashMap["pace_try:21:1:7"] = "fftft.c:171";
	/* <S6>:1:8 */
	this.urlHashMap["pace_try:21:1:8"] = "fftft.c:173";
	/* <S6>:1:9 */
	this.urlHashMap["pace_try:21:1:9"] = "fftft.c:181";
	/* <S6>:1:11 */
	this.urlHashMap["pace_try:21:1:11"] = "fftft.c:183";
	/* <S7>:1 */
	this.urlHashMap["pace_try:22:1"] = "fftEnv.c:33";
	/* <S7>:1:3 */
	this.urlHashMap["pace_try:22:1:3"] = "fftEnv.c:34";
	/* <S7>:1:5 */
	this.urlHashMap["pace_try:22:1:5"] = "fftEnv.c:35";
	/* <S7>:1:7 */
	this.urlHashMap["pace_try:22:1:7"] = "fftEnv.c:41";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "pace_try"};
	this.sidHashMap["pace_try"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S1>"] = {sid: "pace_try:16"};
	this.sidHashMap["pace_try:16"] = {rtwname: "<S1>"};
	this.rtwnameHashMap["<S2>"] = {sid: "pace_try:8"};
	this.sidHashMap["pace_try:8"] = {rtwname: "<S2>"};
	this.rtwnameHashMap["<S3>"] = {sid: "pace_try:4"};
	this.sidHashMap["pace_try:4"] = {rtwname: "<S3>"};
	this.rtwnameHashMap["<S4>"] = {sid: "pace_try:12"};
	this.sidHashMap["pace_try:12"] = {rtwname: "<S4>"};
	this.rtwnameHashMap["<S5>"] = {sid: "pace_try:25"};
	this.sidHashMap["pace_try:25"] = {rtwname: "<S5>"};
	this.rtwnameHashMap["<S6>"] = {sid: "pace_try:21"};
	this.sidHashMap["pace_try:21"] = {rtwname: "<S6>"};
	this.rtwnameHashMap["<S7>"] = {sid: "pace_try:22"};
	this.sidHashMap["pace_try:22"] = {rtwname: "<S7>"};
	this.rtwnameHashMap["<Root>/ALSA Audio Capture"] = {sid: "pace_try:42"};
	this.sidHashMap["pace_try:42"] = {rtwname: "<Root>/ALSA Audio Capture"};
	this.rtwnameHashMap["<Root>/Ausgewaehlte Elektroden"] = {sid: "pace_try:33"};
	this.sidHashMap["pace_try:33"] = {rtwname: "<Root>/Ausgewaehlte Elektroden"};
	this.rtwnameHashMap["<Root>/Ausgewaehlte Elektroden1"] = {sid: "pace_try:40"};
	this.sidHashMap["pace_try:40"] = {rtwname: "<Root>/Ausgewaehlte Elektroden1"};
	this.rtwnameHashMap["<Root>/Bandselection"] = {sid: "pace_try:16"};
	this.sidHashMap["pace_try:16"] = {rtwname: "<Root>/Bandselection"};
	this.rtwnameHashMap["<Root>/Buffer"] = {sid: "pace_try:30"};
	this.sidHashMap["pace_try:30"] = {rtwname: "<Root>/Buffer"};
	this.rtwnameHashMap["<Root>/Data Type Conversion"] = {sid: "pace_try:43"};
	this.sidHashMap["pace_try:43"] = {rtwname: "<Root>/Data Type Conversion"};
	this.rtwnameHashMap["<Root>/Elektrode Map"] = {sid: "pace_try:23"};
	this.sidHashMap["pace_try:23"] = {rtwname: "<Root>/Elektrode Map"};
	this.rtwnameHashMap["<Root>/FFT Filterbank"] = {sid: "pace_try:8"};
	this.sidHashMap["pace_try:8"] = {rtwname: "<Root>/FFT Filterbank"};
	this.rtwnameHashMap["<Root>/Gain"] = {sid: "pace_try:45"};
	this.sidHashMap["pace_try:45"] = {rtwname: "<Root>/Gain"};
	this.rtwnameHashMap["<Root>/Gesammte Einhuellende"] = {sid: "pace_try:37"};
	this.sidHashMap["pace_try:37"] = {rtwname: "<Root>/Gesammte Einhuellende"};
	this.rtwnameHashMap["<Root>/Indicis der  Elektroden"] = {sid: "pace_try:24"};
	this.sidHashMap["pace_try:24"] = {rtwname: "<Root>/Indicis der  Elektroden"};
	this.rtwnameHashMap["<Root>/Main Function"] = {sid: "pace_try:4"};
	this.sidHashMap["pace_try:4"] = {rtwname: "<Root>/Main Function"};
	this.rtwnameHashMap["<Root>/Multiport Selector"] = {sid: "pace_try:44"};
	this.sidHashMap["pace_try:44"] = {rtwname: "<Root>/Multiport Selector"};
	this.rtwnameHashMap["<Root>/NT-specific Demo1"] = {sid: "pace_try:39"};
	this.sidHashMap["pace_try:39"] = {rtwname: "<Root>/NT-specific Demo1"};
	this.rtwnameHashMap["<Root>/NT-specific Demo2"] = {sid: "pace_try:7"};
	this.sidHashMap["pace_try:7"] = {rtwname: "<Root>/NT-specific Demo2"};
	this.rtwnameHashMap["<Root>/Original datei"] = {sid: "pace_try:38"};
	this.sidHashMap["pace_try:38"] = {rtwname: "<Root>/Original datei"};
	this.rtwnameHashMap["<Root>/Spectural Envlope Detection"] = {sid: "pace_try:12"};
	this.sidHashMap["pace_try:12"] = {rtwname: "<Root>/Spectural Envlope Detection"};
	this.rtwnameHashMap["<S1>/Sel"] = {sid: "pace_try:17"};
	this.sidHashMap["pace_try:17"] = {rtwname: "<S1>/Sel"};
	this.rtwnameHashMap["<S1>/MATLAB Function"] = {sid: "pace_try:25"};
	this.sidHashMap["pace_try:25"] = {rtwname: "<S1>/MATLAB Function"};
	this.rtwnameHashMap["<S1>/u"] = {sid: "pace_try:18"};
	this.sidHashMap["pace_try:18"] = {rtwname: "<S1>/u"};
	this.rtwnameHashMap["<S1>/y"] = {sid: "pace_try:19"};
	this.sidHashMap["pace_try:19"] = {rtwname: "<S1>/y"};
	this.rtwnameHashMap["<S1>/y1"] = {sid: "pace_try:26"};
	this.sidHashMap["pace_try:26"] = {rtwname: "<S1>/y1"};
	this.rtwnameHashMap["<S2>/fftft"] = {sid: "pace_try:9"};
	this.sidHashMap["pace_try:9"] = {rtwname: "<S2>/fftft"};
	this.rtwnameHashMap["<S2>/MATLAB Function"] = {sid: "pace_try:21"};
	this.sidHashMap["pace_try:21"] = {rtwname: "<S2>/MATLAB Function"};
	this.rtwnameHashMap["<S2>/u"] = {sid: "pace_try:10"};
	this.sidHashMap["pace_try:10"] = {rtwname: "<S2>/u"};
	this.rtwnameHashMap["<S2>/y"] = {sid: "pace_try:11"};
	this.sidHashMap["pace_try:11"] = {rtwname: "<S2>/y"};
	this.rtwnameHashMap["<S3>:1"] = {sid: "pace_try:4:1"};
	this.sidHashMap["pace_try:4:1"] = {rtwname: "<S3>:1"};
	this.rtwnameHashMap["<S3>:1:3"] = {sid: "pace_try:4:1:3"};
	this.sidHashMap["pace_try:4:1:3"] = {rtwname: "<S3>:1:3"};
	this.rtwnameHashMap["<S3>:1:4"] = {sid: "pace_try:4:1:4"};
	this.sidHashMap["pace_try:4:1:4"] = {rtwname: "<S3>:1:4"};
	this.rtwnameHashMap["<S3>:1:5"] = {sid: "pace_try:4:1:5"};
	this.sidHashMap["pace_try:4:1:5"] = {rtwname: "<S3>:1:5"};
	this.rtwnameHashMap["<S3>:1:6"] = {sid: "pace_try:4:1:6"};
	this.sidHashMap["pace_try:4:1:6"] = {rtwname: "<S3>:1:6"};
	this.rtwnameHashMap["<S3>:1:9"] = {sid: "pace_try:4:1:9"};
	this.sidHashMap["pace_try:4:1:9"] = {rtwname: "<S3>:1:9"};
	this.rtwnameHashMap["<S3>:1:10"] = {sid: "pace_try:4:1:10"};
	this.sidHashMap["pace_try:4:1:10"] = {rtwname: "<S3>:1:10"};
	this.rtwnameHashMap["<S3>:1:15"] = {sid: "pace_try:4:1:15"};
	this.sidHashMap["pace_try:4:1:15"] = {rtwname: "<S3>:1:15"};
	this.rtwnameHashMap["<S3>:1:11"] = {sid: "pace_try:4:1:11"};
	this.sidHashMap["pace_try:4:1:11"] = {rtwname: "<S3>:1:11"};
	this.rtwnameHashMap["<S3>:1:16"] = {sid: "pace_try:4:1:16"};
	this.sidHashMap["pace_try:4:1:16"] = {rtwname: "<S3>:1:16"};
	this.rtwnameHashMap["<S3>:1:17"] = {sid: "pace_try:4:1:17"};
	this.sidHashMap["pace_try:4:1:17"] = {rtwname: "<S3>:1:17"};
	this.rtwnameHashMap["<S3>:1:18"] = {sid: "pace_try:4:1:18"};
	this.sidHashMap["pace_try:4:1:18"] = {rtwname: "<S3>:1:18"};
	this.rtwnameHashMap["<S3>:1:19"] = {sid: "pace_try:4:1:19"};
	this.sidHashMap["pace_try:4:1:19"] = {rtwname: "<S3>:1:19"};
	this.rtwnameHashMap["<S3>:1:20"] = {sid: "pace_try:4:1:20"};
	this.sidHashMap["pace_try:4:1:20"] = {rtwname: "<S3>:1:20"};
	this.rtwnameHashMap["<S4>/fftEnv"] = {sid: "pace_try:13"};
	this.sidHashMap["pace_try:13"] = {rtwname: "<S4>/fftEnv"};
	this.rtwnameHashMap["<S4>/MATLAB Function"] = {sid: "pace_try:22"};
	this.sidHashMap["pace_try:22"] = {rtwname: "<S4>/MATLAB Function"};
	this.rtwnameHashMap["<S4>/u"] = {sid: "pace_try:14"};
	this.sidHashMap["pace_try:14"] = {rtwname: "<S4>/u"};
	this.rtwnameHashMap["<S4>/y"] = {sid: "pace_try:15"};
	this.sidHashMap["pace_try:15"] = {rtwname: "<S4>/y"};
	this.rtwnameHashMap["<S5>:1"] = {sid: "pace_try:25:1"};
	this.sidHashMap["pace_try:25:1"] = {rtwname: "<S5>:1"};
	this.rtwnameHashMap["<S5>:1:8"] = {sid: "pace_try:25:1:8"};
	this.sidHashMap["pace_try:25:1:8"] = {rtwname: "<S5>:1:8"};
	this.rtwnameHashMap["<S5>:1:9"] = {sid: "pace_try:25:1:9"};
	this.sidHashMap["pace_try:25:1:9"] = {rtwname: "<S5>:1:9"};
	this.rtwnameHashMap["<S5>:1:11"] = {sid: "pace_try:25:1:11"};
	this.sidHashMap["pace_try:25:1:11"] = {rtwname: "<S5>:1:11"};
	this.rtwnameHashMap["<S5>:1:12"] = {sid: "pace_try:25:1:12"};
	this.sidHashMap["pace_try:25:1:12"] = {rtwname: "<S5>:1:12"};
	this.rtwnameHashMap["<S5>:1:13"] = {sid: "pace_try:25:1:13"};
	this.sidHashMap["pace_try:25:1:13"] = {rtwname: "<S5>:1:13"};
	this.rtwnameHashMap["<S5>:1:14"] = {sid: "pace_try:25:1:14"};
	this.sidHashMap["pace_try:25:1:14"] = {rtwname: "<S5>:1:14"};
	this.rtwnameHashMap["<S5>:1:15"] = {sid: "pace_try:25:1:15"};
	this.sidHashMap["pace_try:25:1:15"] = {rtwname: "<S5>:1:15"};
	this.rtwnameHashMap["<S5>:1:16"] = {sid: "pace_try:25:1:16"};
	this.sidHashMap["pace_try:25:1:16"] = {rtwname: "<S5>:1:16"};
	this.rtwnameHashMap["<S5>:1:17"] = {sid: "pace_try:25:1:17"};
	this.sidHashMap["pace_try:25:1:17"] = {rtwname: "<S5>:1:17"};
	this.rtwnameHashMap["<S5>:1:35"] = {sid: "pace_try:25:1:35"};
	this.sidHashMap["pace_try:25:1:35"] = {rtwname: "<S5>:1:35"};
	this.rtwnameHashMap["<S5>:1:36"] = {sid: "pace_try:25:1:36"};
	this.sidHashMap["pace_try:25:1:36"] = {rtwname: "<S5>:1:36"};
	this.rtwnameHashMap["<S5>:1:41"] = {sid: "pace_try:25:1:41"};
	this.sidHashMap["pace_try:25:1:41"] = {rtwname: "<S5>:1:41"};
	this.rtwnameHashMap["<S5>:1:42"] = {sid: "pace_try:25:1:42"};
	this.sidHashMap["pace_try:25:1:42"] = {rtwname: "<S5>:1:42"};
	this.rtwnameHashMap["<S5>:1:43"] = {sid: "pace_try:25:1:43"};
	this.sidHashMap["pace_try:25:1:43"] = {rtwname: "<S5>:1:43"};
	this.rtwnameHashMap["<S5>:1:51"] = {sid: "pace_try:25:1:51"};
	this.sidHashMap["pace_try:25:1:51"] = {rtwname: "<S5>:1:51"};
	this.rtwnameHashMap["<S5>:1:52"] = {sid: "pace_try:25:1:52"};
	this.sidHashMap["pace_try:25:1:52"] = {rtwname: "<S5>:1:52"};
	this.rtwnameHashMap["<S5>:1:53"] = {sid: "pace_try:25:1:53"};
	this.sidHashMap["pace_try:25:1:53"] = {rtwname: "<S5>:1:53"};
	this.rtwnameHashMap["<S5>:1:54"] = {sid: "pace_try:25:1:54"};
	this.sidHashMap["pace_try:25:1:54"] = {rtwname: "<S5>:1:54"};
	this.rtwnameHashMap["<S5>:1:56"] = {sid: "pace_try:25:1:56"};
	this.sidHashMap["pace_try:25:1:56"] = {rtwname: "<S5>:1:56"};
	this.rtwnameHashMap["<S5>:1:57"] = {sid: "pace_try:25:1:57"};
	this.sidHashMap["pace_try:25:1:57"] = {rtwname: "<S5>:1:57"};
	this.rtwnameHashMap["<S5>:1:46"] = {sid: "pace_try:25:1:46"};
	this.sidHashMap["pace_try:25:1:46"] = {rtwname: "<S5>:1:46"};
	this.rtwnameHashMap["<S5>:1:47"] = {sid: "pace_try:25:1:47"};
	this.sidHashMap["pace_try:25:1:47"] = {rtwname: "<S5>:1:47"};
	this.rtwnameHashMap["<S5>:1:18"] = {sid: "pace_try:25:1:18"};
	this.sidHashMap["pace_try:25:1:18"] = {rtwname: "<S5>:1:18"};
	this.rtwnameHashMap["<S5>:1:20"] = {sid: "pace_try:25:1:20"};
	this.sidHashMap["pace_try:25:1:20"] = {rtwname: "<S5>:1:20"};
	this.rtwnameHashMap["<S5>:1:21"] = {sid: "pace_try:25:1:21"};
	this.sidHashMap["pace_try:25:1:21"] = {rtwname: "<S5>:1:21"};
	this.rtwnameHashMap["<S5>:1:22"] = {sid: "pace_try:25:1:22"};
	this.sidHashMap["pace_try:25:1:22"] = {rtwname: "<S5>:1:22"};
	this.rtwnameHashMap["<S5>:1:26"] = {sid: "pace_try:25:1:26"};
	this.sidHashMap["pace_try:25:1:26"] = {rtwname: "<S5>:1:26"};
	this.rtwnameHashMap["<S5>:1:27"] = {sid: "pace_try:25:1:27"};
	this.sidHashMap["pace_try:25:1:27"] = {rtwname: "<S5>:1:27"};
	this.rtwnameHashMap["<S5>:1:28"] = {sid: "pace_try:25:1:28"};
	this.sidHashMap["pace_try:25:1:28"] = {rtwname: "<S5>:1:28"};
	this.rtwnameHashMap["<S6>:1"] = {sid: "pace_try:21:1"};
	this.sidHashMap["pace_try:21:1"] = {rtwname: "<S6>:1"};
	this.rtwnameHashMap["<S6>:1:7"] = {sid: "pace_try:21:1:7"};
	this.sidHashMap["pace_try:21:1:7"] = {rtwname: "<S6>:1:7"};
	this.rtwnameHashMap["<S6>:1:8"] = {sid: "pace_try:21:1:8"};
	this.sidHashMap["pace_try:21:1:8"] = {rtwname: "<S6>:1:8"};
	this.rtwnameHashMap["<S6>:1:9"] = {sid: "pace_try:21:1:9"};
	this.sidHashMap["pace_try:21:1:9"] = {rtwname: "<S6>:1:9"};
	this.rtwnameHashMap["<S6>:1:11"] = {sid: "pace_try:21:1:11"};
	this.sidHashMap["pace_try:21:1:11"] = {rtwname: "<S6>:1:11"};
	this.rtwnameHashMap["<S7>:1"] = {sid: "pace_try:22:1"};
	this.sidHashMap["pace_try:22:1"] = {rtwname: "<S7>:1"};
	this.rtwnameHashMap["<S7>:1:3"] = {sid: "pace_try:22:1:3"};
	this.sidHashMap["pace_try:22:1:3"] = {rtwname: "<S7>:1:3"};
	this.rtwnameHashMap["<S7>:1:5"] = {sid: "pace_try:22:1:5"};
	this.sidHashMap["pace_try:22:1:5"] = {rtwname: "<S7>:1:5"};
	this.rtwnameHashMap["<S7>:1:7"] = {sid: "pace_try:22:1:7"};
	this.sidHashMap["pace_try:22:1:7"] = {rtwname: "<S7>:1:7"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
