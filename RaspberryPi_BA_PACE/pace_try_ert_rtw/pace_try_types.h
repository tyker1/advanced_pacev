/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: pace_try_types.h
 *
 * Code generated for Simulink model 'pace_try'.
 *
 * Model version                  : 1.184
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:37:21 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_pace_try_types_h_
#define RTW_HEADER_pace_try_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_pace_try_T_ P_pace_try_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_pace_try_T RT_MODEL_pace_try_T;

#endif                                 /* RTW_HEADER_pace_try_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
