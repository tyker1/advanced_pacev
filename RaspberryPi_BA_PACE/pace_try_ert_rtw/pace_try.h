/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: pace_try.h
 *
 * Code generated for Simulink model 'pace_try'.
 *
 * Model version                  : 1.184
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:37:21 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_pace_try_h_
#define RTW_HEADER_pace_try_h_
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef pace_try_COMMON_INCLUDES_
# define pace_try_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "dt_info.h"
#include "ext_work.h"
#include "alsa_audio_capture_macro.h"
#endif                                 /* pace_try_COMMON_INCLUDES_ */

#include "pace_try_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Child system includes */
#include "Sel_private.h"
#include "Sel.h"
#include "fftEnv_private.h"
#include "fftEnv.h"
#include "fftft_private.h"
#include "fftft.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block signals (auto storage) */
typedef struct {
  creal_T spectrum[128];
  creal_T dcv0[128];
  real_T dv0[128];
  real_T dv1[128];
  real_T rtu_u[128];
  real_T r_sqr[128];
  real_T Buffer[128];                  /* '<Root>/Buffer' */
  real_T a_env[22];
  real_T Gain[16];                     /* '<Root>/Gain' */
  real_T electrode[11];                /* '<Root>/Main Function' */
  real_T index[11];                    /* '<Root>/Main Function' */
  real_T output[22];                   /* '<Root>/Main Function' */
  real_T output_1[484];                /* '<Root>/Main Function' */
  real_T env_out[22];                  /* '<Root>/Main Function' */
  real_T a_db[22];
  real_T Mask_a[11];
  real_T outAmp[11];                   /* '<S1>/MATLAB Function' */
  real_T outIndex[11];                 /* '<S1>/MATLAB Function' */
  real_T sorted_idx[11];
  real_T electrode_m[11];
  real_T b_index[11];
  real_T xwork[11];
  int16_T ALSAAudioCapture[32];        /* '<Root>/ALSA Audio Capture' */
  int32_T idx[11];
  int32_T iwork[11];
  real_T x4[4];
  creal_T spectrum_m;
  boolean_T x[11];
  real_T b_max;
  real_T re;
  real_T im;
  real_T temp_re;
  int8_T idx4[4];
  int8_T perm[4];
  int32_T yIdx;
  int32_T i;
  int32_T nNaNs;
  int32_T m;
  int32_T i3;
  int32_T i4;
  int32_T tailOffset;
  int32_T nTail;
  int32_T i_c;
  int32_T n;
  int32_T q;
  int32_T qend;
  int32_T iout;
} B_pace_try_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T Buffer_CircBuff[112];         /* '<Root>/Buffer' */
  struct {
    void *LoggedData;
  } AusgewaehlteElektroden_PWORK;      /* '<Root>/Ausgewaehlte Elektroden' */

  struct {
    void *LoggedData;
  } AusgewaehlteElektroden1_PWORK;     /* '<Root>/Ausgewaehlte Elektroden1' */

  struct {
    void *LoggedData;
  } ElektrodeMap_PWORK;                /* '<Root>/Elektrode Map' */

  struct {
    void *LoggedData;
  } GesammteEinhuellende_PWORK;        /* '<Root>/Gesammte Einhuellende' */

  struct {
    void *LoggedData;
  } IndicisderElektroden_PWORK;        /* '<Root>/Indicis der  Elektroden' */

  struct {
    void *LoggedData;
  } Originaldatei_PWORK;               /* '<Root>/Original datei' */

  int32_T Buffer_CircBufIdx;           /* '<Root>/Buffer' */
  int8_T SpecturalEnvlopeDetection_Subsy;/* '<Root>/Spectural Envlope Detection' */
  int8_T FFTFilterbank_SubsysRanBC;    /* '<Root>/FFT Filterbank' */
  int8_T Bandselection_SubsysRanBC;    /* '<Root>/Bandselection' */
} DW_pace_try_T;

/* Parameters (auto storage) */
struct P_pace_try_T_ {
  real_T MATLABFunction_Labs[22];      /* Expression: Labs
                                        * Referenced by: '<S1>/MATLAB Function'
                                        */
  real_T MATLABFunction_M;             /* Expression: M
                                        * Referenced by: '<S1>/MATLAB Function'
                                        */
  real_T MATLABFunction_alpha;         /* Expression: alpha
                                        * Referenced by: '<S1>/MATLAB Function'
                                        */
  real_T MATLABFunction_av;            /* Expression: av
                                        * Referenced by: '<S1>/MATLAB Function'
                                        */
  real_T MATLABFunction_sl;            /* Expression: sl
                                        * Referenced by: '<S1>/MATLAB Function'
                                        */
  real_T MATLABFunction_sr;            /* Expression: sr
                                        * Referenced by: '<S1>/MATLAB Function'
                                        */
  real_T MATLABFunction_WindowFunc[128];/* Expression: WindowFunc
                                         * Referenced by: '<S2>/MATLAB Function'
                                         */
  real_T MATLABFunction_WindowSize;    /* Expression: WindowSize
                                        * Referenced by: '<S2>/MATLAB Function'
                                        */
  real_T MATLABFunction_FilterKoeff[2816];/* Expression: FilterKoeff
                                           * Referenced by: '<S4>/MATLAB Function'
                                           */
  real_T Gain_Gain;                    /* Expression: 100.0/32767
                                        * Referenced by: '<Root>/Gain'
                                        */
  real_T Buffer_ic;                    /* Expression: 0
                                        * Referenced by: '<Root>/Buffer'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_pace_try_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_pace_try_T pace_try_P;

/* Block signals (auto storage) */
extern B_pace_try_T pace_try_B;

/* Block states (auto storage) */
extern DW_pace_try_T pace_try_DW;

/* Model entry point functions */
extern void pace_try_initialize(void);
extern void pace_try_step(void);
extern void pace_try_terminate(void);

/* Real-time Model object */
extern RT_MODEL_pace_try_T *const pace_try_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'pace_try'
 * '<S1>'   : 'pace_try/Bandselection'
 * '<S2>'   : 'pace_try/FFT Filterbank'
 * '<S3>'   : 'pace_try/Main Function'
 * '<S4>'   : 'pace_try/Spectural Envlope Detection'
 * '<S5>'   : 'pace_try/Bandselection/MATLAB Function'
 * '<S6>'   : 'pace_try/FFT Filterbank/MATLAB Function'
 * '<S7>'   : 'pace_try/Spectural Envlope Detection/MATLAB Function'
 */
#endif                                 /* RTW_HEADER_pace_try_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
