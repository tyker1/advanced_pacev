/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: pace_try_private.h
 *
 * Code generated for Simulink model 'pace_try'.
 *
 * Model version                  : 1.184
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:37:21 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_pace_try_private_h_
#define RTW_HEADER_pace_try_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Private macros used by the generated code to access rtModel */
#ifndef rtmSetTFinal
# define rtmSetTFinal(rtm, val)        ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               (&(rtm)->Timing.taskTime0)
#endif

extern const uint8_T rtCP_pooled_8itxA31bV5ED[15];

#define rtCP_ALSAAudioCapture_p1       rtCP_pooled_8itxA31bV5ED  /* Expression: device
                                                                  * Referenced by: '<Root>/ALSA Audio Capture'
                                                                  */
#endif                                 /* RTW_HEADER_pace_try_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
