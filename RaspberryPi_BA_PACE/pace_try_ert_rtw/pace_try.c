/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: pace_try.c
 *
 * Code generated for Simulink model 'pace_try'.
 *
 * Model version                  : 1.184
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:37:21 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "pace_try.h"
#include "pace_try_private.h"
#include "pace_try_dt.h"

/* Block signals (auto storage) */
B_pace_try_T pace_try_B;

/* Block states (auto storage) */
DW_pace_try_T pace_try_DW;

/* Real-time model */
RT_MODEL_pace_try_T pace_try_M_;
RT_MODEL_pace_try_T *const pace_try_M = &pace_try_M_;

/* Forward declaration for local functions */
static void pace_try_baaijmohhdjmekno_merge(int32_T idx[11], real_T x[11],
  int32_T offset, int32_T np, int32_T nq, int32_T iwork[11], real_T xwork[11]);
static void pace_try_mgdjimgdphlnaaaa_sort(real_T x[11]);

/* Function for MATLAB Function: '<Root>/Main Function' */
static void pace_try_baaijmohhdjmekno_merge(int32_T idx[11], real_T x[11],
  int32_T offset, int32_T np, int32_T nq, int32_T iwork[11], real_T xwork[11])
{
  int32_T exitg1;
  if (!((np == 0) || (nq == 0))) {
    pace_try_B.n = np + nq;
    pace_try_B.q = 0;
    while (pace_try_B.q + 1 <= pace_try_B.n) {
      iwork[pace_try_B.q] = idx[offset + pace_try_B.q];
      xwork[pace_try_B.q] = x[offset + pace_try_B.q];
      pace_try_B.q++;
    }

    pace_try_B.n = 0;
    pace_try_B.q = np;
    pace_try_B.qend = np + nq;
    pace_try_B.iout = offset - 1;
    do {
      exitg1 = 0;
      pace_try_B.iout++;
      if (xwork[pace_try_B.n] <= xwork[pace_try_B.q]) {
        idx[pace_try_B.iout] = iwork[pace_try_B.n];
        x[pace_try_B.iout] = xwork[pace_try_B.n];
        if (pace_try_B.n + 1 < np) {
          pace_try_B.n++;
        } else {
          exitg1 = 1;
        }
      } else {
        idx[pace_try_B.iout] = iwork[pace_try_B.q];
        x[pace_try_B.iout] = xwork[pace_try_B.q];
        if (pace_try_B.q + 1 < pace_try_B.qend) {
          pace_try_B.q++;
        } else {
          pace_try_B.q = pace_try_B.iout - pace_try_B.n;
          while (pace_try_B.n + 1 <= np) {
            idx[(pace_try_B.q + pace_try_B.n) + 1] = iwork[pace_try_B.n];
            x[(pace_try_B.q + pace_try_B.n) + 1] = xwork[pace_try_B.n];
            pace_try_B.n++;
          }

          exitg1 = 1;
        }
      }
    } while (exitg1 == 0);
  }
}

/* Function for MATLAB Function: '<Root>/Main Function' */
static void pace_try_mgdjimgdphlnaaaa_sort(real_T x[11])
{
  pace_try_B.x4[0] = 0.0;
  pace_try_B.idx4[0] = 0;
  pace_try_B.x4[1] = 0.0;
  pace_try_B.idx4[1] = 0;
  pace_try_B.x4[2] = 0.0;
  pace_try_B.idx4[2] = 0;
  pace_try_B.x4[3] = 0.0;
  pace_try_B.idx4[3] = 0;
  memset(&pace_try_B.xwork[0], 0, 11U * sizeof(real_T));
  for (pace_try_B.i_c = 0; pace_try_B.i_c < 11; pace_try_B.i_c++) {
    pace_try_B.idx[pace_try_B.i_c] = 0;
  }

  pace_try_B.nNaNs = 0;
  pace_try_B.i_c = 0;
  for (pace_try_B.m = 0; pace_try_B.m < 11; pace_try_B.m++) {
    if (rtIsNaN(x[pace_try_B.m])) {
      pace_try_B.idx[10 - pace_try_B.nNaNs] = pace_try_B.m + 1;
      pace_try_B.xwork[10 - pace_try_B.nNaNs] = x[pace_try_B.m];
      pace_try_B.nNaNs++;
    } else {
      pace_try_B.i_c++;
      pace_try_B.idx4[pace_try_B.i_c - 1] = (int8_T)(pace_try_B.m + 1);
      pace_try_B.x4[pace_try_B.i_c - 1] = x[pace_try_B.m];
      if (pace_try_B.i_c == 4) {
        pace_try_B.i_c = pace_try_B.m - pace_try_B.nNaNs;
        if (pace_try_B.x4[0] <= pace_try_B.x4[1]) {
          pace_try_B.tailOffset = 1;
          pace_try_B.nTail = 2;
        } else {
          pace_try_B.tailOffset = 2;
          pace_try_B.nTail = 1;
        }

        if (pace_try_B.x4[2] <= pace_try_B.x4[3]) {
          pace_try_B.i3 = 3;
          pace_try_B.i4 = 4;
        } else {
          pace_try_B.i3 = 4;
          pace_try_B.i4 = 3;
        }

        if (pace_try_B.x4[pace_try_B.tailOffset - 1] <=
            pace_try_B.x4[pace_try_B.i3 - 1]) {
          if (pace_try_B.x4[pace_try_B.nTail - 1] <= pace_try_B.x4[pace_try_B.i3
              - 1]) {
            pace_try_B.perm[0] = (int8_T)pace_try_B.tailOffset;
            pace_try_B.perm[1] = (int8_T)pace_try_B.nTail;
            pace_try_B.perm[2] = (int8_T)pace_try_B.i3;
            pace_try_B.perm[3] = (int8_T)pace_try_B.i4;
          } else if (pace_try_B.x4[pace_try_B.nTail - 1] <=
                     pace_try_B.x4[pace_try_B.i4 - 1]) {
            pace_try_B.perm[0] = (int8_T)pace_try_B.tailOffset;
            pace_try_B.perm[1] = (int8_T)pace_try_B.i3;
            pace_try_B.perm[2] = (int8_T)pace_try_B.nTail;
            pace_try_B.perm[3] = (int8_T)pace_try_B.i4;
          } else {
            pace_try_B.perm[0] = (int8_T)pace_try_B.tailOffset;
            pace_try_B.perm[1] = (int8_T)pace_try_B.i3;
            pace_try_B.perm[2] = (int8_T)pace_try_B.i4;
            pace_try_B.perm[3] = (int8_T)pace_try_B.nTail;
          }
        } else if (pace_try_B.x4[pace_try_B.tailOffset - 1] <=
                   pace_try_B.x4[pace_try_B.i4 - 1]) {
          if (pace_try_B.x4[pace_try_B.nTail - 1] <= pace_try_B.x4[pace_try_B.i4
              - 1]) {
            pace_try_B.perm[0] = (int8_T)pace_try_B.i3;
            pace_try_B.perm[1] = (int8_T)pace_try_B.tailOffset;
            pace_try_B.perm[2] = (int8_T)pace_try_B.nTail;
            pace_try_B.perm[3] = (int8_T)pace_try_B.i4;
          } else {
            pace_try_B.perm[0] = (int8_T)pace_try_B.i3;
            pace_try_B.perm[1] = (int8_T)pace_try_B.tailOffset;
            pace_try_B.perm[2] = (int8_T)pace_try_B.i4;
            pace_try_B.perm[3] = (int8_T)pace_try_B.nTail;
          }
        } else {
          pace_try_B.perm[0] = (int8_T)pace_try_B.i3;
          pace_try_B.perm[1] = (int8_T)pace_try_B.i4;
          pace_try_B.perm[2] = (int8_T)pace_try_B.tailOffset;
          pace_try_B.perm[3] = (int8_T)pace_try_B.nTail;
        }

        pace_try_B.idx[pace_try_B.i_c - 3] = pace_try_B.idx4[pace_try_B.perm[0]
          - 1];
        pace_try_B.idx[pace_try_B.i_c - 2] = pace_try_B.idx4[pace_try_B.perm[1]
          - 1];
        pace_try_B.idx[pace_try_B.i_c - 1] = pace_try_B.idx4[pace_try_B.perm[2]
          - 1];
        pace_try_B.idx[pace_try_B.i_c] = pace_try_B.idx4[pace_try_B.perm[3] - 1];
        x[pace_try_B.i_c - 3] = pace_try_B.x4[pace_try_B.perm[0] - 1];
        x[pace_try_B.i_c - 2] = pace_try_B.x4[pace_try_B.perm[1] - 1];
        x[pace_try_B.i_c - 1] = pace_try_B.x4[pace_try_B.perm[2] - 1];
        x[pace_try_B.i_c] = pace_try_B.x4[pace_try_B.perm[3] - 1];
        pace_try_B.i_c = 0;
      }
    }
  }

  if (pace_try_B.i_c > 0) {
    pace_try_B.perm[1] = 0;
    pace_try_B.perm[2] = 0;
    pace_try_B.perm[3] = 0;
    switch (pace_try_B.i_c) {
     case 1:
      pace_try_B.perm[0] = 1;
      break;

     case 2:
      if (pace_try_B.x4[0] <= pace_try_B.x4[1]) {
        pace_try_B.perm[0] = 1;
        pace_try_B.perm[1] = 2;
      } else {
        pace_try_B.perm[0] = 2;
        pace_try_B.perm[1] = 1;
      }
      break;

     default:
      if (pace_try_B.x4[0] <= pace_try_B.x4[1]) {
        if (pace_try_B.x4[1] <= pace_try_B.x4[2]) {
          pace_try_B.perm[0] = 1;
          pace_try_B.perm[1] = 2;
          pace_try_B.perm[2] = 3;
        } else if (pace_try_B.x4[0] <= pace_try_B.x4[2]) {
          pace_try_B.perm[0] = 1;
          pace_try_B.perm[1] = 3;
          pace_try_B.perm[2] = 2;
        } else {
          pace_try_B.perm[0] = 3;
          pace_try_B.perm[1] = 1;
          pace_try_B.perm[2] = 2;
        }
      } else if (pace_try_B.x4[0] <= pace_try_B.x4[2]) {
        pace_try_B.perm[0] = 2;
        pace_try_B.perm[1] = 1;
        pace_try_B.perm[2] = 3;
      } else if (pace_try_B.x4[1] <= pace_try_B.x4[2]) {
        pace_try_B.perm[0] = 2;
        pace_try_B.perm[1] = 3;
        pace_try_B.perm[2] = 1;
      } else {
        pace_try_B.perm[0] = 3;
        pace_try_B.perm[1] = 2;
        pace_try_B.perm[2] = 1;
      }
      break;
    }

    pace_try_B.m = 11;
    while (pace_try_B.m - 10 <= pace_try_B.i_c) {
      pace_try_B.idx[(pace_try_B.m - pace_try_B.nNaNs) - pace_try_B.i_c] =
        pace_try_B.idx4[pace_try_B.perm[pace_try_B.m - 11] - 1];
      x[(pace_try_B.m - pace_try_B.nNaNs) - pace_try_B.i_c] =
        pace_try_B.x4[pace_try_B.perm[pace_try_B.m - 11] - 1];
      pace_try_B.m++;
    }
  }

  pace_try_B.m = pace_try_B.nNaNs >> 1;
  pace_try_B.i_c = 1;
  while (pace_try_B.i_c <= pace_try_B.m) {
    pace_try_B.tailOffset = pace_try_B.idx[(pace_try_B.i_c - pace_try_B.nNaNs) +
      10];
    pace_try_B.idx[(pace_try_B.i_c - pace_try_B.nNaNs) + 10] = pace_try_B.idx[11
      - pace_try_B.i_c];
    pace_try_B.idx[11 - pace_try_B.i_c] = pace_try_B.tailOffset;
    x[(pace_try_B.i_c - pace_try_B.nNaNs) + 10] = pace_try_B.xwork[11 -
      pace_try_B.i_c];
    x[11 - pace_try_B.i_c] = pace_try_B.xwork[(pace_try_B.i_c - pace_try_B.nNaNs)
      + 10];
    pace_try_B.i_c++;
  }

  if ((pace_try_B.nNaNs & 1) != 0) {
    x[(pace_try_B.m - pace_try_B.nNaNs) + 11] = pace_try_B.xwork[(pace_try_B.m -
      pace_try_B.nNaNs) + 11];
  }

  if (11 - pace_try_B.nNaNs > 1) {
    for (pace_try_B.i_c = 0; pace_try_B.i_c < 11; pace_try_B.i_c++) {
      pace_try_B.iwork[pace_try_B.i_c] = 0;
    }

    pace_try_B.i_c = (11 - pace_try_B.nNaNs) >> 2;
    pace_try_B.m = 4;
    while (pace_try_B.i_c > 1) {
      if ((pace_try_B.i_c & 1) != 0) {
        pace_try_B.i_c--;
        pace_try_B.tailOffset = pace_try_B.m * pace_try_B.i_c;
        pace_try_B.nTail = 11 - (pace_try_B.nNaNs + pace_try_B.tailOffset);
        if (pace_try_B.nTail > pace_try_B.m) {
          pace_try_baaijmohhdjmekno_merge(pace_try_B.idx, x,
            pace_try_B.tailOffset, pace_try_B.m, pace_try_B.nTail - pace_try_B.m,
            pace_try_B.iwork, pace_try_B.xwork);
        }
      }

      pace_try_B.tailOffset = pace_try_B.m << 1;
      pace_try_B.i_c >>= 1;
      pace_try_B.nTail = 1;
      while (pace_try_B.nTail <= pace_try_B.i_c) {
        pace_try_baaijmohhdjmekno_merge(pace_try_B.idx, x, (pace_try_B.nTail - 1)
          * pace_try_B.tailOffset, pace_try_B.m, pace_try_B.m, pace_try_B.iwork,
          pace_try_B.xwork);
        pace_try_B.nTail++;
      }

      pace_try_B.m = pace_try_B.tailOffset;
    }

    if (11 - pace_try_B.nNaNs > pace_try_B.m) {
      pace_try_baaijmohhdjmekno_merge(pace_try_B.idx, x, 0, pace_try_B.m, 11 -
        (pace_try_B.nNaNs + pace_try_B.m), pace_try_B.iwork, pace_try_B.xwork);
    }
  }
}

/* Model step function */
void pace_try_step(void)
{
  /* Reset subsysRan breadcrumbs */
  srClearBC(pace_try_DW.Bandselection_SubsysRanBC);

  /* Reset subsysRan breadcrumbs */
  srClearBC(pace_try_DW.FFTFilterbank_SubsysRanBC);

  /* Reset subsysRan breadcrumbs */
  srClearBC(pace_try_DW.SpecturalEnvlopeDetection_Subsy);

  /* S-Function (alsa_audio_capture_sfcn): '<Root>/ALSA Audio Capture' */
  audioCapture(rtCP_ALSAAudioCapture_p1, pace_try_B.ALSAAudioCapture, 16U);
  for (pace_try_B.i = 0; pace_try_B.i < 16; pace_try_B.i++) {
    /* Gain: '<Root>/Gain' incorporates:
     *  S-Function (sdspmultiportsel): '<Root>/Multiport Selector'
     */
    pace_try_B.Gain[pace_try_B.i] = pace_try_P.Gain_Gain * (real_T)
      pace_try_B.ALSAAudioCapture[pace_try_B.i];
  }

  /* Buffer: '<Root>/Buffer' */
  pace_try_B.i = 0;
  while (pace_try_B.i < 112 - pace_try_DW.Buffer_CircBufIdx) {
    pace_try_B.Buffer[pace_try_B.i] =
      pace_try_DW.Buffer_CircBuff[pace_try_DW.Buffer_CircBufIdx + pace_try_B.i];
    pace_try_B.i++;
  }

  pace_try_B.yIdx = 112 - pace_try_DW.Buffer_CircBufIdx;
  pace_try_B.i = 0;
  while (pace_try_B.i < pace_try_DW.Buffer_CircBufIdx) {
    pace_try_B.Buffer[pace_try_B.yIdx + pace_try_B.i] =
      pace_try_DW.Buffer_CircBuff[pace_try_B.i];
    pace_try_B.i++;
  }

  pace_try_B.yIdx += pace_try_DW.Buffer_CircBufIdx;
  memcpy(&pace_try_B.Buffer[pace_try_B.yIdx], &pace_try_B.Gain[0], sizeof(real_T)
         << 4U);
  if (16 > 112 - pace_try_DW.Buffer_CircBufIdx) {
    pace_try_B.i = 0;
    while (pace_try_B.i < 112 - pace_try_DW.Buffer_CircBufIdx) {
      pace_try_DW.Buffer_CircBuff[pace_try_DW.Buffer_CircBufIdx + pace_try_B.i] =
        pace_try_B.Gain[pace_try_B.i];
      pace_try_B.i++;
    }

    pace_try_B.yIdx = -pace_try_DW.Buffer_CircBufIdx;
    pace_try_B.i = 0;
    while (pace_try_B.i < pace_try_DW.Buffer_CircBufIdx - 96) {
      pace_try_DW.Buffer_CircBuff[pace_try_B.i] = pace_try_B.Gain
        [(pace_try_B.yIdx + pace_try_B.i) + 112];
      pace_try_B.i++;
    }
  } else {
    for (pace_try_B.i = 0; pace_try_B.i < 16; pace_try_B.i++) {
      pace_try_DW.Buffer_CircBuff[pace_try_DW.Buffer_CircBufIdx + pace_try_B.i] =
        pace_try_B.Gain[pace_try_B.i];
    }
  }

  pace_try_DW.Buffer_CircBufIdx += 16;
  if (pace_try_DW.Buffer_CircBufIdx >= 112) {
    pace_try_DW.Buffer_CircBufIdx -= 112;
  }

  /* End of Buffer: '<Root>/Buffer' */

  /* MATLAB Function: '<Root>/Main Function' */
  /* MATLAB Function 'Main Function': '<S3>:1' */
  /* '<S3>:1:3' r_sqr = fftft(u); */
  fftft(pace_try_B.Buffer, pace_try_B.r_sqr);

  /*  Maginude Square from 128-Point FFT */
  /* '<S3>:1:4' a_env = fftEnv(r_sqr); */
  fftEnv(pace_try_B.r_sqr, pace_try_B.a_env);

  /*  Bandverteilung und Hullkurve Detection */
  /* '<S3>:1:5' [electrode,index] = Sel(a_env); */
  Sel(pace_try_B.a_env, pace_try_B.electrode_m, pace_try_B.b_index);

  /*  Selektion */
  /* '<S3>:1:6' env_out = a_env; */
  /*  Ausgabe der Einh�llende Einstellung f�r alle Elektrode */
  /* '<S3>:1:9' output = zeros(M,1); */
  memset(&pace_try_B.output[0], 0, 22U * sizeof(real_T));

  /* '<S3>:1:10' for i = 1:N */
  /*  Ausgabe der Impulse */
  /* '<S3>:1:15' sorted_idx = sort(index); */
  for (pace_try_B.i = 0; pace_try_B.i < 11; pace_try_B.i++) {
    /* '<S3>:1:11' output(index(i)) = electrode(i); */
    pace_try_B.output[(int32_T)pace_try_B.b_index[pace_try_B.i] - 1] =
      pace_try_B.electrode_m[pace_try_B.i];
    pace_try_B.sorted_idx[pace_try_B.i] = pace_try_B.b_index[pace_try_B.i];
  }

  pace_try_mgdjimgdphlnaaaa_sort(pace_try_B.sorted_idx);

  /* '<S3>:1:16' output_1 = zeros(M,N*2); */
  memset(&pace_try_B.output_1[0], 0, 484U * sizeof(real_T));

  /* '<S3>:1:17' for i = 1:N */
  for (pace_try_B.i = 0; pace_try_B.i < 11; pace_try_B.i++) {
    /* '<S3>:1:18' Amp = env_out(sorted_idx(i)); */
    /* '<S3>:1:19' output_1(sorted_idx(i),(i-1)*2+1) = Amp; */
    pace_try_B.output_1[((int32_T)pace_try_B.sorted_idx[pace_try_B.i] + 22 *
                         (pace_try_B.i << 1)) - 1] = pace_try_B.a_env[(int32_T)
      pace_try_B.sorted_idx[pace_try_B.i] - 1];

    /* '<S3>:1:20' output_1(sorted_idx(i),i*2) = -Amp; */
    pace_try_B.output_1[((int32_T)pace_try_B.sorted_idx[pace_try_B.i] + 22 *
                         (((1 + pace_try_B.i) << 1) - 1)) - 1] =
      -pace_try_B.a_env[(int32_T)pace_try_B.sorted_idx[pace_try_B.i] - 1];
    pace_try_B.electrode[pace_try_B.i] = pace_try_B.electrode_m[pace_try_B.i];
    pace_try_B.index[pace_try_B.i] = pace_try_B.b_index[pace_try_B.i];
  }

  memcpy(&pace_try_B.env_out[0], &pace_try_B.a_env[0], 22U * sizeof(real_T));

  /* End of MATLAB Function: '<Root>/Main Function' */

  /* External mode */
  rtExtModeUploadCheckTrigger(1);

  {                                    /* Sample time: [0.001s, 0.0s] */
    rtExtModeUpload(0, pace_try_M->Timing.taskTime0);
  }

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.001s, 0.0s] */
    if ((rtmGetTFinal(pace_try_M)!=-1) &&
        !((rtmGetTFinal(pace_try_M)-pace_try_M->Timing.taskTime0) >
          pace_try_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(pace_try_M, "Simulation finished");
    }

    if (rtmGetStopRequested(pace_try_M)) {
      rtmSetErrorStatus(pace_try_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  pace_try_M->Timing.taskTime0 =
    (++pace_try_M->Timing.clockTick0) * pace_try_M->Timing.stepSize0;
}

/* Model initialize function */
void pace_try_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)pace_try_M, 0,
                sizeof(RT_MODEL_pace_try_T));
  rtmSetTFinal(pace_try_M, 10.0);
  pace_try_M->Timing.stepSize0 = 0.001;

  /* External mode info */
  pace_try_M->Sizes.checksums[0] = (3378320782U);
  pace_try_M->Sizes.checksums[1] = (1375969568U);
  pace_try_M->Sizes.checksums[2] = (2870467739U);
  pace_try_M->Sizes.checksums[3] = (3951283042U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[8];
    pace_try_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = (sysRanDType *)&pace_try_DW.Bandselection_SubsysRanBC;
    systemRan[2] = (sysRanDType *)&pace_try_DW.Bandselection_SubsysRanBC;
    systemRan[3] = (sysRanDType *)&pace_try_DW.FFTFilterbank_SubsysRanBC;
    systemRan[4] = (sysRanDType *)&pace_try_DW.FFTFilterbank_SubsysRanBC;
    systemRan[5] = &rtAlwaysEnabled;
    systemRan[6] = (sysRanDType *)&pace_try_DW.SpecturalEnvlopeDetection_Subsy;
    systemRan[7] = (sysRanDType *)&pace_try_DW.SpecturalEnvlopeDetection_Subsy;
    rteiSetModelMappingInfoPtr(pace_try_M->extModeInfo,
      &pace_try_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(pace_try_M->extModeInfo, pace_try_M->Sizes.checksums);
    rteiSetTPtr(pace_try_M->extModeInfo, rtmGetTPtr(pace_try_M));
  }

  /* block I/O */
  (void) memset(((void *) &pace_try_B), 0,
                sizeof(B_pace_try_T));

  /* states (dwork) */
  (void) memset((void *)&pace_try_DW, 0,
                sizeof(DW_pace_try_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    pace_try_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 14;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.BTransTable = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.PTransTable = &rtPTransTable;
  }

  {
    int32_T i;

    /* Start for S-Function (alsa_audio_capture_sfcn): '<Root>/ALSA Audio Capture' */
    audioCaptureInit(rtCP_ALSAAudioCapture_p1, 16000U, 0.5, 16U);

    /* InitializeConditions for Buffer: '<Root>/Buffer' */
    for (i = 0; i < 112; i++) {
      pace_try_DW.Buffer_CircBuff[i] = pace_try_P.Buffer_ic;
    }

    pace_try_DW.Buffer_CircBufIdx = 0;

    /* End of InitializeConditions for Buffer: '<Root>/Buffer' */
  }
}

/* Model terminate function */
void pace_try_terminate(void)
{
  /* Terminate for S-Function (alsa_audio_capture_sfcn): '<Root>/ALSA Audio Capture' */
  audioCaptureTerminate(rtCP_ALSAAudioCapture_p1);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
