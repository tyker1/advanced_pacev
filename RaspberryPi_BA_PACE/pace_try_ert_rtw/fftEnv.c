/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: fftEnv.c
 *
 * Code generated for Simulink model 'pace_try'.
 *
 * Model version                  : 1.184
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:37:21 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "fftEnv.h"

/* Include model header file for global data */
#include "pace_try.h"
#include "pace_try_private.h"

/* Output and update for Simulink Function: '<Root>/Spectural Envlope Detection' */
void fftEnv(const real_T rtu_u[128], real_T rty_y[22])
{
  int32_T i;
  real_T tmp;
  int32_T i_0;

  /* MATLAB Function 'Spectural Envlope Detection/MATLAB Function': '<S7>:1' */
  /* '<S7>:1:3' a = zeros(M,1); */
  /* '<S7>:1:5' for i = 1:M */
  for (i = 0; i < 22; i++) {
    /* MATLAB Function: '<S4>/MATLAB Function' incorporates:
     *  SignalConversion: '<S4>/TmpSignal ConversionAtuOutport1'
     */
    /*  a ist die "Power-sum combination of bins" */
    /* '<S7>:1:7' a(i) = sqrt(r_sqr'*FilterKoeff(:,i)); */
    tmp = 0.0;
    for (i_0 = 0; i_0 < 128; i_0++) {
      tmp += pace_try_P.MATLABFunction_FilterKoeff[(i << 7) + i_0] * rtu_u[i_0];
    }

    /* SignalConversion: '<S4>/TmpSignal ConversionAtyInport1' incorporates:
     *  MATLAB Function: '<S4>/MATLAB Function'
     */
    rty_y[i] = sqrt(tmp);
  }

  pace_try_DW.SpecturalEnvlopeDetection_Subsy = 4;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
