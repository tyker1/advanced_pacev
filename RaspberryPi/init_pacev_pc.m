%Initialisierungsfunktion f��r ACE-Strategie
%% Erlkaerung fuer globalen Variable
global WindowSize;
global Samplefrequence;
global SamplePerFrame;
global M;
global N;
global WindowFunc;
global FilterKoeff;
global FilterGain;
global Labs;
global av;
global sl;
global sr;
global alpha;
global MidFreq;

%% Allgemeine Initialisierungen
Aufloesung_koeff = 8;
Aufloesung = 100;
%Samplefrequence = 44100; % Standart ACE arbeitet mit dem Abtastfrequenz von 16kHz
%strMedia = get_param('pacev_try/ALSA Audio Capture','sampleRateEnum');
strMedia = get_param('pacev_try_pc/From Multimedia File','MaskDisplay');
strfs = regexp(strMedia,'(?<=A:\s+)\d+(?=\s+)','match');
Samplefrequence = str2double(strfs);
WindowSize = 2^(nextpow2(Samplefrequence/Aufloesung))*Aufloesung_koeff; %FFT-Fenstergrosse definiert nach standart ACE
SamplePerFrame = WindowSize/8;
BinBW = Samplefrequence/WindowSize;
M = 24*Aufloesung_koeff-1; %Anzahl der Elektrode bzw. Freqenzbaender

N = 11; %Anzahl der aktivierende Elektrode je Frame

WindowFunc = hann(WindowSize); %Jedes Frame muss erst mit Hanningfenster vorverarbeitet sein


%% Einstellung fuer Filterbank, Standart ACE hat 22 Kanaele
% Format: Mittelfrequenz | Anzahl der FFT-Bins | Koeffizient fuer jedes
% FFT-Bins
% FilterVect =   [250.0	1	0.98
%                 312.0	1	0.98
%                 375.0	1	0.98
%                 437.0	1	0.98
%                 500.0	1	0.98
%                 562.0	1	0.98
%                 625.0	1	0.98
%                 687.0	1	0.98
%                 750.0	1	0.98
%                 812.0	1	0.98
%                 875.0	1	0.98
%                 937.0	1	0.98
%                 1000.0	1	0.98
%                 1062.0	1	0.98
%                 1125.0	1	0.98
%                 1187.0	1	0.98
%                 1250.0	1	0.98
%                 1312.0	2	0.68
%                 1437.0	2	0.68
%                 1562.0	2	0.68
%                 1687.0	2	0.68
%                 1812.0	2	0.68
%                 1937.0	2	0.68
%                 2062.0	2	0.68
%                 2187.0	2	0.68
%                 2312.0	3	0.65
%                 2500.0	3	0.65
%                 2687.0	3	0.65
%                 2875.0	3	0.65
%                 3062.0	4	0.65
%                 3312.0	4	0.65
%                 3562.0	4	0.65
%                 3812.0	4	0.65
%                 4062.0	5	0.65
%                 4375.0	5	0.65
%                 4687.0	5	0.65
%                 5000.0	5	0.65
%                 5312.0	6	0.65
%                 5687.0	6	0.65
%                 6062.0	7	0.65
%                 6500.0	7	0.65
%                 6937.0	8	0.65
%                 7437.0	8	0.65
% ];

FilterVect = calc_FilterVect(Samplefrequence,Aufloesung_koeff,Aufloesung);

FilterKoeff = zeros(WindowSize,M);
MidFreq = FilterVect(:,1);
FFTBins = FilterVect(:,2);
FilterGain = FilterVect(:,3);

%% Einhuellende Detektion
offsetglobal = MidFreq(1)/(BinBW)+1;
for i = 1:M
    offset = sum(FFTBins(1:i-1));
    % Da FFT(0) ist der Gleichanteil, faengt das Filterbank von FFT(2) an
    % da Erste Bandpassfilter dem Mittelfrequenz 250Hz entspricht
    FilterKoeff((offset+offsetglobal):(offset+FFTBins(i)+offsetglobal-1),i) = ones(FFTBins(i),1).*FilterGain(i);
end


%% Pyschoakustisches Modell
%Intepretierte Funktion der absoluten Hoerschwelle in Ruheumgebung nach 
%"Perceptual coding of digital audio" IEEE vol. 88, no. 4, pp455-515, 2000
Tabs = @(f) 3.64*(f/1000).^(-0.8)-6.5*exp(-0.6.*(f/1000-3.3).^2)+10^(-3)*(f/1000).^4;

% Level Function
Labs = Tabs(MidFreq);
Labs = Labs - min(Labs); % Nach Paper, soll minimale Werte von Labs 0dB sein

% Einstellung fuer Masking Modells
% Alle in dB
av = 10; % attenuation parameter
sl=20; % left slope
sr=17; % right slope
alpha = 0.25; % parameter fuer "Power-law Modell"