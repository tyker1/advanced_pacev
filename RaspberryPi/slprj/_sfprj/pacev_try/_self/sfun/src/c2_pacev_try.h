#ifndef __c2_pacev_try_h__
#define __c2_pacev_try_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc2_pacev_tryInstanceStruct
#define typedef_SFc2_pacev_tryInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c2_sfEvent;
  boolean_T c2_doneDoubleBufferReInit;
  uint8_T c2_is_active_c2_pacev_try;
  real_T c2_M;
  real_T c2_N;
  void *c2_fEmlrtCtx;
  real_T (*c2_u)[1024];
  real_T (*c2_electrode)[11];
  real_T (*c2_index)[11];
  real_T (*c2_output)[47];
  real_T (*c2_env_out)[47];
  real_T (*c2_stimulation_elektrode)[517];
} SFc2_pacev_tryInstanceStruct;

#endif                                 /*typedef_SFc2_pacev_tryInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c2_pacev_try_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c2_pacev_try_get_check_sum(mxArray *plhs[]);
extern void c2_pacev_try_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
