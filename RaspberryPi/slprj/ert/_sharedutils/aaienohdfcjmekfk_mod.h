/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: aaienohdfcjmekfk_mod.h
 *
 * Code generated for Simulink model 'pacev_try'.
 *
 * Model version                  : 1.275
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 13:22:30 2017
 */

#ifndef SHARE_aaienohdfcjmekfk_mod
#define SHARE_aaienohdfcjmekfk_mod
#include "rtwtypes.h"

extern real_T aaienohdfcjmekfk_mod(real_T x);

#endif

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
