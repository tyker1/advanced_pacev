/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: Sel.h
 *
 * Code generated for Simulink model 'pacev_try'.
 *
 * Model version                  : 1.276
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 13:58:40 2017
 */

#ifndef RTW_HEADER_Sel_
#define RTW_HEADER_Sel_

/* Shared type includes */
#include "rtwtypes.h"

extern void Sel(const real_T rtu_u[47], real_T rty_y[11], real_T rty_y1[11]);

#endif

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
