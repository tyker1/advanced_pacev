/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: imopbiechlfcbaie_power.c
 *
 * Code generated for Simulink model 'pacev_try'.
 *
 * Model version                  : 1.276
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 13:58:40 2017
 */

#include "rtwtypes.h"
#include "imopbiechlfcbaie_power.h"

/* Function for MATLAB Function: '<S2>/MATLAB Function' */
void imopbiechlfcbaie_power(const real_T a[1024], real_T y[1024])
{
  int32_T k;
  for (k = 0; k < 1024; k++) {
    y[k] = a[k] * a[k];
  }
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
