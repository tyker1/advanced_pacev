/*
 * const_params.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "pacev_try".
 *
 * Model version              : 1.271
 * Simulink Coder version : 8.12 (R2017a) 16-Feb-2017
 * C source code generated on : Mon Sep 11 12:44:19 2017
 */
#include "rtwtypes.h"

extern const uint8_T rtCP_pooled_8itxA31bV5ED[15];
const uint8_T rtCP_pooled_8itxA31bV5ED[15] = { 112U, 108U, 117U, 103U, 104U,
  119U, 58U, 67U, 79U, 68U, 69U, 67U, 44U, 48U, 0U } ;

extern const uint8_T rtCP_pooled_aiE3KO8ZGEkC[11];
const uint8_T rtCP_pooled_aiE3KO8ZGEkC[11] = { 104U, 119U, 58U, 67U, 79U, 68U,
  69U, 67U, 44U, 48U, 0U } ;
