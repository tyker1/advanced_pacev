/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: bimghlfkcjekfkno_mod.c
 *
 * Code generated for Simulink model 'pacev_try'.
 *
 * Model version                  : 1.270
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 11:40:42 2017
 */

#include "rtwtypes.h"
#include "rtGetNaN.h"
#include "rt_nonfinite.h"
#include <math.h>
#include "bimghlfkcjekfkno_mod.h"

/* Function for MATLAB Function: '<Root>/MATLAB Function' */
real_T bimghlfkcjekfkno_mod(real_T x)
{
  real_T r;
  if ((!rtIsInf(x)) && (!rtIsNaN(x))) {
    if (x == 0.0) {
      r = 0.0;
    } else {
      r = fmod(x, 2.0);
      if (r == 0.0) {
        r = 0.0;
      } else {
        if (x < 0.0) {
          r += 2.0;
        }
      }
    }
  } else {
    r = (rtNaN);
  }

  return r;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
