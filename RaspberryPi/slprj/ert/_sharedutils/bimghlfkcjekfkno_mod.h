/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: bimghlfkcjekfkno_mod.h
 *
 * Code generated for Simulink model 'pacev_try'.
 *
 * Model version                  : 1.270
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 11:40:42 2017
 */

#ifndef SHARE_bimghlfkcjekfkno_mod
#define SHARE_bimghlfkcjekfkno_mod
#include "rtwtypes.h"

extern real_T bimghlfkcjekfkno_mod(real_T x);

#endif

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
