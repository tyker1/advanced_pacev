/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: Sel.c
 *
 * Code generated for Simulink model 'pacev_try'.
 *
 * Model version                  : 1.276
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 13:58:40 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Sel.h"

/* Include model header file for global data */
#include "pacev_try.h"
#include "pacev_try_private.h"
#include "rt_powd_snf.h"

/* Output and update for Simulink Function: '<Root>/Bandselection' */
void Sel(const real_T rtu_u[47], real_T rty_y[11], real_T rty_y1[11])
{
  real_T my_max;
  int32_T max_idx;
  real_T It;
  real_T Li;
  int32_T b_idx;
  int32_T b_ii;
  int32_T ixstart;
  static const real_T b_Labs[47] = { 18.431900092938619, 16.42214167200401,
    14.976886531369818, 13.881954464258991, 13.020163169471868,
    12.32171694939565, 11.742305408251809, 11.252358259068876,
    10.831330901848469, 10.464465293427885, 10.140856262705501,
    9.8522464621931185, 9.4713608669258971, 9.0367476791010812,
    8.6618589085482469, 8.328085076796107, 8.02185973946467, 7.73283274462244,
    7.4528079083413985, 7.175114797847586, 6.8942411673227761,
    6.5318155862282321, 6.0712794660218261, 5.5751712549347952,
    5.0397293182737721, 4.4660654674808882, 3.7571992637324318,
    2.9190145417513018, 2.0920527305804293, 1.2418633842248847,
    0.49235911171106661, 0.017180760772615322, 0.0, 0.556214331844429,
    1.7366463271893946, 3.3278760170170045, 4.884320163808, 6.03612287585944,
    6.762168543455596, 7.3358693533778014, 8.0335643288442853,
    9.0539949628944072, 10.661541925255044, 13.230332163312859,
    17.280542740127139, 23.716936192340114, 34.195303058095682 };

  static const int8_T b[8] = { -7, -6, -5, -4, -3, -2, -1, 0 };

  int32_T i;
  int32_T ii_size_idx_1;
  boolean_T exitg1;

  /* MATLAB Function: '<S1>/MATLAB Function' incorporates:
   *  SignalConversion: '<S1>/TmpSignal ConversionAtuOutport1'
   */
  /* MATLAB Function 'Bandselection/MATLAB Function': '<S6>:1' */
  /* '<S6>:1:3' outAmp = zeros(1,N); */
  /* '<S6>:1:4' outIndex = zeros(1,N); */
  /* '<S6>:1:5' Mask_a = zeros(1,N); */
  memset(&pacev_try_B.Mask_a[0], 0, 11U * sizeof(real_T));
  for (i = 0; i < 11; i++) {
    pacev_try_B.outIndex[i] = 0;
  }

  /* '<S6>:1:6' index_a = zeros(1,M); */
  /* '<S6>:1:7' new_a = zeros(1,M); */
  memset(&pacev_try_B.new_a[0], 0, 47U * sizeof(real_T));

  /* '<S6>:1:9' for i = 8:8:M */
  for (i = 0; i < 5; i++) {
    max_idx = (i << 3) + 8;

    /* '<S6>:1:10' [am, idx_a] = max(a(i-7:i)); */
    ixstart = 1;
    my_max = rtu_u[max_idx - 8];
    b_idx = -8;
    if (rtIsNaN(rtu_u[max_idx - 8])) {
      b_ii = 2;
      exitg1 = false;
      while ((!exitg1) && (b_ii < 9)) {
        ixstart = b_ii;
        if (!rtIsNaN(rtu_u[(b_ii + max_idx) - 9])) {
          my_max = rtu_u[(b_ii + max_idx) - 9];
          b_idx = b_ii - 9;
          exitg1 = true;
        } else {
          b_ii++;
        }
      }
    }

    if (ixstart < 8) {
      while (ixstart + 1 < 9) {
        if (rtu_u[(b[ixstart] + max_idx) - 1] > my_max) {
          my_max = rtu_u[(int8_T)((int8_T)((int8_T)ixstart + max_idx) - 7) - 1];
          b_idx = ixstart - 8;
        }

        ixstart++;
      }
    }

    /* '<S6>:1:11' new_a(idx_a+i-8) = am; */
    pacev_try_B.new_a[b_idx + max_idx] = my_max;
  }

  /* '<S6>:1:14' a_db = 10*log10(new_a*32767); */
  for (i = 0; i < 47; i++) {
    pacev_try_B.new_a[i] = log10(pacev_try_B.new_a[i] * 32767.0) * 10.0;
  }

  /* '<S6>:1:15' for i = 1:N */
  for (i = 0; i < 11; i++) {
    /* '<S6>:1:16' my_max = -65536; */
    my_max = -65536.0;

    /* '<S6>:1:17' max_idx = 1; */
    max_idx = 0;

    /* '<S6>:1:18' for j = 1:M */
    for (ixstart = 0; ixstart < 47; ixstart++) {
      /* '<S6>:1:19' mask_effect = power_law(Mask_a, j, i-1, Labs, sl, sr, av, alpha); */
      /* '<S6>:1:37' if (Len == 0) */
      if (i == 0) {
        /* '<S6>:1:38' Lt = 0; */
        It = 0.0;
      } else {
        /*  I = 10^(L/10); */
        /*  It(zi) = [Iabs(zi)^alpha + Sum(Ii(zj)^alpha)]^(1/alpha) */
        /* '<S6>:1:43' It = (10^(Labs(z)/10)).^alpha; */
        It = rt_powd_snf(rt_powd_snf(10.0, b_Labs[ixstart] / 10.0), 0.25);

        /* '<S6>:1:44' for idx = 1:Len */
        for (b_idx = 0; b_idx < i; b_idx++) {
          /* '<S6>:1:45' It = It + (10.^(spread_func(a(idx),idx, z, sl, sr, av)/10)).^alpha; */
          /* '<S6>:1:53' if (i > z) */
          if (1 + b_idx > 1 + ixstart) {
            /* '<S6>:1:54' Li = a - av - sl*(i-z); */
            Li = (pacev_try_B.Mask_a[b_idx] - 10.0) - (real_T)(b_idx - ixstart) *
              20.0;
          } else {
            /* '<S6>:1:55' else */
            /* '<S6>:1:56' Li = a - av - sr*(z-i); */
            Li = (pacev_try_B.Mask_a[b_idx] - 10.0) - (real_T)(ixstart - b_idx) *
              17.0;
          }

          /* '<S6>:1:59' if (Li < 0) */
          if (Li < 0.0) {
            /* '<S6>:1:60' Li = 0; */
            Li = 0.0;
          }

          It += rt_powd_snf(rt_powd_snf(10.0, Li / 10.0), 0.25);
        }

        /* '<S6>:1:48' It = It.^(1/alpha); */
        It = rt_powd_snf(It, 4.0);

        /* '<S6>:1:49' Lt = 10*log10(It); */
        It = 10.0 * log10(It);
      }

      /* '<S6>:1:20' delta = a_db(j) - mask_effect; */
      It = pacev_try_B.new_a[ixstart] - It;

      /* '<S6>:1:22' if ((delta > my_max) && (isempty(find(outIndex == j, 1)))) */
      if (It > my_max) {
        b_idx = 1 + ixstart;
        for (ii_size_idx_1 = 0; ii_size_idx_1 < 11; ii_size_idx_1++) {
          pacev_try_B.x[ii_size_idx_1] = (pacev_try_B.outIndex[ii_size_idx_1] ==
            b_idx);
        }

        b_idx = 0;
        ii_size_idx_1 = 1;
        b_ii = 1;
        exitg1 = false;
        while ((!exitg1) && (b_ii < 12)) {
          if (pacev_try_B.x[b_ii - 1]) {
            b_idx = 1;
            exitg1 = true;
          } else {
            b_ii++;
          }
        }

        if (b_idx == 0) {
          ii_size_idx_1 = 0;
        }

        if (ii_size_idx_1 == 0) {
          /* '<S6>:1:23' my_max = delta; */
          my_max = It;

          /* '<S6>:1:24' max_idx = j; */
          max_idx = ixstart;
        }
      }
    }

    /* '<S6>:1:28' Mask_a(i) = a_db(max_idx); */
    pacev_try_B.Mask_a[i] = pacev_try_B.new_a[max_idx];

    /*  Einheit dB */
    /* '<S6>:1:29' outAmp(i) = a(max_idx); */
    pacev_try_B.outAmp[i] = rtu_u[max_idx];

    /* '<S6>:1:30' outIndex(i) = max_idx; */
    pacev_try_B.outIndex[i] = (int8_T)(max_idx + 1);

    /* SignalConversion: '<S1>/TmpSignal ConversionAtyInport1' incorporates:
     *  SignalConversion: '<S1>/TmpSignal ConversionAtuOutport1'
     */
    rty_y[i] = pacev_try_B.outAmp[i];

    /* SignalConversion: '<S1>/TmpSignal ConversionAty1Inport1' */
    rty_y1[i] = pacev_try_B.outIndex[i];
  }

  /* End of MATLAB Function: '<S1>/MATLAB Function' */
  pacev_try_DW.Bandselection_SubsysRanBC = 4;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
