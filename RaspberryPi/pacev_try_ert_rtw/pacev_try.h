/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: pacev_try.h
 *
 * Code generated for Simulink model 'pacev_try'.
 *
 * Model version                  : 1.276
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 13:58:40 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_pacev_try_h_
#define RTW_HEADER_pacev_try_h_
#include <math.h>
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef pacev_try_COMMON_INCLUDES_
# define pacev_try_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "dt_info.h"
#include "ext_work.h"
#include "alsa_audio_capture_macro.h"
#include "alsa_audio_playback_macro.h"
#endif                                 /* pacev_try_COMMON_INCLUDES_ */

#include "pacev_try_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Child system includes */
#include "Sel_private.h"
#include "Sel.h"
#include "fftEnv_private.h"
#include "fftEnv.h"
#include "fftft_private.h"
#include "fftft.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block signals (auto storage) */
typedef struct {
  creal_T spectrum[1024];
  creal_T dcv0[1024];
  real_T dv0[1024];
  real_T dv1[1024];
  real_T rtu_u[1024];
  real_T r_sqr[1024];
  real_T Buffer[1024];                 /* '<Root>/Buffer' */
  real_T DataTypeConversion[256];      /* '<Root>/Data Type Conversion' */
  real_T Gain[128];                    /* '<Root>/Gain' */
  real_T signal[128];                  /* '<Root>/Synthese' */
  real_T t[128];
  int16_T DataTypeConversion1[256];    /* '<Root>/Data Type Conversion1' */
  real_T new_a[47];
  real_T a_env[47];
  real_T Mask_a[11];
  real_T outAmp[11];                   /* '<S1>/MATLAB Function' */
  real_T electrode[11];
  real_T b_index[11];
  creal_T spectrum_m;
  boolean_T x[11];
  int8_T outIndex[11];                 /* '<S1>/MATLAB Function' */
  real_T re;
  real_T im;
  real_T temp_re;
  real_T amp1;
  real_T delta1;
  real_T delta2;
  real_T delta;
  int32_T j;
  int32_T i;
} B_pacev_try_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T Buffer_CircBuff[896];         /* '<Root>/Buffer' */
  real_T tl[47];                       /* '<Root>/Synthese' */
  real_T last_Amp[47];                 /* '<Root>/Synthese' */
  struct {
    void *LoggedData;
  } ToWorkspace5_PWORK;                /* '<Root>/To Workspace5' */

  struct {
    void *LoggedData;
  } ToWorkspace6_PWORK;                /* '<Root>/To Workspace6' */

  int32_T Buffer_CircBufIdx;           /* '<Root>/Buffer' */
  int8_T SpecturalEnvlopeDetection_Subsy;/* '<Root>/Spectural Envlope Detection' */
  int8_T FFTFilterbank_SubsysRanBC;    /* '<Root>/FFT Filterbank' */
  int8_T Bandselection_SubsysRanBC;    /* '<Root>/Bandselection' */
} DW_pacev_try_T;

/* Parameters (auto storage) */
struct P_pacev_try_T_ {
  real_T Gain_Gain;                    /* Expression: 100.0/32767
                                        * Referenced by: '<Root>/Gain'
                                        */
  real_T Buffer_ic;                    /* Expression: 0
                                        * Referenced by: '<Root>/Buffer'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_pacev_try_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_pacev_try_T pacev_try_P;

/* Block signals (auto storage) */
extern B_pacev_try_T pacev_try_B;

/* Block states (auto storage) */
extern DW_pacev_try_T pacev_try_DW;

/* Model entry point functions */
extern void pacev_try_initialize(void);
extern void pacev_try_step(void);
extern void pacev_try_terminate(void);

/* Real-time Model object */
extern RT_MODEL_pacev_try_T *const pacev_try_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'pacev_try'
 * '<S1>'   : 'pacev_try/Bandselection'
 * '<S2>'   : 'pacev_try/FFT Filterbank'
 * '<S3>'   : 'pacev_try/MATLAB Function'
 * '<S4>'   : 'pacev_try/Spectural Envlope Detection'
 * '<S5>'   : 'pacev_try/Synthese'
 * '<S6>'   : 'pacev_try/Bandselection/MATLAB Function'
 * '<S7>'   : 'pacev_try/FFT Filterbank/MATLAB Function'
 * '<S8>'   : 'pacev_try/Spectural Envlope Detection/MATLAB Function'
 */
#endif                                 /* RTW_HEADER_pacev_try_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
