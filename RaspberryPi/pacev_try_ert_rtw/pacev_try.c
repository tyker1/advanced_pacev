/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: pacev_try.c
 *
 * Code generated for Simulink model 'pacev_try'.
 *
 * Model version                  : 1.276
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 13:58:40 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "pacev_try.h"
#include "pacev_try_private.h"
#include "pacev_try_dt.h"

/* Named constants for MATLAB Function: '<Root>/Synthese' */
#define pacev_try_SamplePerFrame       (128.0)

/* Block signals (auto storage) */
B_pacev_try_T pacev_try_B;

/* Block states (auto storage) */
DW_pacev_try_T pacev_try_DW;

/* Real-time model */
RT_MODEL_pacev_try_T pacev_try_M_;
RT_MODEL_pacev_try_T *const pacev_try_M = &pacev_try_M_;

/* Model step function */
void pacev_try_step(void)
{
  static const real_T b[47] = { 193.798828125, 236.865234375, 279.931640625,
    322.998046875, 366.064453125, 409.130859375, 452.197265625, 495.263671875,
    538.330078125, 581.396484375, 624.462890625, 667.529296875, 732.12890625,
    818.26171875, 904.39453125, 990.52734375, 1076.66015625, 1162.79296875,
    1248.92578125, 1335.05859375, 1421.19140625, 1528.857421875, 1658.056640625,
    1787.255859375, 1916.455078125, 2045.654296875, 2196.38671875, 2368.65234375,
    2540.91796875, 2734.716796875, 2950.048828125, 3186.9140625, 3445.3125,
    3725.244140625, 4048.2421875, 4414.306640625, 4823.4375, 5275.634765625,
    5770.8984375, 6330.76171875, 6955.224609375, 7644.287109375, 8441.015625,
    9366.943359375, 10422.0703125, 11627.9296875, 13027.587890625 };

  /* Reset subsysRan breadcrumbs */
  srClearBC(pacev_try_DW.Bandselection_SubsysRanBC);

  /* Reset subsysRan breadcrumbs */
  srClearBC(pacev_try_DW.FFTFilterbank_SubsysRanBC);

  /* Reset subsysRan breadcrumbs */
  srClearBC(pacev_try_DW.SpecturalEnvlopeDetection_Subsy);

  /* S-Function (alsa_audio_capture_sfcn): '<Root>/ALSA Audio Capture' */
  audioCapture(rtCP_ALSAAudioCapture_p1, pacev_try_B.DataTypeConversion1, 128U);

  /* DataTypeConversion: '<Root>/Data Type Conversion' */
  for (pacev_try_B.i = 0; pacev_try_B.i < 256; pacev_try_B.i++) {
    pacev_try_B.DataTypeConversion[pacev_try_B.i] =
      pacev_try_B.DataTypeConversion1[pacev_try_B.i];
  }

  /* End of DataTypeConversion: '<Root>/Data Type Conversion' */
  for (pacev_try_B.j = 0; pacev_try_B.j < 128; pacev_try_B.j++) {
    /* Gain: '<Root>/Gain' incorporates:
     *  S-Function (sdspmultiportsel): '<Root>/Multiport Selector'
     */
    pacev_try_B.Gain[pacev_try_B.j] = pacev_try_P.Gain_Gain * (real_T)(int16_T)
      pacev_try_B.DataTypeConversion[pacev_try_B.j];
  }

  /* Buffer: '<Root>/Buffer' */
  pacev_try_B.i = 0;
  while (pacev_try_B.i < 896 - pacev_try_DW.Buffer_CircBufIdx) {
    pacev_try_B.Buffer[pacev_try_B.i] =
      pacev_try_DW.Buffer_CircBuff[pacev_try_DW.Buffer_CircBufIdx +
      pacev_try_B.i];
    pacev_try_B.i++;
  }

  pacev_try_B.j = 896 - pacev_try_DW.Buffer_CircBufIdx;
  pacev_try_B.i = 0;
  while (pacev_try_B.i < pacev_try_DW.Buffer_CircBufIdx) {
    pacev_try_B.Buffer[pacev_try_B.j + pacev_try_B.i] =
      pacev_try_DW.Buffer_CircBuff[pacev_try_B.i];
    pacev_try_B.i++;
  }

  pacev_try_B.j += pacev_try_DW.Buffer_CircBufIdx;
  memcpy(&pacev_try_B.Buffer[pacev_try_B.j], &pacev_try_B.Gain[0], sizeof(real_T)
         << 7U);
  if (128 > 896 - pacev_try_DW.Buffer_CircBufIdx) {
    pacev_try_B.i = 0;
    while (pacev_try_B.i < 896 - pacev_try_DW.Buffer_CircBufIdx) {
      pacev_try_DW.Buffer_CircBuff[pacev_try_DW.Buffer_CircBufIdx +
        pacev_try_B.i] = pacev_try_B.Gain[pacev_try_B.i];
      pacev_try_B.i++;
    }

    pacev_try_B.j = -pacev_try_DW.Buffer_CircBufIdx;
    pacev_try_B.i = 0;
    while (pacev_try_B.i < pacev_try_DW.Buffer_CircBufIdx - 768) {
      pacev_try_DW.Buffer_CircBuff[pacev_try_B.i] = pacev_try_B.Gain
        [(pacev_try_B.j + pacev_try_B.i) + 896];
      pacev_try_B.i++;
    }
  } else {
    for (pacev_try_B.i = 0; pacev_try_B.i < 128; pacev_try_B.i++) {
      pacev_try_DW.Buffer_CircBuff[pacev_try_DW.Buffer_CircBufIdx +
        pacev_try_B.i] = pacev_try_B.Gain[pacev_try_B.i];
    }
  }

  pacev_try_DW.Buffer_CircBufIdx += 128;
  if (pacev_try_DW.Buffer_CircBufIdx >= 896) {
    pacev_try_DW.Buffer_CircBufIdx -= 896;
  }

  /* End of Buffer: '<Root>/Buffer' */

  /* MATLAB Function: '<Root>/MATLAB Function' */
  /* MATLAB Function 'MATLAB Function': '<S3>:1' */
  /* '<S3>:1:3' r_sqr = fftft(u); */
  fftft(pacev_try_B.Buffer, pacev_try_B.r_sqr);

  /*  Maginude Square from 128-Point FFT */
  /* '<S3>:1:4' a_env = fftEnv(r_sqr); */
  fftEnv(pacev_try_B.r_sqr, pacev_try_B.a_env);

  /*  Bandverteilung und Hullkurve Detection */
  /* '<S3>:1:5' [electrode,index] = Sel(a_env); */
  Sel(pacev_try_B.a_env, pacev_try_B.electrode, pacev_try_B.b_index);

  /*  Selektion */
  /* '<S3>:1:7' output = zeros(M,1); */
  memset(&pacev_try_B.a_env[0], 0, 47U * sizeof(real_T));

  /* '<S3>:1:8' env_out = a_env; */
  /* '<S3>:1:10' for i = 1:N */
  for (pacev_try_B.i = 0; pacev_try_B.i < 11; pacev_try_B.i++) {
    /* '<S3>:1:11' output(index(i)) = electrode(i); */
    pacev_try_B.a_env[(int32_T)pacev_try_B.b_index[pacev_try_B.i] - 1] =
      pacev_try_B.electrode[pacev_try_B.i];
  }

  /* End of MATLAB Function: '<Root>/MATLAB Function' */

  /* MATLAB Function: '<Root>/Synthese' */
  /*  Stimulation der 22 Psysikalicher Kanaelen */
  /*  Initialisieren, 22 Elektrode, jedes Freme N Impulse ausgegeben werden */
  /*  (fuer virtuelle Kanal, 2 Impulse gleichzeitig wird nur zur Aufbau der */
  /*  virtuelle kanal genutzt, aber wird als eine Impulse programmesig */
  /*  gezaehlt) */
  /* '<S3>:1:19' stimulation_elektrode = zeros(M,N); */
  /* '<S3>:1:20' idx = 1; */
  /* stimulationsreihenfolge : Tieffrequenz -> Hochfrequenz */
  /* '<S3>:1:22' for i = 1:M */
  /* MATLAB Function 'Synthese': '<S5>:1' */
  /* '<S5>:1:3' signal = zeros(SamplePerFrame,1); */
  memset(&pacev_try_B.signal[0], 0, sizeof(real_T) << 7U);

  /* '<S5>:1:8' if isempty(tl) */
  /* '<S5>:1:12' if isempty(last_Amp) */
  /* signal = zeros(SamplePerFrame,1); */
  /* '<S5>:1:16' for i = 1:M */
  for (pacev_try_B.i = 0; pacev_try_B.i < 47; pacev_try_B.i++) {
    /* '<S5>:1:17' amp1 = last_Amp(i); */
    pacev_try_B.amp1 = pacev_try_DW.last_Amp[pacev_try_B.i];

    /* '<S5>:1:18' last_Amp(i) = elektrode2(i); */
    pacev_try_DW.last_Amp[pacev_try_B.i] = pacev_try_B.a_env[pacev_try_B.i];

    /* '<S5>:1:19' amp2 = elektrode2(i); */
    /* '<S5>:1:21' if (amp1 ~= 0) || (amp2 ~= 0) */
    if ((pacev_try_B.amp1 != 0.0) || (pacev_try_B.a_env[pacev_try_B.i] != 0.0))
    {
      /* '<S5>:1:22' tr = tl(i) + SamplePerFrame/Samplefrequence; */
      /* '<S5>:1:23' t = linspace(tl(i),tr,SamplePerFrame); */
      pacev_try_B.delta = pacev_try_DW.tl[pacev_try_B.i];
      pacev_try_B.t[127] = pacev_try_DW.tl[pacev_try_B.i] +
        0.0029024943310657597;
      pacev_try_B.t[0] = pacev_try_DW.tl[pacev_try_B.i];
      if (((pacev_try_DW.tl[pacev_try_B.i] + 0.0029024943310657597 < 0.0) !=
           (pacev_try_DW.tl[pacev_try_B.i] < 0.0)) && ((fabs
            (pacev_try_DW.tl[pacev_try_B.i]) > 8.9884656743115785E+307) || (fabs
            (pacev_try_DW.tl[pacev_try_B.i] + 0.0029024943310657597) >
            8.9884656743115785E+307))) {
        pacev_try_B.delta1 = pacev_try_DW.tl[pacev_try_B.i] / 127.0;
        pacev_try_B.delta2 = (pacev_try_DW.tl[pacev_try_B.i] +
                              0.0029024943310657597) / 127.0;
        for (pacev_try_B.j = 0; pacev_try_B.j < 126; pacev_try_B.j++) {
          pacev_try_B.t[pacev_try_B.j + 1] = ((1.0 + (real_T)pacev_try_B.j) *
            pacev_try_B.delta2 + pacev_try_B.delta) - (1.0 + (real_T)
            pacev_try_B.j) * pacev_try_B.delta1;
        }
      } else {
        pacev_try_B.delta1 = ((pacev_try_DW.tl[pacev_try_B.i] +
          0.0029024943310657597) - pacev_try_DW.tl[pacev_try_B.i]) / 127.0;
        for (pacev_try_B.j = 0; pacev_try_B.j < 126; pacev_try_B.j++) {
          pacev_try_B.t[pacev_try_B.j + 1] = (1.0 + (real_T)pacev_try_B.j) *
            pacev_try_B.delta1 + pacev_try_B.delta;
        }
      }

      /* '<S5>:1:24' tl(i) = tr + 1/Samplefrequence; */
      pacev_try_DW.tl[pacev_try_B.i] = (pacev_try_DW.tl[pacev_try_B.i] +
        0.0029024943310657597) + 2.2675736961451248E-5;

      /* '<S5>:1:25' temp_amp = amp_interp(amp1,amp2,SamplePerFrame); */
      /* '<S5>:1:34' n = 0:amp_N-1; */
      /* '<S5>:1:36' delta = (a2-a1) / amp_N; */
      pacev_try_B.delta = (pacev_try_B.a_env[pacev_try_B.i] - pacev_try_B.amp1) /
        pacev_try_SamplePerFrame;

      /* '<S5>:1:38' vect_amp = a1 + n.*delta; */
      /* '<S5>:1:26' signal = signal + temp_amp'.* sin(2*pi*MidFreq(i).*t'); */
      pacev_try_B.delta1 = 6.2831853071795862 * b[pacev_try_B.i];
      for (pacev_try_B.j = 0; pacev_try_B.j < 128; pacev_try_B.j++) {
        pacev_try_B.signal[pacev_try_B.j] += ((real_T)pacev_try_B.j *
          pacev_try_B.delta + pacev_try_B.amp1) * sin(pacev_try_B.delta1 *
          pacev_try_B.t[pacev_try_B.j]);
      }
    }
  }

  /* End of MATLAB Function: '<Root>/Synthese' */
  /* signal = signal; */
  for (pacev_try_B.i = 0; pacev_try_B.i < 128; pacev_try_B.i++) {
    /* SignalConversion: '<Root>/ConcatBufferAtMatrix ConcatenateIn1' */
    pacev_try_B.DataTypeConversion[pacev_try_B.i] =
      pacev_try_B.signal[pacev_try_B.i];

    /* SignalConversion: '<Root>/ConcatBufferAtMatrix ConcatenateIn2' */
    pacev_try_B.DataTypeConversion[pacev_try_B.i + 128] =
      pacev_try_B.signal[pacev_try_B.i];
  }

  /* DataTypeConversion: '<Root>/Data Type Conversion1' */
  for (pacev_try_B.i = 0; pacev_try_B.i < 256; pacev_try_B.i++) {
    pacev_try_B.delta1 = floor(pacev_try_B.DataTypeConversion[pacev_try_B.i]);
    if (rtIsNaN(pacev_try_B.delta1) || rtIsInf(pacev_try_B.delta1)) {
      pacev_try_B.delta1 = 0.0;
    } else {
      pacev_try_B.delta1 = fmod(pacev_try_B.delta1, 65536.0);
    }

    pacev_try_B.DataTypeConversion1[pacev_try_B.i] = (int16_T)
      (pacev_try_B.delta1 < 0.0 ? (int32_T)(int16_T)-(int16_T)(uint16_T)
       -pacev_try_B.delta1 : (int32_T)(int16_T)(uint16_T)pacev_try_B.delta1);
  }

  /* End of DataTypeConversion: '<Root>/Data Type Conversion1' */

  /* S-Function (alsa_audio_playback_sfcn): '<Root>/ALSA Audio Playback' */
  audioPlayback(rtCP_ALSAAudioPlayback_p1, pacev_try_B.DataTypeConversion1, 128U);

  /* External mode */
  rtExtModeUploadCheckTrigger(1);

  {                                    /* Sample time: [0.0029024943310657597s, 0.0s] */
    rtExtModeUpload(0, pacev_try_M->Timing.taskTime0);
  }

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.0029024943310657597s, 0.0s] */
    if ((rtmGetTFinal(pacev_try_M)!=-1) &&
        !((rtmGetTFinal(pacev_try_M)-pacev_try_M->Timing.taskTime0) >
          pacev_try_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(pacev_try_M, "Simulation finished");
    }

    if (rtmGetStopRequested(pacev_try_M)) {
      rtmSetErrorStatus(pacev_try_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  pacev_try_M->Timing.taskTime0 =
    (++pacev_try_M->Timing.clockTick0) * pacev_try_M->Timing.stepSize0;
}

/* Model initialize function */
void pacev_try_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)pacev_try_M, 0,
                sizeof(RT_MODEL_pacev_try_T));
  rtmSetTFinal(pacev_try_M, 9.9990929705215414);
  pacev_try_M->Timing.stepSize0 = 0.0029024943310657597;

  /* External mode info */
  pacev_try_M->Sizes.checksums[0] = (4174458385U);
  pacev_try_M->Sizes.checksums[1] = (1558026289U);
  pacev_try_M->Sizes.checksums[2] = (3272008028U);
  pacev_try_M->Sizes.checksums[3] = (2483825612U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[9];
    pacev_try_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = (sysRanDType *)&pacev_try_DW.Bandselection_SubsysRanBC;
    systemRan[2] = (sysRanDType *)&pacev_try_DW.Bandselection_SubsysRanBC;
    systemRan[3] = (sysRanDType *)&pacev_try_DW.FFTFilterbank_SubsysRanBC;
    systemRan[4] = (sysRanDType *)&pacev_try_DW.FFTFilterbank_SubsysRanBC;
    systemRan[5] = &rtAlwaysEnabled;
    systemRan[6] = (sysRanDType *)&pacev_try_DW.SpecturalEnvlopeDetection_Subsy;
    systemRan[7] = (sysRanDType *)&pacev_try_DW.SpecturalEnvlopeDetection_Subsy;
    systemRan[8] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(pacev_try_M->extModeInfo,
      &pacev_try_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(pacev_try_M->extModeInfo, pacev_try_M->Sizes.checksums);
    rteiSetTPtr(pacev_try_M->extModeInfo, rtmGetTPtr(pacev_try_M));
  }

  /* block I/O */
  (void) memset(((void *) &pacev_try_B), 0,
                sizeof(B_pacev_try_T));

  /* states (dwork) */
  (void) memset((void *)&pacev_try_DW, 0,
                sizeof(DW_pacev_try_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    pacev_try_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 14;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.BTransTable = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.PTransTable = &rtPTransTable;
  }

  {
    int32_T i;

    /* Start for S-Function (alsa_audio_capture_sfcn): '<Root>/ALSA Audio Capture' */
    audioCaptureInit(rtCP_ALSAAudioCapture_p1, 44100U, 0.5, 128U);

    /* Start for S-Function (alsa_audio_playback_sfcn): '<Root>/ALSA Audio Playback' */
    audioPlaybackInit(rtCP_ALSAAudioPlayback_p1, 44100U, 0.5, 128U);

    /* InitializeConditions for Buffer: '<Root>/Buffer' */
    for (i = 0; i < 896; i++) {
      pacev_try_DW.Buffer_CircBuff[i] = pacev_try_P.Buffer_ic;
    }

    pacev_try_DW.Buffer_CircBufIdx = 0;

    /* End of InitializeConditions for Buffer: '<Root>/Buffer' */

    /* SystemInitialize for MATLAB Function: '<Root>/Synthese' */
    /* '<S5>:1:9' tl = zeros(M,1); */
    /* '<S5>:1:13' last_Amp = zeros(M,1); */
    memset(&pacev_try_DW.tl[0], 0, 47U * sizeof(real_T));
    memset(&pacev_try_DW.last_Amp[0], 0, 47U * sizeof(real_T));
  }
}

/* Model terminate function */
void pacev_try_terminate(void)
{
  /* Terminate for S-Function (alsa_audio_capture_sfcn): '<Root>/ALSA Audio Capture' */
  audioCaptureTerminate(rtCP_ALSAAudioCapture_p1);

  /* Terminate for S-Function (alsa_audio_playback_sfcn): '<Root>/ALSA Audio Playback' */
  audioPlaybackTerminate(rtCP_ALSAAudioPlayback_p1);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
