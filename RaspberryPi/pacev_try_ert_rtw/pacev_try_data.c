/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: pacev_try_data.c
 *
 * Code generated for Simulink model 'pacev_try'.
 *
 * Model version                  : 1.276
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 13:58:40 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "pacev_try.h"
#include "pacev_try_private.h"

/* Block parameters (auto storage) */
P_pacev_try_T pacev_try_P = {
  0.0030518509475997192,               /* Expression: 100.0/32767
                                        * Referenced by: '<Root>/Gain'
                                        */
  0.0                                  /* Expression: 0
                                        * Referenced by: '<Root>/Buffer'
                                        */
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
