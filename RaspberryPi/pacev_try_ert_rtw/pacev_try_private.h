/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: pacev_try_private.h
 *
 * Code generated for Simulink model 'pacev_try'.
 *
 * Model version                  : 1.276
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 13:58:40 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_pacev_try_private_h_
#define RTW_HEADER_pacev_try_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Private macros used by the generated code to access rtModel */
#ifndef rtmSetTFinal
# define rtmSetTFinal(rtm, val)        ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               (&(rtm)->Timing.taskTime0)
#endif

extern const uint8_T rtCP_pooled_8itxA31bV5ED[15];

#define rtCP_ALSAAudioCapture_p1       rtCP_pooled_8itxA31bV5ED  /* Expression: device
                                                                  * Referenced by: '<Root>/ALSA Audio Capture'
                                                                  */
#define rtCP_ALSAAudioPlayback_p1      rtCP_pooled_8itxA31bV5ED  /* Expression: device
                                                                  * Referenced by: '<Root>/ALSA Audio Playback'
                                                                  */
#endif                                 /* RTW_HEADER_pacev_try_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
