/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: fftft_private.h
 *
 * Code generated for Simulink model 'pacev_try'.
 *
 * Model version                  : 1.276
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 13:58:40 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_fftft_private_h_
#define RTW_HEADER_fftft_private_h_
#ifndef pacev_try_COMMON_INCLUDES_
# define pacev_try_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "dt_info.h"
#include "ext_work.h"
#include "alsa_audio_capture_macro.h"
#include "alsa_audio_playback_macro.h"
#endif                                 /* pacev_try_COMMON_INCLUDES_ */

/* Shared type includes */
#include "multiword_types.h"
#endif                                 /* RTW_HEADER_fftft_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
