if (~exist('plotBefehl','var'))
    choice = questdlg('FFT Filterbank muss erst umgewandelt werden um zu plotten wollen Sie jetzt sie umwandeln?', ...
	'FFT Filterbank Umwandlung', ...
	'Ja','Nein','Nein');
    if (choice == 'Ja')
        plotBefehl = "fvtool(";
        for i = 1:M
            seitenBand= FFTBins(i)/2*(125/2);
            sperrBand = ((FFTBins(i)-1)/2*125+125)/2;
            FFTfilter = designfilt('bandpassiir', 'StopbandFrequency1', MidFreq(i)-sperrBand, 'PassbandFrequency1', MidFreq(i)-seitenBand, 'PassbandFrequency2', MidFreq(i)+seitenBand, 'StopbandFrequency2', MidFreq(i)+sperrBand, 'StopbandAttenuation1', 6, 'PassbandRipple', 1, 'StopbandAttenuation2', 6, 'SampleRate', 16000);
            befehl = sprintf("f%d=FFTfilter;",i);
            eval(befehl);
            variableName = sprintf("f%d",i);
            if ( i == 1)
                plotBefehl = plotBefehl + variableName;
            else
                plotBefehl = plotBefehl + "," + variableName;
            end
        end

        plotBefehl = plotBefehl + ")";
        eval(plotBefehl);
        ylim([-30,5]);
        xlim([0,8]);
    end
else
    eval(plotBefehl);
    ylim([-30,5]);
    xlim([0,8]);
end