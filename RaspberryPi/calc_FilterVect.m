function [ FilterVect ] = calc_FilterVect( fs,Aufloesung_koeff,Aufloesung )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

CW = @(f) 25+75*(1+1.4*(f/1000).^2).^0.69;
%Aufloesung = 50;
fftlen=2^(nextpow2(fs/Aufloesung))*Aufloesung_koeff;

BinBW = fs/fftlen;
BinFreq = 0:(fftlen/2+1);
BinFreq = BinFreq .* BinBW;

idx = round(200/BinBW);

totalFt = 0;
AimFt = 24*Aufloesung_koeff-1;

FilterVect = zeros(AimFt,3);

while (idx < fftlen/2+2) && (totalFt < AimFt)
    Bandwidth = CW(BinFreq(idx))/Aufloesung_koeff;
    BinWidth = round(Bandwidth / BinBW);
    
    totalFt = totalFt + 1;
    
    midFreq = mean(BinFreq(idx:idx+BinWidth));
    
    FilterVect(totalFt,:) = [midFreq, BinWidth, 1];
    idx = idx + BinWidth;
end

end

