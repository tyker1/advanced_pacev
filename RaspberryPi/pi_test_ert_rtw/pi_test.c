/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: pi_test.c
 *
 * Code generated for Simulink model 'pi_test'.
 *
 * Model version                  : 1.4
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:06:34 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "pi_test.h"
#include "pi_test_private.h"
#include "pi_test_dt.h"

/* Block signals (auto storage) */
B_pi_test_T pi_test_B;

/* Block states (auto storage) */
DW_pi_test_T pi_test_DW;

/* Real-time model */
RT_MODEL_pi_test_T pi_test_M_;
RT_MODEL_pi_test_T *const pi_test_M = &pi_test_M_;

/* Model step function */
void pi_test_step(void)
{
  int32_T i;
  real_T rtb_Gain1;

  /* S-Function (alsa_audio_capture_sfcn): '<Root>/ALSA Audio Capture' */
  audioCapture(pi_test_ConstP.pooled3, pi_test_B.ALSAAudioCapture, 128U);

  /* DataTypeConversion: '<Root>/Data Type Conversion' */
  for (i = 0; i < 256; i++) {
    pi_test_B.Gain[i] = pi_test_B.ALSAAudioCapture[i];
  }

  /* End of DataTypeConversion: '<Root>/Data Type Conversion' */

  /* Gain: '<Root>/Gain' */
  for (i = 0; i < 256; i++) {
    pi_test_B.Gain[i] *= pi_test_P.Gain_Gain;
  }

  /* End of Gain: '<Root>/Gain' */

  /* S-Function (sdspmultiportsel): '<Root>/Multiport Selector' */
  memcpy(&pi_test_B.MultiportSelector_o1[0], &pi_test_B.Gain[0], sizeof(real_T) <<
         7U);
  for (i = 0; i < 128; i++) {
    /* Gain: '<Root>/Gain1' */
    rtb_Gain1 = pi_test_P.Gain1_Gain * pi_test_B.MultiportSelector_o1[i];

    /* SignalConversion: '<Root>/ConcatBufferAtMatrix ConcatenateIn1' */
    pi_test_B.Gain[i] = rtb_Gain1;

    /* SignalConversion: '<Root>/ConcatBufferAtMatrix ConcatenateIn2' */
    pi_test_B.Gain[i + 128] = rtb_Gain1;
  }

  /* DataTypeConversion: '<Root>/Data Type Conversion1' */
  for (i = 0; i < 256; i++) {
    rtb_Gain1 = floor(pi_test_B.Gain[i]);
    if (rtIsNaN(rtb_Gain1) || rtIsInf(rtb_Gain1)) {
      rtb_Gain1 = 0.0;
    } else {
      rtb_Gain1 = fmod(rtb_Gain1, 65536.0);
    }

    pi_test_B.DataTypeConversion1[i] = (int16_T)(rtb_Gain1 < 0.0 ? (int32_T)
      (int16_T)-(int16_T)(uint16_T)-rtb_Gain1 : (int32_T)(int16_T)(uint16_T)
      rtb_Gain1);
  }

  /* End of DataTypeConversion: '<Root>/Data Type Conversion1' */

  /* S-Function (alsa_audio_playback_sfcn): '<Root>/ALSA Audio Playback' */
  audioPlayback(pi_test_ConstP.pooled3, pi_test_B.DataTypeConversion1, 128U);

  /* External mode */
  rtExtModeUploadCheckTrigger(1);

  {                                    /* Sample time: [0.008s, 0.0s] */
    rtExtModeUpload(0, pi_test_M->Timing.taskTime0);
  }

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.008s, 0.0s] */
    if ((rtmGetTFinal(pi_test_M)!=-1) &&
        !((rtmGetTFinal(pi_test_M)-pi_test_M->Timing.taskTime0) >
          pi_test_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(pi_test_M, "Simulation finished");
    }

    if (rtmGetStopRequested(pi_test_M)) {
      rtmSetErrorStatus(pi_test_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  pi_test_M->Timing.taskTime0 =
    (++pi_test_M->Timing.clockTick0) * pi_test_M->Timing.stepSize0;
}

/* Model initialize function */
void pi_test_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)pi_test_M, 0,
                sizeof(RT_MODEL_pi_test_T));
  rtmSetTFinal(pi_test_M, 10.0);
  pi_test_M->Timing.stepSize0 = 0.008;

  /* External mode info */
  pi_test_M->Sizes.checksums[0] = (3947167625U);
  pi_test_M->Sizes.checksums[1] = (2948654200U);
  pi_test_M->Sizes.checksums[2] = (3037622101U);
  pi_test_M->Sizes.checksums[3] = (3342727293U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    pi_test_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(pi_test_M->extModeInfo,
      &pi_test_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(pi_test_M->extModeInfo, pi_test_M->Sizes.checksums);
    rteiSetTPtr(pi_test_M->extModeInfo, rtmGetTPtr(pi_test_M));
  }

  /* block I/O */
  (void) memset(((void *) &pi_test_B), 0,
                sizeof(B_pi_test_T));

  /* states (dwork) */
  (void) memset((void *)&pi_test_DW, 0,
                sizeof(DW_pi_test_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    pi_test_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 14;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.BTransTable = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.PTransTable = &rtPTransTable;
  }

  /* Start for S-Function (alsa_audio_capture_sfcn): '<Root>/ALSA Audio Capture' */
  audioCaptureInit(pi_test_ConstP.pooled3, 16000U, 0.5, 128U);

  /* Start for S-Function (alsa_audio_playback_sfcn): '<Root>/ALSA Audio Playback' */
  audioPlaybackInit(pi_test_ConstP.pooled3, 16000U, 0.5, 128U);
}

/* Model terminate function */
void pi_test_terminate(void)
{
  /* Terminate for S-Function (alsa_audio_capture_sfcn): '<Root>/ALSA Audio Capture' */
  audioCaptureTerminate(pi_test_ConstP.pooled3);

  /* Terminate for S-Function (alsa_audio_playback_sfcn): '<Root>/ALSA Audio Playback' */
  audioPlaybackTerminate(pi_test_ConstP.pooled3);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
