function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <Root>/ALSA Audio Capture */
	this.urlHashMap["pi_test:1"] = "pi_test.c:40,178,188&pi_test.h:86,104&pi_test_data.c:37";
	/* <Root>/ALSA Audio Playback */
	this.urlHashMap["pi_test:2"] = "pi_test.c:87,181,191&pi_test.h:105&pi_test_data.c:38";
	/* <Root>/Data Type Conversion */
	this.urlHashMap["pi_test:3"] = "pi_test.c:43,48";
	/* <Root>/Data Type Conversion1 */
	this.urlHashMap["pi_test:4"] = "pi_test.c:71,85&pi_test.h:85";
	/* <Root>/Gain */
	this.urlHashMap["pi_test:6"] = "pi_test.c:50,55&pi_test.h:83,113&pi_test_data.c:26";
	/* <Root>/Gain1 */
	this.urlHashMap["pi_test:7"] = "pi_test.c:61&pi_test.h:116&pi_test_data.c:29";
	/* <Root>/Matrix
Concatenate */
	this.urlHashMap["pi_test:10"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=pi_test:10";
	/* <Root>/Multiport
Selector */
	this.urlHashMap["pi_test:9"] = "pi_test.c:57&pi_test.h:84";
	/* <Root>/To Workspace */
	this.urlHashMap["pi_test:5"] = "pi_test.h:93";
	/* <Root>/To Workspace1 */
	this.urlHashMap["pi_test:8"] = "pi_test.h:97";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "pi_test"};
	this.sidHashMap["pi_test"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<Root>/ALSA Audio Capture"] = {sid: "pi_test:1"};
	this.sidHashMap["pi_test:1"] = {rtwname: "<Root>/ALSA Audio Capture"};
	this.rtwnameHashMap["<Root>/ALSA Audio Playback"] = {sid: "pi_test:2"};
	this.sidHashMap["pi_test:2"] = {rtwname: "<Root>/ALSA Audio Playback"};
	this.rtwnameHashMap["<Root>/Data Type Conversion"] = {sid: "pi_test:3"};
	this.sidHashMap["pi_test:3"] = {rtwname: "<Root>/Data Type Conversion"};
	this.rtwnameHashMap["<Root>/Data Type Conversion1"] = {sid: "pi_test:4"};
	this.sidHashMap["pi_test:4"] = {rtwname: "<Root>/Data Type Conversion1"};
	this.rtwnameHashMap["<Root>/Gain"] = {sid: "pi_test:6"};
	this.sidHashMap["pi_test:6"] = {rtwname: "<Root>/Gain"};
	this.rtwnameHashMap["<Root>/Gain1"] = {sid: "pi_test:7"};
	this.sidHashMap["pi_test:7"] = {rtwname: "<Root>/Gain1"};
	this.rtwnameHashMap["<Root>/Matrix Concatenate"] = {sid: "pi_test:10"};
	this.sidHashMap["pi_test:10"] = {rtwname: "<Root>/Matrix Concatenate"};
	this.rtwnameHashMap["<Root>/Multiport Selector"] = {sid: "pi_test:9"};
	this.sidHashMap["pi_test:9"] = {rtwname: "<Root>/Multiport Selector"};
	this.rtwnameHashMap["<Root>/To Workspace"] = {sid: "pi_test:5"};
	this.sidHashMap["pi_test:5"] = {rtwname: "<Root>/To Workspace"};
	this.rtwnameHashMap["<Root>/To Workspace1"] = {sid: "pi_test:8"};
	this.sidHashMap["pi_test:8"] = {rtwname: "<Root>/To Workspace1"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
