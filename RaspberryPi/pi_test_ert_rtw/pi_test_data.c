/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: pi_test_data.c
 *
 * Code generated for Simulink model 'pi_test'.
 *
 * Model version                  : 1.4
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:06:34 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "pi_test.h"
#include "pi_test_private.h"

/* Block parameters (auto storage) */
P_pi_test_T pi_test_P = {
  0.0030518509475997192,               /* Expression: 100.0/32767
                                        * Referenced by: '<Root>/Gain'
                                        */
  327.67                               /* Expression: 32767/100
                                        * Referenced by: '<Root>/Gain1'
                                        */
};

/* Constant parameters (auto storage) */
const ConstP_pi_test_T pi_test_ConstP = {
  /* Pooled Parameter (Expression: device)
   * Referenced by:
   *   '<Root>/ALSA Audio Capture'
   *   '<Root>/ALSA Audio Playback'
   */
  { 112U, 108U, 117U, 103U, 104U, 119U, 58U, 67U, 79U, 68U, 69U, 67U, 44U, 48U,
    0U }
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
