/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: pi_test.h
 *
 * Code generated for Simulink model 'pi_test'.
 *
 * Model version                  : 1.4
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:06:34 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_pi_test_h_
#define RTW_HEADER_pi_test_h_
#include <math.h>
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef pi_test_COMMON_INCLUDES_
# define pi_test_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "dt_info.h"
#include "ext_work.h"
#include "alsa_audio_capture_macro.h"
#include "alsa_audio_playback_macro.h"
#endif                                 /* pi_test_COMMON_INCLUDES_ */

#include "pi_test_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block signals (auto storage) */
typedef struct {
  real_T Gain[256];                    /* '<Root>/Gain' */
  real_T MultiportSelector_o1[128];    /* '<Root>/Multiport Selector' */
  int16_T DataTypeConversion1[256];    /* '<Root>/Data Type Conversion1' */
  int16_T ALSAAudioCapture[256];       /* '<Root>/ALSA Audio Capture' */
} B_pi_test_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  struct {
    void *LoggedData;
  } ToWorkspace_PWORK;                 /* '<Root>/To Workspace' */

  struct {
    void *LoggedData;
  } ToWorkspace1_PWORK;                /* '<Root>/To Workspace1' */
} DW_pi_test_T;

/* Constant parameters (auto storage) */
typedef struct {
  /* Pooled Parameter (Expression: device)
   * Referenced by:
   *   '<Root>/ALSA Audio Capture'
   *   '<Root>/ALSA Audio Playback'
   */
  uint8_T pooled3[15];
} ConstP_pi_test_T;

/* Parameters (auto storage) */
struct P_pi_test_T_ {
  real_T Gain_Gain;                    /* Expression: 100.0/32767
                                        * Referenced by: '<Root>/Gain'
                                        */
  real_T Gain1_Gain;                   /* Expression: 32767/100
                                        * Referenced by: '<Root>/Gain1'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_pi_test_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_pi_test_T pi_test_P;

/* Block signals (auto storage) */
extern B_pi_test_T pi_test_B;

/* Block states (auto storage) */
extern DW_pi_test_T pi_test_DW;

/* Constant parameters (auto storage) */
extern const ConstP_pi_test_T pi_test_ConstP;

/* Model entry point functions */
extern void pi_test_initialize(void);
extern void pi_test_step(void);
extern void pi_test_terminate(void);

/* Real-time Model object */
extern RT_MODEL_pi_test_T *const pi_test_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'pi_test'
 */
#endif                                 /* RTW_HEADER_pi_test_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
