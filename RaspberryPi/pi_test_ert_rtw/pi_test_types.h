/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: pi_test_types.h
 *
 * Code generated for Simulink model 'pi_test'.
 *
 * Model version                  : 1.4
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Sep 11 14:06:34 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_pi_test_types_h_
#define RTW_HEADER_pi_test_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_pi_test_T_ P_pi_test_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_pi_test_T RT_MODEL_pi_test_T;

#endif                                 /* RTW_HEADER_pi_test_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
