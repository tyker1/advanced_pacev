%test_synthese

L = length(elektrode);

sig = zeros(1,L*SamplePerFrame);
channels = zeros(M,L*SamplePerFrame);
fs = Samplefrequence;
% tl = 0;
% tr = 0;
tl = zeros(1,M);
tr = zeros(1,M);
t = zeros(M,SamplePerFrame);
for i = 1:L
    sig_sin =zeros(1,SamplePerFrame);
%     tr = tl+SamplePerFrame*(1/fs);
%     t = linspace(tl,tr,SamplePerFrame);
    e_idx = unique(elektrode_index(i,:));
    for j = 1:length(e_idx)
        tr(e_idx(j)) = tl(e_idx(j)) + SamplePerFrame/fs;
        t(e_idx(j),:) = linspace(tl(e_idx(j)),tr(e_idx(j)),SamplePerFrame);
        tl(e_idx(j)) = tr(e_idx(j)) + 1/fs;
    end
    for j = 1:N
        sig_sin = sig_sin + elektrode(i,j)*sin(MidFreq(elektrode_index(i,j))*2*pi*t(elektrode_index(i,j),:));
        channels(elektrode_index(i,j),(i-1)*SamplePerFrame+1:i*SamplePerFrame) = ...
            0.5*sin(MidFreq(elektrode_index(i,j))*2*pi*t(elektrode_index(i,j),:));
    end
%     tl = tr + 1/fs;
    sig((i-1)*SamplePerFrame+1:i*SamplePerFrame) = sig_sin;
end