function [ fMat,rMat ] = waveNodeMat( cord, N, Lo_D,Hi_D, Lo_R,Hi_R, dwtMode )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here


stack = [];
endDepth = cord(1);
endIdx = cord(2);
total = 1;
stack(total,:) = [endDepth, endIdx];
total = total + 1;
while (endDepth > 0)
    endDepth = endDepth - 1;
    endIdx = floor(endIdx / 2);
    stack(total,:) = [endDepth,endIdx];
    total = total + 1;
end

total = total - 1;
lN = N;
fMat = [];
rMat = [];

for i = total-1:-1:1
    ord = stack(i,2);
    [aMat,dMat] = waveletFilterMatrix(lN,Lo_D,Hi_D,fMat, dwtMode);
    lN1 = size(aMat,1);
    [aMatI,dMatI] = waveletRecMatrix(lN1,Lo_R,Hi_R,rMat,lN, dwtMode);
    lN = lN1;
    
    if (mod(ord,2) == 0)
        fMat = aMat;
        rMat = aMatI;
    else
        fMat = dMat;
        rMat = dMatI;
    end
end

end

