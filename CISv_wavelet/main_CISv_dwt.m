%% Init
clear all;

init_cisv;
%% audio Einlesen
[audiodatei,Fa] = audioread('../AudioSample/spfg_16kHz.wav');
TotalFrame = floor(length(audiodatei)/frameSize);
%% Schleife
% Filterbank
% envelope
% Impulse 

% Matrix f�r Einh�llendeinformationen jedes Frames (f�r Einh�llende Synthese)
Log_Envelop = zeros(TotalFrame*PulsePerFrame,NumChannel);
% Matrix f�r Impulsfolgeinformationen jedes Frames (f�r Impulse Synthese)
Log_Imp = zeros(TotalFrame*NumChannel,NumChannel);
Log_Pitch = zeros(TotalFrame,NumChannel);
WinFunc = hann(OverlapLen+frameSize);
for i = 1:TotalFrame
    start = (i-1)*frameSize + 1;
    stop  = start + frameSize - 1;
    audioSeg = audiodatei(start:stop);
    %buffedData = Overlap(audioSeg, OverlapLen, frameSize).*WinFunc';
    buffedData = Overlap(audioSeg, OverlapLen, frameSize);
    % Aufruf der Filterbank Funktion
    %filterErg = filterbank(buffedData,NumChannel,KoefB,KoefA,frameSize, OverlapLen);
    filterErg = WaveletFilterbank( buffedData, NumChannel, waveletMat, OverlapLen + frameSize, NodeLen );
    %Pitch Locator f�r Virtuelle Kan�len
    Log_Pitch(i,:) = PitchLocator(filterErg, NumChannel, FreqMap, frameSize+OverlapLen, numVirtCh, fs);
    % Aufruf der Einh�llendeextraktion Funktion
    envelopeErg = envelope(filterErg, NumChannel, frameSize, OverlapLen);
    for j = 1:PulsePerFrame        
        Log_Envelop((i-1)*PulsePerFrame+j,:) = sampleEnvelope(envelopeErg, NumChannel, frameSize, OverlapLen, SampleSize, StimulationRate, j, fs);
    end
    % Impulsfolgegenerator
    %impulse = pulseModulation(envelopeErg, pulse, numChannel);
    %Log_Imp(((i-1)*numChannel+1):i*numChannel,:) = impulse;
end

%FrequenzMapping
Log_Envelop_Virt =  zeros(TotalFrame*PulsePerFrame,NumChannel* numVirtCh);
j = 1;
for i = 1:TotalFrame*PulsePerFrame
    if (mod(i,PulsePerFrame) == 0)
        j = j + 1;
    end
    Log_Envelop_Virt(i,:) = MapFrequencies( Log_Envelop(i,:), Log_Pitch(j,:), NumChannel , numVirtCh);
end

%% Synthese

Sig = synthese_Envlope(Log_Envelop_Virt, StimulationRate, FreqMap, NumChannel*numVirtCh, fs);