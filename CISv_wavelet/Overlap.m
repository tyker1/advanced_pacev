function [ OverlappedFrame ] = Overlap( Frame, OverlapLen, FrameLen )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

persistent Buffer;

if isempty(Buffer)
    Buffer = zeros(1,OverlapLen+FrameLen);
end

Buffer(OverlapLen+1:end) = Frame;
OverlappedFrame = Buffer;

Buffer(1:OverlapLen) = Buffer(end-OverlapLen+1:end);

end

