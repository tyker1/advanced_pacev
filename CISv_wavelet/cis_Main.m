%% Init
init_cis;
%% audio Einlesen
[audiodatei,Fa] = audioread('../AudioSample/spfg_16kHz.wav');
TotalFrame = floor(length(audiodatei)/frameSize);
%% Schleife
% Filterbank
% envelope
% Impulse 

% Matrix für Einhüllendeinformationen jedes Frames (für Einhüllende Synthese)
Log_Envelop = zeros(TotalFrame*PulsePerFrame,NumChannel);
% Matrix für Impulsfolgeinformationen jedes Frames (für Impulse Synthese)
Log_Imp = zeros(TotalFrame*NumChannel,NumChannel);

for i = 1:TotalFrame
    start = (i-1)*frameSize + 1;
    stop  = start + frameSize - 1;
    audioSeg = audiodatei(start:stop);
    buffedData = Overlap(audioSeg, OverlapLen, frameSize);
    % Aufruf der Filterbank Funktion
    filterErg = filterbank(buffedData,NumChannel,KoefB,KoefA,frameSize, OverlapLen);
    % Aufruf der Einhüllendeextraktion Funktion
    envelopeErg = envelope(filterErg, NumChannel, frameSize, OverlapLen);
    for j = 1:PulsePerFrame
        
        Log_Envelop((i-1)*PulsePerFrame+j,:) = sampleEnvelope(envelopeErg, NumChannel, frameSize, OverlapLen, SampleSize, StimulationRate, j, fs);
    end
    % Impulsfolgegenerator
    %impulse = pulseModulation(envelopeErg, pulse, numChannel);
    %Log_Imp(((i-1)*numChannel+1):i*numChannel,:) = impulse;
end

%% Synthese

Sig = synthese_Envlope(Log_Envelop, StimulationRate, MidFreq, NumChannel, fs);