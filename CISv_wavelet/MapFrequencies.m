function [ MapErg ] = MapFrequencies( sampleErg, PitchLog, numChannel , numVirtCh)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

MapErg = zeros(1,numVirtCh*numChannel);

for i = 1:numChannel
    idx = (i-1)*numVirtCh +  PitchLog(i);
    MapErg(idx) = sampleErg(i);
end

end

