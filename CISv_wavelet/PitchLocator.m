function [ PitchErg ] =PitchLocator(filterErg, NumChannel, FreqMap, N, numVirtCh ,fs)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


PitchErg = zeros(NumChannel,1);

Df = fs / N;

for i = 1:NumChannel

    Flow = FreqMap((i-1)*numVirtCh+1);
    Fhigh = FreqMap(i*numVirtCh);
    DesiredRes = FreqMap((i-1)*numVirtCh+1+1)-FreqMap((i-1)*numVirtCh+1);
    Factor = ceil(Df/DesiredRes);
    FFTLen = N * Factor;
    fftErg = abs(fft(filterErg(:,i), FFTLen));
    newRes = fs/FFTLen;
    
    startBin = round(Flow/newRes);
    endBin = ceil(Fhigh/newRes);
    [mPitch, idx] = max(fftErg(startBin:endBin));
    idx = idx + startBin - 1;
    maxFreq = idx * newRes;

        FreqVect = FreqMap((i-1)*numVirtCh+1:i*numVirtCh);
        [minDis, minIdx] = min(abs(FreqVect-maxFreq));
        PitchErg(i) = minIdx;

end


end

