function [ sampleErg ] = sampleWaveletEnvelop( envelopeErg, NumChannel, frameSize, OverlapLen, SampleSize, StimulationRate, j, fs )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

sampleErg = zeros(1,NumChannel);

Ratio = StimulationRate/ (fs/frameSize); % sample per Frame

for i = 1:NumChannel
    channel = envelopeErg{i};
    channel = channel / length(channel);
    ChLen = length(channel);
    SamplesLen = ChLen / Ratio;
    if (SamplesLen >= 1)
        Seg = channel((j-1)*SamplesLen+1:j*SamplesLen);
        sampleErg(i) = sqrt(sum(Seg.^2));
    else
        sampleErg(i) = sqrt(sum(channel.^2*SamplesLen));
    end
    
end


end

