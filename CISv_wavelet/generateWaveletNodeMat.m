function [ WaveletNodeMat , MidFreq] = generateWaveletNodeMat(filterVector, decompLevel, fs, numChannel)
%UNTITLED11 Summary of this function goes here
%   Detailed explanation goes here


WaveletNodeMat = cell(numChannel, 2);
MidFreq = zeros(1,numChannel);

Scale = fs/2 / (2^decompLevel);
testVect = zeros(numChannel,2);
for i = 1:numChannel
    left = round(filterVector(i,1)/Scale);
    right=round(filterVector(i,2)/Scale)-1;
    testVect(i,:) = [left,right];
    nodes = zeros(right-left+1,2);
    nodes(:,1) = left:right;
    nodes(:,2) = ones(right-left+1,1)*decompLevel;
    MidFreq(i) = (left+right+1)/2*Scale;
    
    while ((size(nodes,1)) >= 2)
        len = size(nodes,1);
        tempNode = [];
        j = 1;
        idx = 1;
        %iterative try to combine bins
        while j <= len
            flag = 0;
            if (mod(nodes(j,1),2)  == 0)
                if ((j+1 <= len) && (nodes(j,2) == nodes(j+1,2)) && (nodes(j,1)+1 == nodes(j+1,1)))
                    tempNode(idx,:) = [nodes(j,1)/2,nodes(j,2)-1];
                    j = j + 1;
                    flag = 1;
                end
            end
            if (flag == 0)
                tempNode(idx,:) = nodes(j,:);
            end
            idx = idx + 1;
            j = j + 1;
        end
        nodes = tempNode;
        
        %if idx is equals to len, theres no nodes to be combined, therefore loop
        %should be ended
        if (idx >= len)
            break;
        end
    end
    WaveletNodeMat(i,:) = {size(nodes,1),nodes};
end

%make all node to same depth

for i = 1:numChannel
    numNode = WaveletNodeMat{i,1};
    NodeVect = WaveletNodeMat{i,2};
    aimDepth = max(NodeVect(:,2));
    minDepth = min(NodeVect(:,2));
    if (aimDepth == minDepth)
        continue;
    end
    
    newVect = [];
    for j = 1:numNode
        ord = NodeVect(j,1);
        depth = NodeVect(j,2);
        temp = splitNode(aimDepth, depth, ord);
        newVect = [newVect;temp];
    end
    numNode = size(newVect,1);
    WaveletNodeMat(i,:) = {numNode, newVect};
end

end

