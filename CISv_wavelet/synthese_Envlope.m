function [ sig ] = synthese_Envlope( envelope, StimulationRate, MidFreq, numChannel , Samplefrequence)
%synthese_ace: Synthese Funktion f�r ACE-Strategie
%   Eingabe Parameter:
%   envelope   -- Elektrodeaktivit�ten von gesamter Simulation (eine L*M
%                 Matrix), L sind die insgamste Frames und M ist die Anzahl
%                 den verf�gbaren Kan�len.
%   SamplePerFrame -- SamplePerFrame entspricht die Anzahl der Samples pro
%                     Frame definiert im initcis.m, dient dazu ein Frame zu 
%                     rekonstruieren
%   MidFreq      -- MidFreq ist ein Vektor f�r Mittelfrequenzen der
%                   Kan�len, die werden automatisch erzeugt beim jedem
%                   Simmulationsvorgang durch initcis.m, bitte direkt die
%                   Variable MidFreq �bergeben
%   numChannel   -- Anzahl der insgesamte verf�gbaren Elektrode.
%   Samplefrequence -- Abtastfrequenz von Audiosignal, entspricht die
%                      Einstellung in initcis.m

    % Ermitteln von Anzahl der gesamten Frames
    L = length(envelope);
    % Initialisierung f�r Synthesefunktion, sig ist eine Variable f�r
    % synthetisiertes Signal
    SamplePerFrame = Samplefrequence / StimulationRate;
    
    sig = zeros(1,L*SamplePerFrame);
    fs = Samplefrequence;
    % tl ist eine Register f�r die Endzeitpunkte der Sinussignale, mit tl
    % wird es gesichert dass f�r die Synthese eine konstante Phasen von
    % Sinussignale genutzt werden.
    tl = zeros(1,numChannel);

    for i = 1:L
        % sig_sin f�r Synthese des akutelles Frames
        sig_sin =zeros(1,SamplePerFrame);
        for j = 1:numChannel
            % Amplitude von Kanal j aus akutellem Frame
            amp1 = envelope(i,j);
            % Amplitude von Kanal j aus n�chstem Frame ermitteln, wenn
            % akutelles Frame das letzte Frame ist, die Amplitude von
            % n�chstem Frame als Null angenommen.
            if (i == L)
                amp2 = 0;
            else
                amp2 = envelope(i+1,j);
            end
            % Falls dieser Kanal aktiviert (f�r CIS sind sie immer
            % akitivert) sollte eine Amplitudeinterpolation ausgef�hrt
            % werden
            if (amp1 ~= 0) || (amp2 ~= 0)
                % Berechnen des Endzeitpunktes dieses Frames
                tr = tl(j) + SamplePerFrame/fs;
                % Erzeugung der Zeitvektor f�r Sinussignal
                t = linspace(tl(j),tr,SamplePerFrame);
                % Abspeichern des Endzeitpunktes
                tl(j) = tr + 1/fs;
                % Amplitudeinterpolation
                temp_amp = amp_interp(amp1,amp2,SamplePerFrame);
                % Sinusoidale Signal mit interpolierter Amplitude
                % modulieren und synthetisieren
                sig_sin = sig_sin + temp_amp.* sin(2*pi*MidFreq(j).*t);
            end
        end
        % abspeichern des synthetisierten Frames
        sig((i-1)*SamplePerFrame+1:i*SamplePerFrame) = sig_sin;
    end
end

function vect_amp = amp_interp(a1,a2,amp_N)
% Lineare Amplitudeinterpolation
    n = 0:amp_N-1;

    delta = (a2-a1) / amp_N;

    vect_amp = a1 + n.*delta;

end