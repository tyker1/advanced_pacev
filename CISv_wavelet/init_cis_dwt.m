global filterVect;
global fs;
global frameSize;
global StimulationRate;
global MidFreq;
global KoefA;
global KoefB;
global filterOrd;
global NumChannel;
global OverlapLen;
global SampleSize;
global PulsePerFrame;
global DecompositionLevel;

global Lo_D; %tiefpassfilter von wavelet (Approximation)
global Hi_D; %hochpassfilter von wavelet (Detail)
global Lo_R; %rekonstruktionsfilter f�r Approximation von wavelet
global Hi_R; %rekonstruktionsfilter f�r Detail von wavelet
global waveletMat;
global NodeLen;

CISdwtMode = 'sym';
waveletTyp = 'db3';
[Lo_D,Hi_D,Lo_R,Hi_R] = wfilters(waveletTyp);
DecompositionLevel = 6;
NumChannel = 15;
filterOrd = 6;
fs = 16000;
StimulationRate = 500;
PulsePerFrame = 4;
frameSize = fs/StimulationRate * PulsePerFrame;
OverlapLen = frameSize; % 50% overlap
SampleSize = 4;
KoefA = zeros(filterOrd+1, NumChannel);
KoefB = zeros(filterOrd+1, NumChannel);

filterVect = [250,500
                      500,750
                      750,1000
                      1000,1250
                      1250,1500
                      1500,1750
                      1750,2050
                      2050,2400
                      2400,2825
                      2825,3312
                      3312,3812
                      3812,4375
                      4375,5000
                      5000,6500
                      6500,8000];
 filterLow = filterVect(:,1);
 filterHigh = filterVect(:,2);
 
[WaveletNodeCell, MidFreq] = generateWaveletNodeMat(filterVect, DecompositionLevel, fs, NumChannel);

NodeLen = cell2mat(WaveletNodeCell(:,1));

waveletMat = cell(NumChannel, max(NodeLen),2);

for i = 1:NumChannel
    numNode = WaveletNodeCell{i,1};
    NodeVect = WaveletNodeCell{i,2};

    for j = 1:numNode
        ord = NodeVect(j,1);
        dep = NodeVect(j,2);
        %%TODO
        %waveNodeMat may have bugs
        [fMat,rMat] = waveNodeMat([dep, ord],frameSize+OverlapLen,Lo_D,Hi_D,Lo_R,Hi_R, CISdwtMode);
        waveletMat(i,j,:) = {fMat,rMat};
    end
end