function [ MatLo_R, MatHi_R ] = waveletRecMatrix( N,Lo_R,Hi_R, Mat, lN, dwtMode )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

perFlag = 0;
if dwtMode == 'per'
     perFlag = 1;
end

uMat = generateUpMat(N, perFlag);

if perFlag
    LenX = size(uMat,1);
    lf = length(Lo_R);
    extMat = generateExtMat(LenX,lf/2,'per');
    uMat = extMat * uMat;
end

LenX = size(uMat,1);
iMatA = filt2Matrix(LenX,Lo_R,'full');
iMatD = filt2Matrix(LenX,Hi_R,'full');
kMat = generateKeepMat(N,Lo_R, lN,perFlag);
MatLo_R = kMat * iMatA * uMat;
MatHi_R = kMat * iMatD * uMat;

if ~isempty(Mat)
    MatLo_R = Mat * MatLo_R;
    MatHi_R = Mat * MatHi_R;
end

end

