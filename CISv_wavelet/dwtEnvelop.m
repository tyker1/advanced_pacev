function [ envErg ] = dwtEnvelop( filterErg, numChannel)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

envErg = cell(numChannel,1);

for i = 1:numChannel
    envErg{i}  = abs(filterErg{i});
end

end

