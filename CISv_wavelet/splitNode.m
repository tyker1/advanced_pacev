function [ NodeVect ] = splitNode( aimDepth, depth, ord )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

NodeVect = [];

if (aimDepth < depth)
    return;
else if (aimDepth == depth)
            NodeVect = [ord, depth];
            return;
    end
end

if ((aimDepth-depth) == 1)
    NodeVect = [ord*2,depth+1;ord*2+1,depth+1];
    return;
else
    NodeVect = [NodeVect;splitNode(aimDepth,depth+1,ord*2)];
    NodeVect = [NodeVect;splitNode(aimDepth,depth+1,ord*2+1)];
end

end