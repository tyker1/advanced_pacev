function [ WPCoef ] = generateWaveletPacketTree( depth, WinSize, Lo_D, Hi_D, Lo_R, Hi_R, CISdwtMode )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

WPCoef = cell(depth+1, 2^depth,2);

for i = 1:depth
    numNode = 2^(i);
    for j = 1:numNode
        [fMat,rMat] = waveNodeMat([i,j-1], WinSize, Lo_D, Hi_D, Lo_R, Hi_R, CISdwtMode);
        WPCoef(i+1,j,:) = {fMat,rMat};
    end
end


end

