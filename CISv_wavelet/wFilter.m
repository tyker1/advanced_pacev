function [ filtErg ] = wFilter( frame, NumChannel, waveletMat, FrameSize, NodeLen )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

filtErg = cell(NumChannel,1);

for i = 1:NumChannel
    filtSig = [];
    for j = 1:NodeLen(i)
        temp = waveletMat{i,j,1}*frame';
        if (isempty(filtSig))
            filtSig = temp;
        else
            filtSig = filtSig + temp;
        end
    end
    
    filtErg{i} = filtSig;
    
end

end

