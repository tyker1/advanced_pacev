%% Wavelet Filterbank Belegung
% Fn = 22050Hz
% ------------------------------------------------------------------------------------------
% Band Nr.      Start Freq	End Freq	Mid Freq    Bandwidth   Nodes
% ------------------------------------------------------------------------------------------
%         1		 13781		 16538		 15159		  2756		(3,5)
%         2		 11025		 13781		 12403		  2756		(3,4)
%         3		  9647		 11025		 10336		  1378		(4,7)
%         4		  8269		  9647		  8958		  1378		(4,6)
%         5		  6891		  8269		  7580		  1378		(4,5)
%         6		  5512		  6891		  6202		  1378		(4,4)
%         7		  4479		  5512		  4996		  1034		(5,7) (6,13)
%         8		  3790		  4479		  4134		   689		(6,11) (6,12)
%         9		  3101		  3790		  3445		   689		(6,9) (6,10)
%        10		  2756		  3101		  2929		   345		(6,8)
%        11		  2412		  2756		  2584		   345		(6,7)
%        12		  2067		  2412		  2239		   345		(6,6)
%        13		  1723		  2067		  1895		   345		(6,5)
%        14		  1464		  1723		  1593		   258		(7,9) (8,17)
%        15		  1292		  1464		  1378		   172		(8,15) (8,16)
%        16		  1120		  1292		  1206		   172		(8,13) (8,14)
%        17		   775		   947		   861		   172		(8,9) (8,10)
%        18		   646		   775		   711		   129		(8,8) (9,15)
%        19		   517		   646		   581		   129		(8,6) (9,14)
%        20		   409		   517		   463		   108		(10,19) (8,5)
%        21		   301		   409		   355		   108		(9,7) (9,8) (10,18)
%        22		   215		   301		   258		    86		(9,5) (9,6)
%        23		   108		   194		   151		    86		(9,3) (10,5) (10,8)
%        24		    43		   108		    75		    65		(9,1) (10,4)

%% Initialisierung
global MidF;
global NodeLen;
global WaveletNodes;
global SampleFrequency;
global SamplePerFrame;
global numElectrode;
global N;
global lowA;
global lowB;
global WinFunc;

%%%%%%%%%%%%%%%%%%%%%%
% Wavelet Parameter  %
%%%%%%%%%%%%%%%%%%%%%%
global DWTDepth; %Dekomposationsstufe
global Lo_D; %tiefpassfilter von wavelet (Approximation)
global Hi_D; %hochpassfilter von wavelet (Detail)
global Lo_R; %rekonstruktionsfilter f�r Approximation von wavelet
global Hi_R; %rekonstruktionsfilter f�r Detail von wavelet
global waveletMat;

waveletTyp = 'sym7';
DWTDepth = 10;
[Lo_D,Hi_D,Lo_R,Hi_R] = wfilters(waveletTyp);

SampleFrequency = 44100;
SamplePerFrame = 128;
WindowSize = 1024;
WinFunc = hann(SamplePerFrame);
numElectrode = 24;
N = 6;

fc_LP = 300;
[lowB,lowA] = butter(3, fc_LP/(SampleFrequency/2));

%Mittelfrequenz des jeweiligen Bandes
MidF = [
        15159
        12403
        10336
        8958
        7580
        6202
        4996
        4134
        3445
        2929
        2584
        2239
        1895
        1593
        1378
        1206
        861
        711
        581
        463
        355
        258
        151
        75
       ];
   
% Anzahl der Knoten f�r jeweiligen Band
NodeLen = [
            1
            1
            1
            1
            1
            1
            2
            2
            2
            1
            1
            1
            1
            2
            2
            2
            2
            2
            2
            2
            3
            2
            3
            2
            ];
        
% WaveletNodes : Inhalt von WaveletPaketTree Knoten f�r entsprechenden Band
WaveletNodes = [
                3, 5, 0, 0, 0, 0
                3, 4, 0, 0, 0, 0
                4, 7, 0, 0, 0, 0
                4, 6, 0, 0, 0, 0
                4, 5, 0, 0, 0, 0
                4, 4, 0, 0, 0, 0
                5, 7, 6, 13, 0, 0
                6, 11, 6, 12, 0, 0
                6, 9, 6, 10, 0, 0
                6, 8, 0, 0, 0, 0
                6, 7, 0, 0, 0, 0
                6, 6, 0, 0, 0, 0
                6, 5, 0, 0, 0, 0
                7, 9, 8, 17, 0, 0
                8, 15, 8, 16, 0, 0
                8, 13, 8, 14, 0, 0
                8, 9, 8, 10, 0, 0
                8, 8, 9, 15, 0, 0
                8, 6, 9, 14, 0, 0
                10, 19, 8, 5, 0, 0
                9, 7, 9, 8, 10, 18
                9, 5, 9, 6, 0, 0
                9, 3, 10, 5, 10, 8
                9, 1, 10, 4, 0, 0
                ];
            
waveletMat = cell(numElectrode,max(NodeLen),2);

for i = 1:numElectrode
    numNode = NodeLen(i);
    for j = 1:numNode
        dep = WaveletNodes(i,(j-1)*2+1);
        ord = WaveletNodes(i,j*2);
        [fMat,rMat] = waveNodeMat([dep,ord],SamplePerFrame,Lo_D,Hi_D,Lo_R,Hi_R);
        waveletMat(i,j,:) = {fMat,rMat};
    end
end