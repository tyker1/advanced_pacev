function [ filtSig ] = filterbank( Sig, nodes, lenNode, Lo_D,Hi_D,Lo_R,Hi_R, depth, numElektrode, SamplePerFrame, wTree )
%filterbank filtert das Signal mit gegebene Waveletfilterbank
%   Detailed explanation goes here

[C,L] = wavedec(Sig,depth,Lo_D,Hi_D);
filtSig = zeros(numElektrode,SamplePerFrame);

for i = 1:numElektrode
    band = zeros(SamplePerFrame,1);
    
    for j = 1:lenNode(i)
        cord = [nodes(i,(j-1)*2+1),nodes(i,j*2)];
        [coef,stack,wTree] = wtGetNode(wTree,C,L,cord,Lo_D,Hi_D,Lo_R,Hi_R);
        tempSig = wprAD(coef,cord,L,stack,Lo_R,Hi_R);
        band = band + tempSig;
    end
    filtSig(i,:) = band;
end

end

