function [ MatLo_D,MatHi_D ] = waveletFilterMatrix( N, Lo_D,Hi_D, Mat)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
Len = length(Lo_D);
extMat = generateExtMat(N,Len-1,'sym');
LenX = size(extMat,1);
MatA_D = filt2Matrix(LenX,Lo_D);
MatD_D = filt2Matrix(LenX,Hi_D);
dwnMat = generateDMat(LenX,Lo_D);

MatLo_D = dwnMat * MatA_D * extMat;
MatHi_D = dwnMat * MatD_D * extMat;

if ~isempty(Mat)
    MatLo_D = MatLo_D * Mat;
    MatHi_D = MatHi_D * Mat;
end

end

