function [ ftMat ] = filt2Matrix( N, fVect )
%FILT2MATRIX generate the Matrix for FIR Filter in order to boost
%performance
%   Detailed explanation goes here

Len = length(fVect);
% newLen = (N + 2*(Len-1)) + (Len - 1);
% LenY = newLen - Len + 1;
newLen = N + Len - 1;
LenY = N;
ftMat = zeros(newLen,LenY);

newVect = fliplr(fVect);

for i = 1:newLen
    if ( i <= LenY)
        startMat = i - Len + 1;
        if startMat < 1
            startMat = 1;
        end
        startVect = Len - i + 1;
        if startVect < 1
            startVect = 1;
        end
        ftMat(i,i:-1:startMat) = newVect(Len:-1:startVect);
    else
        ftMat(i,i-Len+1 : end) = newVect(1:end-(i-LenY));
    end
end

end

