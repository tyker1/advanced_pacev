function [ MatLo_R, MatHi_R ] = waveletRecMatrix( N,Lo_R,Hi_R, Mat, lN )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

uMat = generateUpMat(N);
LenX = size(uMat,1);
iMatA = filt2Matrix(LenX,Lo_R);
iMatD = filt2Matrix(LenX,Hi_R);
kMat = generateKeepMat(N,Lo_R, lN);
MatLo_R = kMat * iMatA * uMat;
MatHi_R = kMat * iMatD * uMat;

if ~isempty(Mat)
    MatLo_R = Mat * MatLo_R;
    MatHi_R = Mat * MatHi_R;
end

end

