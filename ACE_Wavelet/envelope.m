function [ envSound ] = envelope(filtSig, lowA, lowB,numChannel, WinFunc, useFFT)
%ENVELOPE Summary of this function goes here
%   Detailed explanation goes here

envSound = zeros(numChannel,1);

if useFFT
    for i = 1:numChannel
        chSound = filtSig(i,:);
        chSound = chSound .* WinFunc';
        fftChSound = fft(chSound);
        len = length(chSound);
        fftChSound = fftChSound(1:floor(len/2))./len*2;
        envSound(i) = sqrt(sum(real(fftChSound).^2 + imag(fftChSound).^2));
        %envSound(i) = mean(abs(hilbert(filtSig(i,:))));
    end
else
    for i = 1:numChannel
        rectSig = abs(filtSig(i,:));
        ftSig = filtfilt(lowB,lowA,rectSig);
        envSound(i) = mean(ftSig);
    end
end
end

