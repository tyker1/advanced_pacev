function [ downMatrix ] = generateDMat( N, fVect )
%GENERATEDMAT Summary of this function goes here
%   Detailed explanation goes here

Len = length(fVect);
LenY = N + Len - 1;
% LenY = (N + 2*(Len-1)) + (Len - 1);
LenX = floor((LenY - 2*(Len-1))/2);
downMatrix = zeros(LenX,LenY);
sMat = Len+1;
eMat = LenY - Len + 1;
oneVect = sMat:2:eMat;

for i = 1:LenX
    idx = oneVect(i);
    downMatrix(i,idx) = 1;
end

end

