function [ kMat ] = generateKeepMat( N,fVect, s )
%GENERATEKEEPMAT Summary of this function goes here
%   Detailed explanation goes here

Len = length(fVect);
if isempty(s)
    LenX = N*2 - Len + 2;
else
    LenX = s;
end


LenY = N*2 - 1 + Len - 1;
kMat = zeros(LenX,LenY);

first = 1+floor((LenY-LenX)/2);
last = LenY - ceil((LenY-LenX)/2);
idxVect = first:last;
for i = 1:LenX
    kMat(i,idxVect(i)) = 1;
end
end

