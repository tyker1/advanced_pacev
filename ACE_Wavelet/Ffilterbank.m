function [ filtSig ] = Ffilterbank( Frame, numChannel, waveletMat, N, lenNode )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

filtSig = zeros(numChannel,N);
for i = 1:numChannel
    sig = zeros(N,1);
    for j = 1:lenNode(i)
        fSig = waveletMat{i,j,1} * Frame;
        recSig = waveletMat{i,j,2} * fSig;
%        recSig = waveletMat{i,j} * Frame;
        sig = sig + recSig;
    end
    filtSig(i,:) = sig;
end

end

