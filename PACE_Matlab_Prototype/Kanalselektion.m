function [outAmp,outIndex] = m_sel(a, N, Labs, sl, sr, av, alpha, M)
% [sortedArray, IndexBeforeSort] = sort(a,'descend');
% 
% outAmp = sortedArray(1:N);
% outIndex = IndexBeforeSort(1:N);


outAmp = zeros(1,N);
outIndex = zeros(1,N);

Mask_a = zeros(1,N);
a_db = 10*log10(floor(a/(10^-12)));
PowerLawMask= zeros(M,1);

for i = 1:N
%    max = -65536;
%    max_idx = 1;
%    for j = 1:M
%        mask_effect = power_law(Mask_a, j, i-1, Labs, sl, sr, av, alpha);
%        delta = a_db(j) - mask_effect;
%        
%        if ((delta > max) && (isempty(find(outIndex == j, 1))))
%            max = delta;
%            max_idx = j;
%        end
%    end

   [max_wert, max_idx] = max(a_db-10*log10(PowerLawMask.^(1/alpha)));
   maske = a_db(max_idx);
   for j = 1:M
       PowerLawMask(j) = power_law(maske, j, max_idx, Labs,sl,sr,av,alpha,PowerLawMask(j));
   end
   
   Mask_a(i) = a_db(max_idx); % Einheit dB
   outAmp(i) = a(max_idx);
   outIndex(i) = max_idx;
end

end


function Lt = power_law(a, z, idx, Labs, sl, sr, av, alpha, it)
        
%         if (Len == 0)
%             Lt = 0;
%             return;
%         end
%         % I = 10^(L/10);
%         % It(zi) = [Iabs(zi)^alpha + Sum(Ii(zj)^alpha)]^(1/alpha)
%         It = (10^(Labs(z)/10)).^alpha;
%         for idx = 1:Len
%             It = It + (10.^(spread_func(a(idx),idx, z, sl, sr, av)/10)).^alpha;
%         end
%         
%         It = It.^(1/alpha);
%         Lt = 10*log10(It);

          Lt = it;
          if (it == 0)
              Lt = (10^(Labs(z)/10)).^alpha;
          end
          
          Lt = Lt + (10.^(spread_func(a,idx, z, sl, sr, av)/10)).^alpha;
end
    
function Li = spread_func(a, i, z, sl, sr, av)
    if (i > z)
        Li = a - av - sl*(i-z);
    else
        Li = a - av - sr*(z-i);
    end
    if (Li < 0)
        Li = 0;
    end
end


% function [outAmp,outIndex] = m_sel(a, N, Labs, sl, sr, av, alpha, M)
% % [sortedArray, IndexBeforeSort] = sort(a,'descend');
% % 
% % outAmp = sortedArray(1:N);
% % outIndex = IndexBeforeSort(1:N);
% 
% outAmp = zeros(1,N);
% outIndex = zeros(1,N);
% 
% Mask_a = zeros(1,N);
% for i = 1:N
%    max = -1;
%    max_idx = 0;
%    for j = 1:M
%        mask_effect = power_law(Mask_a, j, i-1, Labs, sl, sr, av, alpha);
%        delta = a(j) - mask_effect;
%        
%        if ((delta > max) && (isempty(find(outIndex == j, 1))))
%            max = delta;
%            max_idx = j;
%        end
%    end
%    
%    Mask_a(i) = 10*log10(a(max_idx)); % Einheit dB
%    outAmp(i) = a(max_idx);
%    outIndex(i) = max_idx;
% end
% 
% end
% 
% function Lt = power_law(a, z, Len, Labs, sl, sr, av, alpha)
%         
%         if (Len == 0)
%             Lt = 0;
%             return;
%         end
%         % I = 10^(L/10);
%         % Lt(zi) = [Iabs(zi)^alpha + Sum(Ii(zj)^alpha)]^(1/alpha)
%         Lt = (10^(Labs(z)/10)).^alpha;
%         for idx = 1:Len
%             Lt = Lt + (10.^(spread_func(a(idx),idx, z, sl, sr, av)/10)).^alpha;
%         end
%         
%         Lt = Lt.^(1/alpha);
% end
%     
% function Li = spread_func(a, i, z, sl, sr, av)
%     if (i > z)
%         Li = a - av - sl*(i-z);
%     else
%         Li = a - av - sr*(z-i);
%     end
% end