function fftres = fftFilter(signal,WindowSize, WindowFunc)
%% Berechnet FFT von signal, unter Verwendung von Fensterfunktion
% (WindowFunc), WindowSize entspricht der Fenstergröße
% Diese Erzeugt eine FIR Filterbank mit Anzahl der Filters identisch zu
% WindowSize

windowedSig = signal .* WindowFunc; %Anwendung der Fensterfunktion
spectrum = fft(windowedSig)./WindowSize*2; % FFT Berechnen und Amplituden korigieren
r_sqr = imag(spectrum).^2 + real(spectrum).^2; %Power Sum bilden

fftres = r_sqr;

end