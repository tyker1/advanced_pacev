function a = FFTEnvelope(r_sqr,FilterKoeff,M)

a = zeros(M,1);

for i = 1:M
    % a ist die "Power-sum combination of bins"
    a(i) = sqrt(r_sqr*FilterKoeff(:,i));
end