%Initialisierungsfunktion f��r ACE-Strategie
%% Erlkaerung fuer globalen Variable
global WindowSize;
global Samplefrequence;
global SamplePerFrame;
global M;
global N;
global WindowFunc;
global FilterKoeff;
global FilterGain;
global Labs;
global av;
global sl;
global sr;
global alpha;
global AudioData;

%% Allgemeine Initialisierungen
[Data, Samplefrequence] = audioread(Datapath);
AudioData = Data(1:(Samplefrequence*SimDuration));

%% Allgemeine Initialisierungen
WindowSize = 256; %FFT-Fenstergrosse definiert nach standart ACE
Samplefrequence = 16000; % Standart ACE arbeitet mit dem Abtastfrequenz von 16kHz
SamplePerFrame = 32;
M = 43; %Anzahl der Elektrode bzw. Freqenzbaender

N = 11; %Anzahl der aktivierende Elektrode je Frame

WindowFunc = hann(WindowSize); %Jedes Frame muss erst mit Hanningfenster vorverarbeitet sein


%% Einstellung fuer Filterbank, Standart ACE hat 22 Kanaele
% Format: Mittelfrequenz | Anzahl der FFT-Bins | Koeffizient fuer jedes
% FFT-Bins
FilterVect =   [250	1	0.98
                312	1	0.98
                375	1	0.98
                437	1	0.98
                500	1	0.98
                562	1	0.98
                625	1	0.98
                687	1	0.98
                750	1	0.98
                812	1	0.98
                875	1	0.98
                937	1	0.98
                1000	1	0.98
                1062	1	0.98
                1125	1	0.98
                1187	1	0.98
                1250	1	0.98
                1312	1	0.98
                1437	2	0.68
                1562	2	0.68
                1687	2	0.68
                1812	2	0.68
                1937	2	0.68
                2062	2	0.68
                2187	2	0.68
                2312	2	0.68
                2500	3	0.65
                2688	3	0.65
                2875	3	0.65
                3063	3	0.65
                3312	4	0.65
                3562	4	0.65
                3812	4	0.65
                4062	4	0.65
                4375	5	0.65
                4688	5	0.65
                5000	5	0.65
                5313	5	0.65
                5687	6	0.65
                6062	6	0.65
                6500	7	0.65
                6938	7	0.65
                7437	16	0.65
];
          
FilterKoeff = zeros(WindowSize,M);
MidFreq = FilterVect(:,1);
FFTBins = FilterVect(:,2);
FilterGain = FilterVect(:,3);

%% Einhuellende Detektion
for i = 1:M
    offset = sum(FFTBins(1:i-1));
    % Da FFT(0) ist der Gleichanteil, faengt das Filterbank von FFT(2) an
    % da Erste Bandpassfilter dem Mittelfrequenz 250Hz entspricht
    FilterKoeff((offset+5):(offset+FFTBins(i)+4),i) = ones(FFTBins(i),1).*FilterGain(i);
end

%% Pyschoakustisches Modell
%Intepretierte Funktion der absoluten Hoerschwelle in Ruheumgebung nach 
%"Perceptual coding of digital audio" IEEE vol. 88, no. 4, pp455-515, 2000
Tabs = @(f) 3.64*(f/1000).^(-0.8)-6.5*exp(-0.6.*(f/1000-3.3).^2)+10^(-3)*(f/1000).^4;

% Level Function
Labs = Tabs(MidFreq);
Labs = Labs - min(Labs); % Nach Paper, soll minimale Werte von Labs 0dB sein

% Einstellung fuer Masking Modells
% Alle in dB
av = 10; % attenuation parameter
sl=20; % left slope
sr=17; % right slope
alpha = 0.25; % parameter fuer "Power-law Modell"