function [ synSig ] = synthese( Frames, SamplePerFrame,MidFreq, fs, M)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
L = length(Frames);

sig = zeros(1,L*SamplePerFrame);
tl = zeros(1,M);

for i = 1:L
    sig_sin =zeros(1,SamplePerFrame);
    for j = 1:M
        amp1 = Frames(i,j);
        if (i == L)
            amp2 = 0;
        else
            amp2 = Frames(i+1,j);
        end
        if (amp1 ~= 0) || (amp2 ~= 0)
            tr = tl(j) + SamplePerFrame/fs;
            t = linspace(tl(j),tr,SamplePerFrame);
            tl(j) = tr + 1/fs;
            temp_amp = amp_interp(amp1,amp2,SamplePerFrame);
            %temp_Sig = conv(sin(2*pi*MidFreq(j).*t), temp_amp,'same');
            sig_sin = sig_sin + temp_amp.* sin(2*pi*MidFreq(j).*t);
        end
    end
    sig((i-1)*SamplePerFrame+1:i*SamplePerFrame) = sig_sin;
end

synSig = sig;
end

function vect_amp = amp_interp(a1,a2,amp_N)

n = 0:amp_N-1;

delta = (a2-a1) / amp_N;

vect_amp = a1 + n.*delta;

end

